#include <Key.h>
#include <Keypad.h>
#include <TM1637Display.h>

const int ANSWER = 1524;

const byte AUDIO_OUT_PIN = 4;

const byte ROWS = 4;
const byte COLS = 4;

const int DISP_CLK = 3;
const int DISP_DIO = 2;

TM1637Display display(DISP_CLK, DISP_DIO);

char hexaKeys[ROWS][COLS] = {
  {'H', 'C', 'D', 'E'},
  {'F', 'G', 'A', 'B'},
  {'c', 'd', 'e', 'f'},
  {'g', 'a', 'b', 'h'}
};

byte rowPins[ROWS] = {13, 12, 11, 10};
byte colPins[COLS] = {9, 8, 7, 6};

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

void setup() {
  Serial.begin(9600);

  display.showNumberDec(0);
  display.setBrightness(0x0a); //set the diplay to maximum brightness
}

unsigned long lastOutUpdate = 0;
long outVal = 9999;
long ansVal = 0;
byte ansDigits = 0;
bool answerReached = false;

const int ANIMATION_FRAME = 150; 

void doOutAnimation() {
  unsigned long now = millis();

  if (now - lastOutUpdate > ANIMATION_FRAME) {
    lastOutUpdate = now;

    outVal -= 1111;

    if (answerReached) {
      if (ansDigits < 4) {

        long outPart = outVal / 1000;
        long ansPart = (ANSWER / (1000 / (int)pow(10, ansDigits))) % 10;

        if (outPart == ansPart) {
          ansVal = ansVal * 10 + ansPart;
          ansDigits++;
        }

        display.showNumberDec(outVal, false, 4 - ansDigits, ansDigits);
        display.showNumberDec(ansVal, false, ansDigits, 0);
      }
    }
    else {
      display.showNumberDec(outVal, true);
    }

    if (outVal < 0) {
      outVal = 9999;
    }
  }
}

const byte ANSWER_LENGHT = 5;
char in[ANSWER_LENGHT] = { 0, 0 , 0 , 0 , 0};
char answer[ANSWER_LENGHT] = { 'g', 'a', 'f', 'F', 'c'};
byte dataCount = 0;

void play(int hz) {
  tone(AUDIO_OUT_PIN, hz, 500);
}

bool isCorrect() {
  byte charsChecked = 0;
  int pos = dataCount;

  while (charsChecked < ANSWER_LENGHT) {
    Serial.println(pos);
    Serial.println(in[pos]);
    Serial.println(answer[ANSWER_LENGHT - 1 - charsChecked]);
    Serial.println("-");
    if (in[pos] != answer[ANSWER_LENGHT - 1 - charsChecked]) {
      return false;
    }
    pos--;

    if (pos < 0) {
      pos = ANSWER_LENGHT - 1;
    }

    charsChecked++;
  }

  return true;
}


void loop() {

  doOutAnimation();

  char customKey = customKeypad.getKey();

  if (customKey) {

    in[dataCount] = customKey;

    if (isCorrect()) {
      answerReached = true;
    }

    dataCount++;

    if (dataCount >= ANSWER_LENGHT) {
      dataCount = 0;
    }

    //    Serial.println(customKey);
    //    Serial.println(in);
    //    Serial.println(answer);
    //    Serial.println("-");

    switch (customKey) {
      case 'H': play(123); break;
      case 'C': play(131); break;
      case 'D': play(147); break;
      case 'E': play(165); break;
      case 'F': play(175); break;
      case 'G': play(196); break;
      case 'A': play(220); break;
      case 'B': play(247); break;
      case 'c': play(262); break;
      case 'd': play(294); break;
      case 'e': play(330); break;
      case 'f': play(349); break;
      case 'g': play(392); break;
      case 'a': play(440); break;
      case 'b': play(494); break;
      case 'h': play(523); break;
    }
  }
}
