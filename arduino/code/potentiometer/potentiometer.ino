#include <TM1637Display.h>

#include <LedControl.h>

const int CLK = 3; //Set the CLK pin connection to the display
const int DIO = 2; //Set the DIO pin connection to the display

int numCounter = 0;

TM1637Display display(CLK, DIO); //set up the 4-Digit Display.

/*
  pin 12 is connected to the DataIn
  pin 13 is connected to the CLK
  pin 10 is connected to LOAD
*/
LedControl lc = LedControl(12, 13, 10, 1);

const byte MAX_SAMPLES = 5;
const byte POTS = 4;
int inSamples[MAX_SAMPLES][POTS];
byte sampleNum = 0;

int in[POTS] = { 0, 0, 0, 0 };
int oldIn[POTS] = { 0, 0, 0, 0 };
int out[POTS] = { 0, 0, 0, 0};

void resetSamples() {
  for (byte i = 0; i < MAX_SAMPLES; i++) {
    for (byte j = 0; j < POTS; j++) {
      inSamples[i][j] = 0;
    }
  }
}

void calculateAverage() {
  for (byte j = 0; j < POTS; j++) {

    int potVal = 0;
    for (byte i = 0; i < MAX_SAMPLES; i++) {
      potVal += inSamples[i][j];
    }

    in[j] = (potVal / MAX_SAMPLES) / 100;

    if (in[j] > 9) {
      in[j] = 9;
    }
  }
}

void setup()
{
  resetSamples();

  Serial.begin(9600);

  // the zero refers to the MAX7219 number, it is zero for 1 chip
  lc.shutdown(0, false); // turn off power saving, enables display
  delay(100);
  lc.clearDisplay(0);// clear screen
  lc.setIntensity(0, 0); // sets brightness (0~15 possible values)


  display.showNumberDec(0);
  display.setBrightness(0x0a); //set the diplay to maximum brightness
}

byte getPin(byte index) {
  switch (index) {
    case 0: return A0;
    case 1: return A1;
    case 2: return A2;
    case 3: return A3;
    case 4: return A4;
    case 5: return A5;
  }

  return A5;
}

void inc(byte index, int value) {
  if (index >= 0 && index < POTS) {
    out[index] += value;

    Serial.print("out=");
    Serial.print(out[index]);
    Serial.print(" i=");
    Serial.print(index);
    Serial.print("\n");

    if (out[index] < 0) {
      out[index] = 0;
    }
    else if (out[index] > 9) {
      out[index] = 9;
    }
  }
}

void incOut(byte index, int value) {
  inc(index - 1, -value);
  inc(index, value);
  inc(index + 1, -value);
}

boolean assertReset() {
  boolean reset = true;

  for (byte i = 0; i < POTS; i++) {
    if (in[i] != 0) {
      reset = false;
    }
  }

  if (reset) {
    for (byte i = 0; i < POTS; i++) {
      oldIn[i] = in[i];
      out[i] = 0;
    }
  }

  return reset;
}

const int ANIMATION_FRAME = 70;
const byte NUM_CHARS = 19;
char chars[NUM_CHARS] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E','F', 'H', 'L', 'P' };
char correctAnswer[8] = {' ', ' ', ' ', '6', 'A', 'F', 'F', 'C'};
byte charPos = 0;
byte answerPos = 0;
unsigned long lastOutUpdate = 0;

void setChars(byte from) {

  for(int i = 8 - from; i < 8; i++) {
    lc.setChar(0, i, correctAnswer[8-1-i], false);
  }
  
  for(int i = 0; i < (8 - from); i++) {
    lc.setChar(0, i, chars[charPos], false);
  }
}

void animateOut(bool done) {
  unsigned long now = millis();

  if (now - lastOutUpdate > ANIMATION_FRAME) {
    lastOutUpdate = now;

    setChars(answerPos);

    if(done && answerPos < 8) {
      /* Having the empty char be part of the animation causes too much flicker,
       *  so we have to check against the last pos and empty answer specifically.
       */
      if(chars[charPos] == correctAnswer[answerPos] || 
      (chars[charPos] == 'P' && correctAnswer[answerPos] == ' ')) {
        answerPos++;
      }
    }

    charPos++;

    if(charPos >= NUM_CHARS) {
      charPos = 0;
    } 
  }
}

void loop()
{
  for (byte i = 0; i < POTS; i++) {
    inSamples[sampleNum][i] = analogRead(getPin(i));
  }
  sampleNum++;

  if (sampleNum >= MAX_SAMPLES) {

    calculateAverage();
    resetSamples();
    sampleNum = 0;

    for (byte i = 0; i < POTS; i++) {

      if (!assertReset()) {
        if (in[i] != oldIn[i]) {
          int diff = in[i] - oldIn[i];

          Serial.print("in=");
          Serial.print(in[i]);
          Serial.print(" i=");
          Serial.print(i);
          Serial.print("\n");

          Serial.print("oldIn=");
          Serial.print(oldIn[i]);
          Serial.print(" i=");
          Serial.print(i);
          Serial.print("\n");

          oldIn[i] = in[i];

          incOut(i, diff);
        }
      }
    }
  }


  int dispVal = out[0] + out[1] * 10 + out[2] * 100 + out[3] * 1000;
  display.showNumberDec(dispVal);

  if (dispVal == 4216) {
//    lc.setChar(0, 7, ' ', false);
//    lc.setChar(0, 6, ' ', false);
//    lc.setChar(0, 5, ' ', false);
//    lc.setChar(0, 4, '6', false);
//    lc.setChar(0, 3, 'a', false);
//    lc.setChar(0, 2, 'f', false);
//    lc.setChar(0, 1, 'f', false);
//    lc.setChar(0, 0, 'c', false);
      animateOut(true);
  }
  else {
      animateOut(false);
  }
}
