#include <Key.h>
#include <Keypad.h>
#include <TM1637Display.h>

const int DISP_IN_CLK = 3;
const int DISP_IN_DIO = 2;

const int DISP_OUT_CLK = 5;
const int DISP_OUT_DIO = 4;

TM1637Display displayIn(DISP_IN_CLK, DISP_IN_DIO);
TM1637Display displayOut(DISP_OUT_CLK, DISP_OUT_DIO);

const byte ROWS = 4;
const byte COLS = 3;

const int CORRECT_INPUT = 6495;
const int ANSWER = 4216;

char hexaKeys[ROWS][COLS] = {
  {'6', '0', '4'},
  {'7', '8', '2'},
  {'9', '3', '5'},
  {'*', '1', '#'}
};

byte rowPins[ROWS] = {13, 12, 11, 10};
byte colPins[COLS] = {9, 8, 7};

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

unsigned long val = 0;
unsigned long lastOutUpdate = 0;

void setup() {
  Serial.begin(9600);
  displayIn.showNumberDec(0);
  displayIn.setBrightness(0x0a); //set the diplay to maximum brightness

  displayOut.showNumberDec(0);
  displayOut.setBrightness(0x0a); //set the diplay to maximum brightness
}

long outVal = 9999;
long ansVal = 0;
byte ansDigits = 0;
bool answerReached = false;

void doOutAnimation() {
  unsigned long now = millis();

  if (now - lastOutUpdate > 300) {
    lastOutUpdate = now;

    outVal -= 1111;

    if (answerReached) {
      if (ansDigits < 4) {

        long outPart = outVal / 1000;
        long ansPart = (ANSWER / (1000 / (int)pow(10, ansDigits))) % 10;

        if (outPart == ansPart) {
          ansVal = ansVal * 10 + ansPart;
          ansDigits++;
        }
        
        displayOut.showNumberDec(outVal, false, 4 - ansDigits, ansDigits);
        displayOut.showNumberDec(ansVal, false, ansDigits, 0);
      }
    }
    else {
      displayOut.showNumberDec(outVal, true);
    }

    if (outVal < 0) {
      outVal = 9999;
    }
  }
}

void loop() {

  doOutAnimation();

  char customKey = customKeypad.getKey();

  if (customKey) {
    if (customKey == '*' || customKey == '#') {
      val = 0;
      displayIn.clear();
    }
    else {
      val = val * 10 + (customKey - '0');

      if (val > 9999) {
        val = 0;
        displayIn.clear();
      }
      else {
        displayIn.showNumberDec(val);

        if(val == CORRECT_INPUT) {
          answerReached = true;
        }
      }
    }

    Serial.println(customKey);
    Serial.println(val);
    Serial.println("-");
  }
}
