#include "morse.h"

SpeakerMorseSender morse(8);

void setup() {

  morse.setup();
  morse.setMessage(String("6  4  9  5"));
  morse.setSpeed(100);
  
}

void loop() {
  morse.sendBlocking();
  delay(5000);
}
