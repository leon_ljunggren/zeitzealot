# Zeitzealot

Written in Angular 7 for frontend and Nodejs for backend

## To run localy

Requires that @angular/cli@7.1.2 and typescript@3.1.6 are installed globably with node 11.3.0

sudo npm install -g @angular/cli@7.1.2
sudo npm install -g typescript@3.1.6

Then you can run it localy by just

npm install

npm start

## To deploy to server

Assumes that ssh keys are setup to zeitzealot.org and that pm2 is installed (sudo npm install -g pm2) and already started with pm2 start zeit-server.js --watch

npm run deploy