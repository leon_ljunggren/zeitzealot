-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: zeitdb.cyfxjwwqr4r6.eu-west-1.rds.amazonaws.com    Database: zeit
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED='';

--
-- Table structure for table `FinalState`
--

DROP TABLE IF EXISTS `FinalState`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FinalState` (
  `id` int(11) NOT NULL DEFAULT '0',
  `state` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FinalState`
--

LOCK TABLES `FinalState` WRITE;
/*!40000 ALTER TABLE `FinalState` DISABLE KEYS */;
INSERT INTO `FinalState` VALUES (0,0);
/*!40000 ALTER TABLE `FinalState` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GpsTask`
--

DROP TABLE IF EXISTS `GpsTask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GpsTask` (
  `domain` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `approvalRadius` int(11) DEFAULT '0',
  `description` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT '',
  PRIMARY KEY (`domain`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GpsTask`
--

LOCK TABLES `GpsTask` WRITE;
/*!40000 ALTER TABLE `GpsTask` DISABLE KEYS */;
INSERT INTO `GpsTask` VALUES ('bigcheese.zeitzealot.org','gps-intro',53.355779,-1.204747,100,'head to the place with butterflies.'),('drlove.zeitzealot.org','gps-intro',59.9692,10.731807,100,'head to the water by the parish.'),('editor.zeitzealot.org','gps-intro',59.9692,10.731807,100,'head to the water by the parish.'),('leatherhead.zeitzealot.org','gps-intro',59.420125,5.300407,100,'head to the dam that holds back the windy water.'),('littlemy.zeitzealot.org','gps-intro',59.923082,10.763602,100,'head to the park.'),('mastermind.zeitzealot.org','gps-intro',59.923082,10.763602,100,'head to the park.'),('pussycat.zeitzealot.org','gps-intro',59.420125,5.300407,100,'head to the dam that holds back the windy water.'),('rocketeer.zeitzealot.org','gps-intro',59.819408,17.631875,100,'head to the crossroad'),('tortoise.zeitzealot.org','gps-intro',59.923082,10.763602,100,'head to the park.'),('wormtongue.zeitzealot.org','gps-intro',59.420125,5.300407,100,'head to the dam that holds back the windy water.');
/*!40000 ALTER TABLE `GpsTask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Player`
--

DROP TABLE IF EXISTS `Player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Player` (
  `domain` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subState` int(11) DEFAULT '0',
  `partner` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT '',
  PRIMARY KEY (`domain`),
  UNIQUE KEY `user_index` (`user`),
  UNIQUE KEY `nick_index` (`nickname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Player`
--

LOCK TABLES `Player` WRITE;
/*!40000 ALTER TABLE `Player` DISABLE KEYS */;
INSERT INTO `Player` VALUES ('bigcheese.zeitzealot.org','one','one','final-station',0,'girlfriend'),('drlove.zeitzealot.org','two','two','pre-final-station',0,'husband'),('editor.zeitzealot.org','three','three','stations',0,'husband'),('footballer.zeitzealot.org','four','four','stations',0,'wife'),('leatherhead.zeitzealot.org','five','five','final-station',0,'wife'),('littlemy.zeitzealot.org','six','six','final-station',0,'husband'),('mastermind.zeitzealot.org','seven','seven','final-station',0,'nothing'),('photographer.zeitzealot.org','eight',NULL,'intro',0,'wife'),('pussycat.zeitzealot.org','nine','nine','final-station',0,'boyfriend'),('rocketeer.zeitzealot.org','ten','ten','personality-test',0,'wife'),('tortoise.zeitzealot.org','eleven','eleven','stations',0,'wife'),('wormtongue.zeitzealot.org','twelve','twelve','final-station',0,'girlfriend');
/*!40000 ALTER TABLE `Player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PlayerHistory`
--

DROP TABLE IF EXISTS `PlayerHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PlayerHistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `riddle` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `atempt` text COLLATE utf8mb4_unicode_ci,
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `domain` (`domain`,`riddle`),
  KEY `riddle` (`riddle`)
) ENGINE=InnoDB AUTO_INCREMENT=6910 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Station`
--

DROP TABLE IF EXISTS `Station`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Station` (
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `locationRadius` int(11) DEFAULT '0',
  `approvalRadius` int(11) DEFAULT '0',
  `agentsNeeded` int(11) DEFAULT '0',
  `hour` int(11) DEFAULT '0',
  `minute` int(11) DEFAULT '0',
  `data` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT 'location-unknown',
  PRIMARY KEY (`name`),
  KEY `state` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Station`
--

LOCK TABLES `Station` WRITE;
/*!40000 ALTER TABLE `Station` DISABLE KEYS */;
INSERT INTO `Station` VALUES ('Able','stations',66.85601,13.783506,6000,300,3,0,23,'<div>红鸟导弹系统的代码。 这些代码由CryptoJs库使用AES加密，PKCS7用于填充和CBC模式。</div><div>---</div><div>U2FsdGVkX1+Dh0JFiiHiwZ7I4glCQD80Qldp0diL4rMtiJnq5yXwk1ergYRzbOhD</div>','done'),('Bravo','stations',59.824043,7.192807,6000,200,3,12,56,'<div>-----BEGIN PUBLIC KEY-----</div><div>MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKVeEC+VUvxm4TjY3tIllPQdDpL0sq5h\n5NP3dqUzupKxXWZRmXMAlg1gitWnFPhYrkKO+tpwCGZYxuBAHJbEMb0CAwEAAQ==</div><div>-----END PUBLIC KEY-----</div>','done'),('Foxtrot','stations',59.981367,10.796408,6000,200,3,17,19,'<div>红鸟导弹系统代码密码。 使用MD5进行哈希处理。</div><div>---</div><div>568c31f0f2406ab70255a1d83291220f</div>','done'),('Hotel','stations',53.334044,-1.270348,2000,200,3,19,5,'<div>IMPORTANT: DO NOT SHARE WITH THE FRENCH, THOSE FROG EATERS CAN NOT BE TRUSTED.</div><div>Contains the authorization codes for the Trident II.</div><div>--- RSA ---</div><div>OKiH2B8lv7N9fJit0MDqe3UfAw9M+b5/5srEoFvd1pLEs+Bt9rXA2ploebF2iPNvmQNk43AbQHzsULqpYUQZ2w==</div>','done'),('Whiskey','final-station',78.229301,15.601448,6000,400,2,15,2,'<div>-----BEGIN RSA PRIVATE KEY-----</div><div>MIIBPQIBAAJBAKVeEC+VUvxm4TjY3tIllPQdDpL0sq5h5NP3dqUzupKxXWZRmXMA<br>lg1gitWnFPhYrkKO+tpwCGZYxuBAHJbEMb0CAwEAAQJBAIwcrsoiUzXYVWu9FM4p<br>WZOcKwUIn3xxJVkTi0teEO7IR4JbM3hxSmCJzzlrkgVL323zW6rM9yRBqbLzECcl<br>fgECIQDx8gL4lyDGmuqwsJW3Y70KuvXKZ4Bwu4n13f0hJApTEQIhAK75QVxaBtGd<br>Xz6Pddqbg8EQYcja53gqAt3leLSWSpvtAiEAlMyUyzKXM84/HJ/d1EYBE8JcPDlf<br>ovWUKaR3c5RozSECIQCc0vSdBGeGKmm6VbSIycfuG3O5JVI/JlBJd6mXGfknBQIh<br>AI7G9Mc/ZxcZWEUgllmivin+Dx/Zs/8v4dIhRl6aOAjS</div><div>-----END RSA PRIVATE KEY-----</div>','done');
/*!40000 ALTER TABLE `Station` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TeamTask`
--

DROP TABLE IF EXISTS `TeamTask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TeamTask` (
  `state` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complete` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TeamTask`
--

LOCK TABLES `TeamTask` WRITE;
/*!40000 ALTER TABLE `TeamTask` DISABLE KEYS */;
INSERT INTO `TeamTask` VALUES ('satellite',1);
/*!40000 ALTER TABLE `TeamTask` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-18 17:51:20
