/* If you want to use your local network address to access from other 
 * devices on the local network you have to specify it here. Otherwise 
 * localhost works fine.
 * You also have to add the address to the cross domain settings in 
 * the server's api.ts file.
 */

export const environment = {
  production: false,
  api: 'http://192.168.1.10:3002'
};
