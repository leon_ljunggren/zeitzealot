/*
 * Code from https://github.com/lit-forest/leaflet.migrationLayer
 * Modified by Leon to add controllable speed and ability to destroy induvidual traces.
 */

(function (window) {
    var utils = {
        calculateColor: function (color, opacity) {
            if (color.indexOf('#') === 0) {
                var color16 = color.slice(1);
                var r = parseInt(color16.slice(0, 2), 16);
                var g = parseInt(color16.slice(2, 4), 16);
                var b = parseInt(color16.slice(4), 16);
                return 'rgba(' + r + ',' + g + ',' + b + ',' + opacity + ')';
            } else if (/^rgb\(/.test(color)) {
                return color.replace(/rgb/, 'rgba').replace(')', ",") +
                    opacity + ')';
            } else {
                return color.split(',').splice(0, 3).join(',') +
                    opacity + ')';
            }
        },
        destinationReachedCallback: function () {}
    };
    var arrayUtils = {
        forEach: function (arr, cb, scope) {
            if (typeof Array.prototype.forEach === 'function') {
                arr.forEach(cb, scope);
            } else {
                for (var i = 0, len = arr.length; i < len; i++) {
                    cb.apply(scope, [arr[i], i, arr]);
                }
            }
        },
        map: function (arr, cb, scope) {
            if (typeof Array.prototype.map === 'function') {
                return arr.map(cb, scope);
            } else {
                var mapped = [];
                for (var i = 0, len = arr.length; i < len; i++) {
                    mapped[i] = cb.apply(scope, [arr[i], i, arr]);
                }
                return mapped;
            }
        }
    };

    var Marker = (function () {
        var M = function (options) {
            this.x = options.markerX;
            this.y = options.markerY;
            this.rotation = options.markerRotation;
            this.style = options.markerStyle;
            this.color = options.color;
            this.size = options.markerSize;
            this.borderWidth = options.markerBorderWidth;
            this.borderColor = options.markerBorderColor;
        };

        M.prototype.draw = function (context) {
            context.save();
            context.translate(this.x, this.y);
            context.rotate(this.rotation);

            context.lineWidth = this.borderWidth || 0;
            context.strokeStyle = this.borderColor || '#000';
            context.fillStyle = this.color || '#000';
            context.beginPath();
            if (this.style === 'circle') {
                context.arc(0, 0, this.size, 0, Math.PI * 2, false);
            } else if (this.style === 'arrow') {
                context.moveTo(-this.size * 2, -this.size);
                context.lineTo(-this.size * 5 / 4, 0);
                context.lineTo(-this.size * 2, this.size);
                context.lineTo(0, 0);
                context.lineTo(-this.size * 2, -this.size);
            }
            context.closePath();
            context.stroke();
            context.fill();
            context.restore();
        };

        return M;
    })();

    var Arc = (function () {
        var A = function (options) {
            var startX = options.startX,
                startY = options.startY,
                endX = options.endX,
                endY = options.endY;

            var L = Math.sqrt(Math.pow(startX - endX, 2) + Math.pow(startY - endY, 2));
            var m = (startX + endX) / 2;
            var n = (startY + endY) / 2;
            var factor = 1.5;

            var centerX = (startY - endY) * factor + m;
            var centerY = (endX - startX) * factor + n;

            var radius = Math.sqrt(Math.pow(L / 2, 2) + Math.pow(L * factor, 2));
            var startAngle = Math.atan2(startY - centerY, startX - centerX);
            var endAngle = Math.atan2(endY - centerY, endX - centerX);

            this.startX = startX;
            this.startY = startY;
            this.endX = endX;
            this.endY = endY;
            this.centerX = centerX;
            this.centerY = centerY;
            this.startAngle = startAngle;
            this.endAngle = endAngle;
            this.startLabel = options && options.labels && options.labels[0];
            this.endLabel = options && options.labels && options.labels[1];
            this.radius = radius;
            this.lineWidth = options.arcWidth || 1;
            this.strokeStyle = options.color || '#000';
            this.label = options.label;
            this.font = options.font;
            this.shadowBlur = options.shadowBlur;
            this.endAngle = endAngle - this.lineWidth / radius;
        };

        A.prototype.draw = function (context) {
            context.save();
            context.lineWidth = this.lineWidth;
            context.strokeStyle = this.strokeStyle;
            context.shadowColor = this.strokeStyle;
            context.shadowBlur = this.shadowBlur || 2;

            context.beginPath();
            context.arc(this.centerX, this.centerY, this.radius, this.startAngle, this.endAngle, false);
            context.stroke();
            context.restore();

            context.save();
            context.fillStyle = this.strokeStyle;
            if (this.label) {
                context.font = this.font;
                if (this.startLabel) {
                    var x = this.startX - 15
                    var y = this.startY + 5
                    context.fillText(this.startLabel, x, y);
                }
                if (this.endLabel) {
                    var x = this.endX - 15;
                    var y = this.endY - 5;
                    context.fillText(this.endLabel, x, y);
                }
            }
            context.restore();
        };

        return A;
    })();

    var Pulse = (function () {
        function P(options) {
            this.x = options.pulseX;
            this.y = options.pulseY;
            this.maxRadius = options.pulseRadius;
            this.color = options.color;
            this.shadowBlur = 5;
            this.lineWidth = options.pulseBorderWidth;
            this.r = 0;
        };

        P.prototype.draw = function (context) {
            var vr = 0.5;
            this.r += vr;

            context.save();
            context.translate(this.x, this.y);
            var strokeColor = this.color;
            strokeColor = utils.calculateColor(strokeColor, 1 - this.r / this.maxRadius);
            context.strokeStyle = strokeColor;
            context.shadowBlur = this.shadowBlur;
            context.shadowColor = strokeColor;
            context.lineWidth = this.lineWidth;
            context.beginPath();
            context.arc(0, 0, this.r, 0, Math.PI * 2, false);
            context.stroke();
            context.restore();

            if (Math.abs(this.maxRadius - this.r) < 0.8) {
                this.r = 0;
            }
        }

        return P;
    })();

    var Spark = (function () {
        var S = function (options) {

            this.arc = new Arc(options);
            this.pulse = new Pulse(options);


            var startX = options.startX,
                startY = options.startY,
                endX = options.endX,
                endY = options.endY;


            var L = Math.sqrt(Math.pow(startX - endX, 2) + Math.pow(startY - endY, 2));
            var m = (startX + endX) / 2;
            var n = (startY + endY) / 2;
            var factor = 1.5;

            var centerX = (startY - endY) * factor + m;
            var centerY = (endX - startX) * factor + n;

            var radius = Math.sqrt(Math.pow(L / 2, 2) + Math.pow(L * factor, 2));
            var startAngle = Math.atan2(startY - centerY, startX - centerX);
            var endAngle = Math.atan2(endY - centerY, endX - centerX);

            if (startAngle * endAngle < 0) {
                if (startAngle < 0) {
                    startAngle += Math.PI * 2;
                    endAngle += Math.PI * 2;
                } else {
                    endAngle += Math.PI * 2;
                }
            }

            options.markerRotation = endAngle + Math.PI / 2;
            this.endMarker = new Marker(options);

            this.id = options.id;
            this.tailPointsCount = 50;
            this.centerX = centerX;
            this.centerY = centerY;
            this.startAngle = startAngle;
            this.endAngle = endAngle;
            this.radius = radius;
            this.lineWidth = options.width || 1;
            this.strokeStyle = options.color || '#fff';
            this.radiansPerSecond = Math.abs(this.endAngle - this.startAngle) / (options.speedInMinutes * 60 - options.speedMinusMinutes * 60); // / this.radius;
            this.deltaAngle = (80 / Math.min(this.radius, 400)) / this.tailPointsCount;
            this.trailAngle = this.startAngle;
            this.arcAngle = this.startAngle;

            this.animateBlur = true;

            this.alive = true;
            this.done = false;

            const size = options.size ? options.size / 2 : 1
            this.marker = new Marker({
                markerX: 50,
                markerY: 80,
                markerRotation: 50 * Math.PI / 180,
                markerStyle: 'arrow',
                color: this.strokeStyle,
                markerSize: size + 5,
                markerBorderWidth: size,
                markerBorderColor: this.strokeStyle
            });
        };

        S.prototype.drawArc = function (context, strokeColor, lineWidth, startAngle, endAngle) {
            context.save();
            context.lineWidth = lineWidth;
            context.strokeStyle = strokeColor;
            context.shadowColor = this.strokeStyle;
            context.lineCap = "round";
            context.beginPath();
            context.arc(this.centerX, this.centerY, this.radius, startAngle, endAngle, false);
            context.stroke();
            context.restore();
        };

        S.prototype.draw = function (context, delta, speedAdjustment) {

            if (this.alive) {

                if (!this.done) {

                    this.arc.draw(context, delta);
                    this.endMarker.draw(context, delta);
                    
                    var endAngle = this.endAngle;

                    /* speedAdjustment allows the missiles to speed up/down dynamically in flight */
                    var angle = this.trailAngle + this.radiansPerSecond * delta * speedAdjustment;
                    var strokeColor = this.strokeStyle;
                    if (this.animateBlur) {
                        this.arcAngle = angle;
                    }
                    this.trailAngle = angle;
                    strokeColor = utils.calculateColor(strokeColor, 0.1);

                    this.drawArc(context, strokeColor, this.lineWidth, this.startAngle, this.arcAngle);

                    var count = this.tailPointsCount;
                    for (var i = 0; i < count; i++) {
                        var arcColor = utils.calculateColor(this.strokeStyle, 0.3 - 0.3 / count * i);
                        var tailLineWidth = 5;
                        if (this.trailAngle - this.deltaAngle * i > this.startAngle) {
                            this.drawArc(context, arcColor,
                                tailLineWidth - tailLineWidth / count * i,
                                this.trailAngle - this.deltaAngle * i,
                                this.trailAngle
                            );
                        }
                    }

                    context.save();
                    context.translate(this.centerX, this.centerY);
                    this.marker.x = Math.cos(this.trailAngle) * this.radius;
                    this.marker.y = Math.sin(this.trailAngle) * this.radius;
                    this.marker.rotation = this.trailAngle + Math.PI / 2;
                    this.marker.draw(context);
                    context.restore();

                    if ((endAngle - this.trailAngle) * 180 / Math.PI < 0.5) {
                        utils.destinationReachedCallback(this.id);
                        this.done = true;
                    }
                }
                else {
                    this.pulse.draw(context, delta);
                }
            }
        };

        S.prototype.killNotDone = function() {
            if(!this.done) {
                this.alive = false;
            }
        };

        S.prototype.kill = function() {
            this.alive = false;
        };

        S.prototype.getId = function() {
            return this.id;
        }

        return S;
    })();


    var Migration = (function () {
        var M = function (options) {
            this.data = options.data;
            this.store = {
                sparks: []
            };
            this.playAnimation = true;
            this.started = false;
            this.context = options.context;
            this.style = options.style;
            this.speedMinusMinutes = options.speedMinusMinutes;
            this.speedAdjustment = 1.0;
            this.init();
        };

        M.prototype.init = function () {
            this.updateData(this.data);
        };

        M.prototype.setSpeedAdjustment = function(speedAdjustment) {
            if ( speedAdjustment < 0) {
                this.speedAdjustment = 0;
            }
            else {
                this.speedAdjustment = speedAdjustment;
            }
        };

        M.prototype.add = function (Shape) {

        };
        M.prototype.remove = function () {

        };
        M.prototype.clear = function () {
            this.store = {
                sparks: []
            };

            this.playAnimation = true;
            this.started = false;

            window.cancelAnimationFrame(this.requestAnimationId);
        };

        M.prototype.updateData = function (data) {
            if (!data || data.length === 0) {
                return;
            }
            this.clear();
            this.data = data;
            if (this.data && this.data.length > 0) {
                arrayUtils.forEach(this.data, function (element) {
                    var spark = new Spark({
                        id: element.id,
                        startX: element.from[0],
                        startY: element.from[1],
                        endX: element.to[0],
                        endY: element.to[1],
                        width: 15,
                        color: element.color,
                        size: element.arcWidth,
                        labels: element.labels,
                        label: this.style.arc.label,
                        font: this.style.arc.font,
                        arcWidth: element.arcWidth || this.style.arc.width,
                        color: element.color,
                        markerX: element.to[0],
                        markerY: element.to[1],
                        markerStyle: 'arrow',
                        markerBorderWidth: 0,
                        markerBorderColor: element.color,
                        markerSize: this.style.arc.width + 3,
                        pulseX: element.to[0],
                        pulseY: element.to[1],
                        pulseRadius: this.style.pulse.radius,
                        pulseBorderWidth: this.style.pulse.borderWidth,
                        speedInMinutes: element.speedInMinutes,
                        speedMinusMinutes: this.speedMinusMinutes
                    });
                    this.store.sparks.push(spark);
                }, this);
            }
        };
        /*
        */
        M.prototype.start = function (canvas) {
            var that = this;
            if (!this.started) {
                (function drawFrame() {

                    that.setDelta();

                    if (that.playAnimation) {
                        canvas.width += 1;
                        canvas.width -= 1;
                        for (var p in that.store) {
                            var shapes = that.store[p];
                            for (var i = 0, len = shapes.length; i < len; i++) {
                                shapes[i].draw(that.context, that.delta, that.speedAdjustment);
                            }
                        }
                    }

                    that.requestAnimationId = window.requestAnimationFrame(drawFrame, canvas);
                })();
                this.started = true;
            }
        };

        M.prototype.setDelta = function () {
            this.now = Date.now();
            this.delta = (this.now - this.then) / 1000; // seconds since last frame
            this.then = this.now;
        };
        M.prototype.play = function () {
            this.playAnimation = true;
        };
        M.prototype.pause = function () {
            this.playAnimation = false;
        };
        M.prototype.killAllNotDone = function () {
            this.store.sparks.forEach(function(spark) {
                spark.killNotDone();
            });
        };
        M.prototype.killNotDone = function (id) {
            this.store.sparks.forEach(function(spark) {
                if(spark.getId() === id) {
                    spark.killNotDone();
                }
            });
        };
        M.prototype.kill = function (id) {
            this.store.sparks.forEach(function(spark) {
                if(spark.getId() === id) {
                    spark.kill();
                }
            });
        };
        return M;
    })();

    L.MigrationLayer = L.Class.extend({
        options: {
            map: {},
            data: {},
            pulseRadius: 25,
            pulseBorderWidth: 3,
            arcWidth: 1,
            arcLabel: true,
            arcLabelFont: '15px sans-serif',
            speedMinusMinutes: 1,
            Marker: {},
            Spark: {}

        },
        _setOptions: function (obj, options) {
            if (!obj.hasOwnProperty('options')) {
                obj.options = obj.options ? L.Util.create(obj.options) : {};
            }
            for (var i in options) {
                obj.options[i] = options[i];
            }
            return obj.options;
        },
        initialize: function (options, destinationReachedCallback) {

            if (destinationReachedCallback) {
                utils.destinationReachedCallback = destinationReachedCallback;
            }

            this._setOptions(this, options);
            this._map = this.options.map || {};
            this._data = this.options.data || {};
            this._speedMinusMinutes = this.options.speedMinusMinutes || 0;
            this._style = {
                pulse: {
                    radius: this.options.pulseRadius,
                    borderWidth: this.options.pulseBorderWidth
                },
                arc: {
                    width: this.options.arcWidth,
                    label: this.options.arcLabel,
                    font: this.options.arcLabelFont
                }
            } || {};
            this._show = true;
            this._init();
        },
        _init: function () {
            var container = L.DomUtil.create('div', 'leaflet-ODLayer-container');
            container.style.position = 'absolute';
            container.style.width = this._map.getSize().x + "px";
            container.style.height = this._map.getSize().y + "px";

            this.container = container;

            this.canvas = document.createElement('canvas');
            this.context = this.canvas.getContext('2d');
            container.appendChild(this.canvas);
            this._map.getPanes().overlayPane.appendChild(container);

            if (!this.migration) {
                var data = this._convertData();
                this.migration = new Migration({
                    data: data,
                    context: this.context,
                    style: this._style,
                    speedMinusMinutes: this._speedMinusMinutes
                });
            }
        },
        _resize: function () {
            var bounds = this._map.getBounds();
            var topleft = bounds.getNorthWest();
            var topLeftscreen = this._map.latLngToContainerPoint(topleft);

            if (topLeftscreen.y > 0) {
                this.container.style.top = -topLeftscreen.y + 'px';
            } else {
                this.container.style.top = '0px';
            }

            var containerStyle = window.getComputedStyle(this._map.getContainer());
            this.canvas.setAttribute('width', parseInt(containerStyle.width, 10));
            this.canvas.setAttribute('height', parseInt(containerStyle.height, 10));
        },
        _convertData: function () {
            var bounds = this._map.getBounds();
            let maxValue;
            let minValue
            if (this._data && bounds) {
                arrayUtils.forEach(this._data, function (d) {
                    if (d.value) {
                        if (!maxValue) {
                            maxValue = d.value;
                            minValue = d.value;
                        }
                        if (maxValue < d.value) {
                            maxValue = d.value;
                        }
                        if (minValue > d.value) {
                            minValue = d.value;
                        }
                    }
                });
                var maxWidth = this.options.maxWidth || 10;
                var data = arrayUtils.map(this._data, function (d) {
                    if (d.value) {
                        if (!maxValue) {
                            maxValue = d.value;
                            minValue = d.value;
                        }
                        if (maxValue < d.value) {
                            maxValue = d.value;
                        }
                        if (minValue > d.value) {
                            minValue = d.value;
                        }
                    }

                    var fromPixel = this._map.latLngToContainerPoint(new L.LatLng(d.from[0], d.from[1]));
                    var toPixel = this._map.latLngToContainerPoint(new L.LatLng(d.to[0], d.to[1]));
                    return {
                        from: [fromPixel.x, fromPixel.y],
                        to: [toPixel.x, toPixel.y],
                        labels: d.labels,
                        value: d.value,
                        color: d.color,
                        speedInMinutes: d.speedInMinutes,
                        id: d.id,
                        arcWidth: d.value ? parseInt((d.value - minValue) * (maxWidth - 1) / (maxValue - minValue)) + 1 : this.options.arcWidth
                    }
                }, this);

                return data;
            }
        },
        _bindMapEvents: function () {
            var that = this;
            this._map.on('moveend', function () {
                // that.migration.play();
                // that._draw();
                console.log("Moveend event");
            });
            this._map.on('zoomstart ', function () { that.container.style.display = 'none' });
            this._map.on('zoomend', function () {
                if (that._show) {
                    that.container.style.display = ''
                    that._draw();
                }
            });
        },
        _draw: function () {
            var bounds = this._map.getBounds();
            if (bounds && this.migration.playAnimation) {
                this._resize();
                this._transform();
                var data = this._convertData();
                this.migration.updateData(data);
                this.migration.start(this.canvas);
            }
        },
        _transform: function () {
            var bounds = this._map.getBounds();
            var topLeft = this._map.latLngToLayerPoint(bounds.getNorthWest());
            L.DomUtil.setPosition(this.container, topLeft);
        },
        addTo: function () {
            this._bindMapEvents();
            var bounds = this._map.getBounds();
            if (bounds && this.migration.playAnimation) {
                this._resize();
                this._transform();

                var data = this._convertData();
                this.migration.updateData(data);
                this.migration.start(this.canvas);
            }
        },
        setData: function (data) {
            this._data = data;
            this._draw();
        },
        setSpeedAdjustment: function (speedAdjustment) {
            this.migration.setSpeedAdjustment(speedAdjustment);
        },
        hide: function () {
            this.container.style.display = 'none';
            this._show = false;
        },
        show: function () {
            this.container.style.display = '';
            this._show = true;
        },
        play: function () {
            this.migration.play();
            this._draw();
        },
        pause: function () {
            this.migration.pause();
        },
        killAllNotDone: function () {
            this.migration.killAllNotDone();
        },
        killNotDone: function (id) {
            this.migration.killNotDone(id);
        },
        kill: function (id) {
            this.migration.kill(id);
        },
        destroy: function () {
            this.migration.clear();
            this.container.parentNode.removeChild(this.container);
            this._map.clearAllEventListeners();
            this.mapHandles = [];
        }
    });
    L.migrationLayer = function (options, destinationReachedCallback) {
        return new L.MigrationLayer(options, destinationReachedCallback)
    }
})(window)
