import { Component, OnInit } from '@angular/core';
import { MissileCommand } from '../command/missile-command';

@Component({
  selector: 'app-command-test',
  templateUrl: './command-test.component.html',
  styleUrls: ['./command-test.component.scss']
})
export class CommandTestComponent implements OnInit {

  private mc: MissileCommand;
  
  constructor() { }

  play() {
    this.mc.startLevel(42, 10, 0.0);
  }

  ngOnInit() {
    this.mc = new MissileCommand((levelId: number, success: boolean) => {
      console.log("Level done", levelId, success);
    });
  }
}
