import { createLoweredSymbol } from '@angular/compiler';

export enum Button {
    A = 0,
    B = 1
}

export enum Axis {
    leftJoystickX = 0,
    leftJoystickY = 1
}

export interface JoyCord {
    x: number;
    y: number;
}

export class Controler {

    private static readonly JOY_DEADZONE = 0.2;

    private controlerIndex: number;
    private current: Gamepad;
    private old: Gamepad;

    constructor(controlerIndex: number) {

        this.controlerIndex = controlerIndex;

        window.addEventListener("gamepadconnected", function (e: GamepadEvent) {
            console.log("Gamepad connected at index %d: %s.",
                e.gamepad.index, e.gamepad.id,
                e.gamepad);
        });
    }

    public poll() {
        const gamepads = navigator.getGamepads();

        if (gamepads && gamepads.length > this.controlerIndex) {
            this.old = this.current;
            this.current = gamepads[this.controlerIndex];
        }
    }

    /* Requires a press and release to trigger */
    public isButtonClicked(button: Button): boolean {
        if (this.current && this.old) {
            return !this.current.buttons[button].pressed && this.old.buttons[button].pressed;
        }

        return false;
    }

    public getLefJoystick() {

        const cord = { x: 0, y: 0 };

        if (this.current) {
            if (Math.abs(this.current.axes[Axis.leftJoystickX]) > Controler.JOY_DEADZONE) {
                cord.x = this.current.axes[Axis.leftJoystickX];
            }

            if (Math.abs(this.current.axes[Axis.leftJoystickY]) > Controler.JOY_DEADZONE) {
                cord.y = this.current.axes[Axis.leftJoystickY];
            }
        }

        return cord;
    }
}
