import { Controler, Button, JoyCord } from './controler';

class Player {

    private static readonly TIME_BETWEEN_FIRE = 1000;

    private playerIndex: number;
    private crosshair: Crosshair;
    private controler: Controler;

    private mc: MissileCommand;

    private canFire = true;

    constructor(mc: MissileCommand, playerIndex: number, width: number, height: number, color: string) {
        this.playerIndex = playerIndex;
        this.controler = new Controler(playerIndex),
        this.crosshair = new Crosshair(width, height, color);

        /* This is pretty ugly, but easier than refactoring it */
        this.mc = mc;
    }

    public draw(ctx) {
        this.crosshair.draw(ctx);
    }

    public getPos() {
        return this.crosshair.getPos();
    }

    handleGamePad() {

        this.controler.poll();

        if (this.controler.isButtonClicked(Button.A)) {
            console.log(this.playerIndex, " pressed A button");

            
            if (this.canFire) {

                this.canFire = false;

                setTimeout(() => {
                    this.canFire = true;
                }, Player.TIME_BETWEEN_FIRE);
                this.mc.shoot(this.playerIndex);
            }
        }

        const cord = this.controler.getLefJoystick();

        this.crosshair.move(cord);
    }
}

class Crosshair {

    private static readonly SIZE = 5;
    private static readonly SPEED = 12;

    private x;
    private y;

    private maxX;
    private maxY;

    private color: string;

    constructor(canvasWidth: number, canvasHeight: number, color: string) {
        this.x = canvasWidth / 2;
        this.y = canvasHeight / 2;

        this.maxX = canvasWidth;
        this.maxY = canvasHeight;

        this.color = color;
    }

    public move(input: JoyCord) {
        this.x += input.x * Crosshair.SPEED;
        this.y += input.y * Crosshair.SPEED;

        this.x = Math.max(this.x, 0);
        this.x = Math.min(this.x, this.maxX);

        this.y = Math.max(this.y, 0);
        this.y = Math.min(this.y, this.maxY);
    }

    public draw(ctx) {
        ctx.beginPath();
        ctx.moveTo(this.x - Crosshair.SIZE, this.y);
        ctx.lineTo(this.x + Crosshair.SIZE, this.y);

        ctx.moveTo(this.x, this.y - Crosshair.SIZE);
        ctx.lineTo(this.x, this.y + Crosshair.SIZE);

        ctx.strokeStyle = this.color;
        ctx.stroke();
    }

    public getPos() {
        return { x: this.x, y: this.y };
    }
}

/*
 * Based on https://github.com/patrickhliu/missile-command
 * Translated into some kindof Typescript by Leon.
 */
export class MissileCommand {

    private static readonly MAX_TOWERS = 20;
    private static readonly TOWER_WIDTH = 20;
    private static readonly TOWER_MAX_HEIGHT = 45;
    private static readonly TOWER_MIN_HEIGHT = 20;

    private static readonly START_Y = 446;

    private static readonly PLAYER_MISSILE_SPEED = 1.8;
    private static readonly MINIMUM_ENEMY_MISSILE_SPEED = 0.1;
    private static readonly RAND_VARIANCE_ENEMY_MISSILE_SPEED = 0.15;

    private canvas;
    private ctx;

    private score = 0;

    private level = { id: -1, enemyMissileCount: 8, speed: 0.5 };       // object with properties for each round

    private reqAnimFrame;

    private enemyShootMissileArr = [];                                      // one array to hold enemy missiles (js objects)
    private gameOn = false;                                                 // boolean flag to indicate if game has started or not
    private missileColors = ['red', 'blue', 'green',
        'yellow', 'orange', 'purple',
        'white', 'brown'];

    // blueCities is an array containing parameters that will be passed to a ctx.Rect() method to draw the 6 blue cities on screen
    // and also for collision detection if an enemy missile hits a blue city
    private blueCities = [];

    // yellowMissileRect has an object of parameters for each of the 3 yellow missile bases.  
    // the parameters will be used to detect if an enemy missile has hit a yellow missile base.
    private yellowMissileRect = [
        { x: 20, y: MissileCommand.START_Y, width: 40, height: 20, middleX: 40, middleY: 456 }, // yellow 0
        { x: 540, y: MissileCommand.START_Y, width: 40, height: 20, middleX: 560, middleY: 456 }, // yellow 1
    ];

    private remMissileLoc = { left: [], right: [] };  // array holds coordinates for remaining missiles to draw
    private playerShootMissileArr = { left: [], right: [] };

    private completeCallback: (levelId: number, success: boolean) => void;

    private players: Player[];

    private statusText: string;

    

    private adjustableEnemySpeed = 0;

    constructor(completeCallback: (levelId: number, success: boolean) => void) {
        this.completeCallback = completeCallback;
        this.canvas = document.querySelector('canvas');
        this.ctx = this.canvas.getContext('2d');

        this.players = [ new Player(this, 0, this.canvas.width, this.canvas.height, 'yellow'), 
            new Player(this, 1, this.canvas.width, this.canvas.height, 'red') ];

        this.statusText = "Waiting for downlink...";

        this.reqAnimFrame = window.requestAnimationFrame;

        this.refillMissiles();                                       // draw all 30 missiles + set fired counter to 0 at start of game

        const self = this;
        // every 33.33 milliseconds, these functions will run
        setInterval(function () {

            self.ctx.fillStyle = 'black';
            self.ctx.fillRect(0, 0, self.canvas.width, self.canvas.height);

            if (self.gameOn) {
                self.handleGamePad();

                for (const player of self.players) {
                    player.draw(self.ctx);
                }

                self.drawHills();
                self.drawCities();
                self.drawMissileInv(null, null);
            }
            else {
                self.ctx.font = "30px Arial";
                self.ctx.fillStyle = "red";
                self.ctx.textAlign = "center";
                self.ctx.fillText(self.statusText, self.canvas.width / 2, self.canvas.height / 2);
            }

        }, 1000 / 30);
        setInterval(function () {
            self.checkGameStatus();
        }, 100);
    }

    shoot(player: number) {


            const playerDestX = this.players[player].getPos().x;
            const playerDestY = this.players[player].getPos().y;
            const playerOriginY = MissileCommand.START_Y;

            const self = this;
            const fire = function (missileBattery, playerOriginX) {
                // get parameters for player's missile
                const playerShootMissileParam = self.getMissileParams(playerDestX, playerDestY, playerOriginX, playerOriginY, true, 10);


                if (self.hasMissiles(missileBattery) && self.gameOn) {
                    const playerShootMissile = new self.Missile(self, playerDestX, playerDestY,
                        playerShootMissileParam.xStep, playerShootMissileParam.yStep,
                        playerOriginX, playerOriginY, true);
                    self.playerShootMissileArr[missileBattery].push(playerShootMissile);
                    self.drawMissileInv(missileBattery, self.playerShootMissileArr[missileBattery].length);
                    self.playerShootMissileArr[missileBattery][self.playerShootMissileArr[missileBattery].length - 1].animate();
                }
            };

            if (player === 0) {

                if (this.hasMissiles('left')) {
                    fire('left', 40);
                }
            }
            else {
                if (this.hasMissiles('right')) {
                    fire('right', 560);
                }
            }
    }

    handleGamePad() {
        for (const player of this.players) {
            player.handleGamePad();
        }
    }

    hasMissiles(missileBattery) {
        // <10 means player still has remaining missiles to be fired
        return this.playerShootMissileArr[missileBattery].length < 10;
    }

    // player has a total of 30 missiles.  remMissileLoc is a 2D array, 3 arrays of 10 objects each.
    // each object contains a starting coordinate of where each missile will be drawn on screen.
    refillMissiles() {
        this.remMissileLoc = {
            left: [
                { num: 10, x: 40, y: 447 },
                { num: 9, x: 31, y: 452 },
                { num: 8, x: 49, y: 452 },
                { num: 7, x: 22, y: 457 },
                { num: 6, x: 40, y: 457 },
                { num: 5, x: 58, y: 457 },
                { num: 4, x: 13, y: 462 },
                { num: 3, x: 31, y: 462 },
                { num: 2, x: 49, y: 462 },
                { num: 1, x: 67, y: 462 }
            ],

            right: [
                { num: 10, x: 560, y: 447 },
                { num: 9, x: 551, y: 452 },
                { num: 8, x: 569, y: 452 },
                { num: 7, x: 542, y: 457 },
                { num: 6, x: 560, y: 457 },
                { num: 5, x: 578, y: 457 },
                { num: 4, x: 533, y: 462 },
                { num: 3, x: 551, y: 462 },
                { num: 2, x: 569, y: 462 },
                { num: 1, x: 587, y: 462 }
            ]
        };

        this.playerShootMissileArr = { left: [], right: [] };
    }

    private startGame() {
        this.gameOn = true;

        this.blueCities = [{
            x: 92, y: MissileCommand.START_Y, width: (600 - 2 * 92), height: 20, middleX: 300, middleY: 456,
            towers: []
        }];

        this.generateTowers();

        this.resetEnemyMissiles();

        for (let k = 0; k < this.level.enemyMissileCount; k++) {
            this.drawEnemyMissile(this.level.speed);
        }

        this.refillMissiles();

    }

    private resetEnemyMissiles() {
        for (let b = 0; b < this.enemyShootMissileArr.length; b++) {
            this.enemyShootMissileArr[b].scaleX = 'OFF';
            this.enemyShootMissileArr[b].scaleY = 'OFF';
        }

        this.enemyShootMissileArr = [];
    }

    private explodeEnemyMissiles() {
        for (let b = 0; b < this.enemyShootMissileArr.length; b++) {
            this.enemyShootMissileArr[b].explode();
            this.enemyShootMissileArr[b].scaleX = 'OFF';
            this.enemyShootMissileArr[b].scaleY = 'OFF';
        }
    }

    setAdjustedEnemyMissileSpeed(speed: number) {
        this.adjustableEnemySpeed = speed;
    }

    startLevel(levelId: number, numMisiles: number, speed: number) {
        this.level.id = levelId;
        this.level.enemyMissileCount = numMisiles;
        this.level.speed = speed;

        this.startGame();
    }

    stopGame() {
        this.gameOn = false;
        this.statusText = "";

        this.resetEnemyMissiles();
    }

    generateTowers() {

        for (let j = 0; j < this.blueCities.length; j++) {

            const towers = [];

            for (let i = 0; i < MissileCommand.MAX_TOWERS; i++) {
                const width = MissileCommand.TOWER_WIDTH;
                const height = this.randNum(MissileCommand.TOWER_MIN_HEIGHT, MissileCommand.TOWER_MAX_HEIGHT);
                const tower = {
                    x: this.randNum(this.blueCities[j].x, this.blueCities[j].x + this.blueCities[j].width - width),
                    width: width,
                    height: height,
                    y: (MissileCommand.START_Y - height)
                };

                towers.push(tower);
            }

            this.blueCities[j].towers = towers;
        }

    }

    randNum(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    checkGameStatus() {

        if (this.blueCities.length > 0 && this.gameOn && this.enemyShootMissileArr.length === 0) {
            /* Level succeeded */
            if (this.completeCallback) {
                this.completeCallback(this.level.id, true);
                this.statusText = "";
            }

            this.gameOn = false;
        }
        else if (this.blueCities.length === 0 && this.gameOn) {
            /* Level failed */
            if (this.completeCallback) {
                this.completeCallback(this.level.id, false);
                this.statusText = "";
            }
            
            this.gameOn = false;
        }
    }

    drawCities() {
        for (let j = 0; j < this.blueCities.length; j++) {
            this.ctx.beginPath();
            this.ctx.rect(this.blueCities[j].x, this.blueCities[j].y, this.blueCities[j].width, this.blueCities[j].height);

            for (let k = 0; k < this.blueCities[j].towers.length; k++) {
                this.ctx.rect(this.blueCities[j].towers[k].x, this.blueCities[j].towers[k].y,
                    this.blueCities[j].towers[k].width, this.blueCities[j].towers[k].height);
            }
            this.ctx.fillStyle = '#24FFE5';
            this.ctx.fill();
        }
    }

    // this function draws the 2 yellow missiles bases 
    drawHills() {
        this.ctx.beginPath();
        this.ctx.moveTo(0, 466);
        this.ctx.lineTo(20, 446);
        this.ctx.lineTo(60, 446);
        this.ctx.lineTo(80, 466);
        this.ctx.lineTo(520, 466);
        this.ctx.lineTo(540, 446);
        this.ctx.lineTo(580, 446);
        this.ctx.lineTo(600, 466);
        this.ctx.closePath();
        this.ctx.fillStyle = "yellow";
        this.ctx.fill();

        this.ctx.beginPath();
        this.ctx.rect(0, 466, 600, 20);
        this.ctx.fillStyle = "yellow";
        this.ctx.fill();
    }

    // this function is given a (x,y) coordinate and draws all missiles remaining in player's inventory.
    drawRemMissile(x, y, color) {
        this.ctx.beginPath();
        this.ctx.moveTo(x, y);
        this.ctx.lineTo(x, y + 5);
        this.ctx.lineTo(x + 3, y + 5);
        this.ctx.lineTo(x + 3, y + 10);
        this.ctx.strokeStyle = color;
        this.ctx.stroke();

        this.ctx.beginPath();
        this.ctx.moveTo(x, y);
        this.ctx.lineTo(x, y + 5);
        this.ctx.lineTo(x - 3, y + 5);
        this.ctx.lineTo(x - 3, y + 10);
        this.ctx.strokeStyle = color;
        this.ctx.stroke();
    }

    // this function iterates through the remMissileLoc array and calls drawRemMissile to canvas draw missiles left for the player
    drawMissileInv(arg_location, arg_numMissile) {

        /* if no location argument is providing at function call...
         * this is done by the setInterval function way above...
         * this basically draws all 30 missiles at beginning 
         * remaining missiles as the game progresses 
         */
        if (!arg_location) {
            Object.keys(this.remMissileLoc).forEach((h: string) => {
                for (let i = 0; i < this.remMissileLoc[h].length; i++) {
                    this.drawRemMissile(this.remMissileLoc[h][i].x, this.remMissileLoc[h][i].y, 'red');
                }
            });
        }
        else {
            /* else when a location is provided, this happens when player fires a missile
             * iterate through remMissileLoc
             * 1st missile fired = remMissileLoc[9] (10th element); 2nd missile fired = remMissileLoc[8] (9th element)
             * remove that element from remMissileLoc since user fired it.  Once removed, it won't be re-drawn.
             */
            for (let g = 0; g < this.remMissileLoc[arg_location].length; g++) {
                if (this.remMissileLoc[arg_location][g].num === arg_numMissile) {
                    this.remMissileLoc[arg_location].splice(10 - arg_numMissile, 1);
                }
            }
        }
    }

    // at each level start, this function is called to draw each individual enemy missile
    drawEnemyMissile(arg_speed) {             // speed increases each level, so it's needed in function.
        const enemyOriginX = this.randNum(0, 600);        // origin X
        const enemyOriginY = 0;                           // origin Y will always start at 0
        const enemyDestX = this.randNum(0, 600);          // destination X is from our random number generator
        const enemyDestY = 446;                           // destination Y is 446, this is the top of the yellow missile bases

        let missileSpeed = Number.parseFloat(arg_speed) + this.adjustableEnemySpeed + 
        Math.random() * MissileCommand.RAND_VARIANCE_ENEMY_MISSILE_SPEED * 2 - MissileCommand.RAND_VARIANCE_ENEMY_MISSILE_SPEED;

        if (missileSpeed < MissileCommand.MINIMUM_ENEMY_MISSILE_SPEED) {
            missileSpeed = MissileCommand.MINIMUM_ENEMY_MISSILE_SPEED;
        }

        console.log("Using missile speed=", missileSpeed);

        // getMissileParams function will return object filled with missile trajectory parameters
        // arguments are origin & destination coordinates, false to indicate this is an enemy missile, and the speed needed
        const enemyMissileParam = this.getMissileParams(enemyDestX, enemyDestY, enemyOriginX, enemyOriginY, false, missileSpeed);

        // create a new missile using the Missile constructor
        // arguments are destination (x,y); step(x,y) which is an amount to draw from origin(x,y) by incrementally;
        // origin (x,y);  and a false to indicate this is an enemy missile
        const enemyShootMissile = new this.Missile(this, enemyDestX, enemyDestY, enemyMissileParam.xStep,
            enemyMissileParam.yStep, enemyOriginX, enemyOriginY, false);

        // push the new enemy missile object into the array.  Array lets us know how many enemy missiles remain on screen.  
        // once enemy missile array is empty, we can move on to the next round
        this.enemyShootMissileArr.push(enemyShootMissile);

        // call the animate method of the newest enemy missile generated.  
        // The animate method is what draws the green enemy missiles on screen
        this.enemyShootMissileArr[this.enemyShootMissileArr.length - 1].animate();
    }

    // getMissileParams calculates math variables needed to help make the traveling missile appear smoothly on screen
    getMissileParams(arg_xDest, arg_yDest, arg_xOrig, arg_yOrig, arg_playerFlag, arg_speed) {
        const params = { distance: 0, xDist: 0, yDist: 0, step: 0, xStep: 0, yStep: 0 };    // start with empty object to store our props

        // pythagorean theorm to find distance between origin and destination
        params.distance = Math.sqrt(Math.pow((arg_xDest - arg_xOrig), 2) + Math.pow((arg_yDest - arg_yOrig), 2));

        // variable to store the x distance from origin to destination
        params.xDist = Math.abs(arg_xDest - arg_xOrig);

        // variable to store the y distance from origin to destination
        params.yDist = Math.abs(arg_yDest - arg_yOrig);

        // ternary statement.  If it's a player missile, step is calculated by dividing by 10, 
        // player missiles always move at same speed throughout the game.
        // If it's an enemy missile, step is calculated using whatever speed is needed for the current level.
        // step is the amount to further draw a missile line.  We'll draw on canvas starting at 
        // origin to point 1, then origin to point 2, then origin to point 3,etc....
        // each time incrementing by the value of step and continue drawing until destination (x,y) is reached.
        arg_playerFlag ? params.step = params.distance / MissileCommand.PLAYER_MISSILE_SPEED : params.step = params.distance / arg_speed;

        // set x-direction step based on the ternary above
        params.xStep = params.xDist / params.step;

        // set y-direction step based on the ternary above
        params.yStep = params.yDist / params.step;

        // return this object of parameters so they can be used to draw on canvas
        return params;
    }

    // this function checks if a player missile has hit an enemy missile.
    // this function is called as the player missile is in the exploding phase. (this means as the radius increasing from 0->30)
    // argument is the canvas context of the exploding player missile
    checkPlayerHitEnemy(arg_ctx) {
        /* iterate through the array of enemy missile objects
         * if the current (x,y) of that enemy missile is within our exploding circle
         * set the scaleX & Y (this would be stepX & Y) to string 'off'
         * 'off' will cause the drawing of enemy missile to stop     
         */
        for (let b = 0; b < this.enemyShootMissileArr.length; b++) {
            if (arg_ctx.isPointInPath(this.enemyShootMissileArr[b].scaleX, this.enemyShootMissileArr[b].scaleY)) {
                this.enemyShootMissileArr[b].scaleX = 'OFF';
                this.enemyShootMissileArr[b].scaleY = 'OFF';

                this.enemyShootMissileArr.splice(b, 1);

                this.score += 10;                                                // update score (player gets 10 pts / enemy missile)
            }
        }
    }

    private removeBase(index: number) {
        this.remMissileLoc[this.indexToBase(index)] = [];
    }

    private indexToBase(index: number) {
        switch (index) {
            case 0: return 'left';
            case 1: return 'right';
        }

        return 'left';
    }
    // this function checks if an enemy missile has hit either a blue city / yellow missile base
    // arguments are: the array to check (blueCities / yellowMissileRect)
    // destination (x,y) of the enemy missile
    // radius of the explosion of the enemy missile
    checkEnemyHitBase(arg_Arr, arg_destX, arg_destY, arg_radius) {
        for (let c = 0; c < arg_Arr.length; c++) {                      // iterate through the given array
            const xDist = Math.abs(arg_destX - arg_Arr[c].middleX);
            const yDist = Math.abs(arg_destY - arg_Arr[c].middleY);

            if ((xDist > arg_radius + (arg_Arr[c].width / 2)) && (yDist > arg_radius + (arg_Arr[c].height / 2))) {  // if this, do nothing
                // do nothing
            }
            else if (xDist <= (arg_Arr[c].width / 2) && yDist <= (arg_Arr[c].height / 2)) {
                // and this... a collision occured

                // if enemy missile hit blue city
                if (arg_Arr === this.blueCities) {
                    // remove city from array so it won't be re-drawn
                    this.blueCities.splice(c, 1);
                }
                else if (arg_Arr === this.yellowMissileRect) {
                    // if enemy missile hit yellow missile base

                    // remove that yellow missile base, this doesn't do anything but left for consistency
                    this.yellowMissileRect.splice(c, 1);

                    // remove that yellow missile base, here the base won't be re-drawn
                    this.removeBase(c);
                    this.playerShootMissileArr[this.indexToBase(c)].length = 20;
                }
            }
        }
    }

    // giant missile constructor, this instantiates an object for both player & enemy missiles
    // arguments: destination(x,y), step amount for (x,y), origin (x,y), and a flag indicating whether this is a player/enemy missile
    private Missile = function (parent, arg_xDest, arg_yDest, arg_xStep, arg_yStep, arg_xOrig, arg_yOrig, arg_playerFlag) {
        this.originX = arg_xOrig;
        this.originY = arg_yOrig;
        this.scaleX = this.originX;
        this.scaleY = this.originY;
        this.destX = arg_xDest;
        this.destY = arg_yDest;
        this.radius = 0;

        /* Make sure that the  missile can't go in a vertical line, so we can skip that case */
        if (this.destX === this.originX) {
            this.destX += 1;
        }

        this.deplode = function () {
            /* radius starts at 0 for the missile explosion
             * this function starts the missile's deplosion animation if radius hasn't reached 0, keep de-ploding... 
             * draw the de-plosion animation with current radius
             */
            if (this.radius > 0) {
                parent.ctx.beginPath();
                parent.ctx.arc(this.destX, this.destY, this.radius, 0, 2 * Math.PI, false);
                parent.ctx.fillStyle = parent.missileColors[parent.randNum(0, parent.missileColors.length)];
                parent.ctx.fill();
                this.radius -= 2;         // decrement radius
                parent.reqAnimFrame(this.deplode.bind(this));   // draw next de-plosion circle at next browser frame
            }
            else if (this.radius === 0) {
                if (!arg_playerFlag) { // enemy missiles with radius 0...
                    for (let d = 0; d < parent.enemyShootMissileArr.length; d++) {
                        if (parent.enemyShootMissileArr[d].originX === this.originX &&
                            parent.enemyShootMissileArr[d].originY === this.originY &&
                            parent.enemyShootMissileArr[d].destX === this.destX &&
                            parent.enemyShootMissileArr[d].destY === this.destY) {

                            parent.enemyShootMissileArr.splice(d, 1);
                        }
                    }
                }
            }
        };
        // this function starts a missile's explosion animation
        this.explode = function () {
            if (this.radius === 30) { // max radius is 30, once it's reach start the deplode animation
                this.deplode();
            }
            else {   // else branch means radius is still increasing from 0 --> 30
                parent.ctx.beginPath();

                // explosion circle is drawing w/ destination(x,y) as center
                parent.ctx.arc(this.destX, this.destY, this.radius, 0, 2 * Math.PI, false);

                // draw with a random color
                parent.ctx.fillStyle = parent.missileColors[parent.randNum(0, parent.missileColors.length)];
                parent.ctx.fill();

                if (arg_playerFlag) {   // if this is a player missile...
                    parent.checkPlayerHitEnemy(parent.ctx);  // check if an exploding player missile hits any enemy missiles.
                }
                else if (!arg_playerFlag) {  // if this is an enemy missile...
                    // check if a blue city was hit
                    parent.checkEnemyHitBase(parent.blueCities, this.destX, this.destY, this.radius);

                    // check if a yellow missile base was hit      
                    parent.checkEnemyHitBase(parent.yellowMissileRect, this.destX, this.destY, this.radius);
                }
                this.radius += 2; // increment radius by 2

                // re-draw next explosion circle with bigger radius at next browser frame
                parent.reqAnimFrame(this.explode.bind(this));
            }
        };
        // animate method draws the path of the missile
        this.animate = function () {

            const self = this;
            const drawLine = function (xStep) {
                parent.ctx.beginPath();   // draw the line
                parent.ctx.moveTo(self.originX, self.originY);
                parent.ctx.lineTo(self.scaleX, self.scaleY);
                parent.ctx.lineWidth = 2;
                arg_playerFlag ? parent.ctx.strokeStyle = 'red' : parent.ctx.strokeStyle = '#25F53D';
                parent.ctx.stroke();
                self.scaleX += xStep;
                arg_playerFlag ? self.scaleY -= arg_yStep : self.scaleY += arg_yStep;
                parent.reqAnimFrame(self.animate.bind(self));
            };

            if (this.destX > this.originX) {
                if (this.scaleX < this.destX) {
                    drawLine(arg_xStep);
                }
                else if (this.scaleX !== 'OFF') {
                    this.explode();
                }
            }
            else {
                if (this.scaleX > this.destX) {
                    drawLine(-arg_xStep);
                }
                else if (this.scaleX !== 'OFF') {
                    this.explode();
                }
            }
        };
    };
}

