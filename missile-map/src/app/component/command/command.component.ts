import { Component, OnInit } from '@angular/core';

import { MissileCommand } from './missile-command';
import { CommService } from '../../comm.service';
import { cities, City, CityStatus } from '../../model/city';

@Component({
  selector: 'app-command',
  templateUrl: './command.component.html',
  styleUrls: ['./command.component.scss']
})
export class CommandComponent implements OnInit {

  private static readonly START_LEVEL_AFTER = 3;

  /* Hack to now show this component until a set time after server has ordered startup,
   * a proper way of doing it would be to send an event after the starting video is done.
   */
  private static readonly WAIT_AFTER_STARTUP = 1; // 20000;

  private mc: MissileCommand;

  cityList = Array.from(cities.values()).sort((one, two) => one.speedInMinutes - two.speedInMinutes);
  color = 'yellow';
  currentCityName = "";
  statusText = "";
  interval: any;
  secondsToStart = CommandComponent.START_LEVEL_AFTER;

  started = false;
  levelRunning = false;

  constructor(private commService: CommService) { }

  getStatusClass(city: City) {
    if (!this.started) {
      return { 'status-hide': true };
    }
    else if (city.status === CityStatus.Destroyed) {
      return { 'status-destroyed': true };
    }
    else if (city.status === CityStatus.Saved) {
      return { 'status-saved': true };
    }
    else if (city.status === CityStatus.UnderAttack) {
      return { 'status-pending': true };
    }

    return { 'status-upcomming': true };
  }

  startLevel(levelId: number, numMissiles: number, speed: number) {
    console.log("Starting level id=", levelId);
    this.statusText = "";
    this.mc.startLevel(levelId, numMissiles, speed);
  }

  schedlueLevel(city: City) {
    if (!this.levelRunning) {
      this.levelRunning = true;
      this.currentCityName = city.name;
      this.secondsToStart = CommandComponent.START_LEVEL_AFTER;
      this.statusText = "" + this.secondsToStart;

      city.status = CityStatus.UnderAttack;

      this.statusText = "";
      this.color = 'yellow';

      this.interval = setInterval(() => {
        this.secondsToStart--;
        this.statusText = "" + this.secondsToStart;

        if (this.secondsToStart <= 0) {
          clearInterval(this.interval);
          this.startLevel(city.id, city.missiles, city.missileSpeed);
        }
      }, 1000);
    }
    else {
      /* If the missile command game is bussy, the city is simply destroyed */
      city.status = CityStatus.Destroyed;
      this.commService.sendLevel(city.id);
    }
  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);

    /* The maximum is exclusive and the minimum is inclusive */
    return Math.floor(Math.random() * (max - min)) + min;
  }

  start() {
    if (!this.started) {

      this.started = true;

      this.mc = new MissileCommand((levelId: number, success: boolean) => {
        console.log("Yey! Its over!", levelId, success);

        this.levelRunning = false;

        const city = cities.get(levelId);

        if (success) {
          city.status = CityStatus.Saved;
          this.color = 'green';
          this.statusText = this.currentCityName + " has been saved!";
          this.commService.sendMap(levelId);

          this.commService.sendPlayVideo('def' + this.getRandomInt(1, 5));
        }
        else {
          city.status = CityStatus.Destroyed;
          this.color = 'red';
          this.statusText = this.currentCityName + " has been destroyed!";
          this.commService.sendLevel(levelId);
        }
      });

      const observable = this.commService.onGame();

      observable.subscribe((m: number) => {
        console.log("Got message from server", m);

        const city = cities.get(m);

        this.schedlueLevel(city);
      });

      const missileSpeedObserver = this.commService.onAdjustMissleSpeed();

      missileSpeedObserver.subscribe((m: number) => {
        console.log("Got adjusted missile speed from server from server", m);
        this.mc.setAdjustedEnemyMissileSpeed(m);
      });

      const selfDestructObserver = this.commService.onSelfDestruct();

      selfDestructObserver.subscribe((m: number) => {
        console.log("Got selfdestruct from server", m);

        this.currentCityName = "";
        this.statusText = "";
        this.mc.stopGame();

        /* All remaning cities are safe */
        cities.forEach((city) => {
          if (city.status !== CityStatus.Destroyed) {

            /* Any cities under attack should clear the mark fromt he map */
            if (city.status === CityStatus.UnderAttack) {
              this.commService.sendMap(city.id);
            }

            city.status = CityStatus.Saved;
          }
        });
      });
    }
  }
  ngOnInit() {
    const startObservable = this.commService.onStart();

    startObservable.subscribe((m: number) => {
      console.log("Got start from server", m);
      this.interval = setInterval(() => {
        this.start();
        clearInterval(this.interval);
      }, CommandComponent.WAIT_AFTER_STARTUP);
    });
  }
}
