import { Component, OnInit } from '@angular/core';
import { CommService } from '../../comm.service';
import { ActivatedRoute } from '@angular/router';
import { MessageChannel } from '../../shared-map/message-channels';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.scss']
})
export class ControlComponent implements OnInit {

  path = "control";

  start() {
    this.commService.sendStart();
  }

  setMissleSpeed(speed: string) {
    this.commService.sendAdjustMissileSpeed(Number.parseFloat(speed));
  }

  setMapSpeed(speed: string) {
    this.commService.sendAdjustMapSpeed(1.0 + Number.parseFloat(speed));
  }

  selfDestruct() {
    this.commService.send(MessageChannel.SELF_DESTRUCT, "");
  }

  play(name: string) {
    this.commService.sendPlayVideo(name);
  }

  constructor(private commService: CommService, private route: ActivatedRoute, private titleService: Title) { }

  ngOnInit() {
    this.route.url.subscribe(url => { 
      this.path = url[0].path; 
      this.titleService.setTitle(this.path.replace("control-", "").toUpperCase()); 
    });
  }

}
