import { Component, OnInit } from '@angular/core';
import { CommService } from '../../comm.service';

import { cities } from '../../model/city';

declare let L;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  migrationLayer: any;
  visible = false;

  startTime: number;

  killAll(): void {
    this.migrationLayer.killAllNotDone();
  }

  kill(id: number): void {
    this.migrationLayer.kill(id);
  }

  constructor(private commService: CommService) {
  }

  start() {
    if (!this.visible) {

      this.startTime = new Date().getTime();

      this.visible = true;
      const lrmap = L.map('map', {
        minZoom: 3,
        maxZoom: 3,
      }).setView([20, 0], 3);

      L.tileLayer('https://{s}.basemaps.cartocdn.com/dark_nolabels/{z}/{x}/{y}{r}.png', {
        attribution: ''
      }).addTo(lrmap);

      lrmap.fitWorld().zoomIn();


      const data = Array.from(cities.values());
      const self = this;

      this.migrationLayer = new L.migrationLayer({
        map: lrmap,
        data: data,
        pulseRadius: 30,
        pulseBorderWidth: 3,
        arcWidth: 1,
        arcLabel: true,
        arcLabelFont: '10px sans-serif',
        maxWidth: 10,
        speedMinusMinutes: 0
      },
        function (id: number) {
          if (id) {
            self.commService.sendGame(id);

            const time = (new Date().getTime() - self.startTime) / 1000 / 60;
            console.log("Play game id=", id, time);
          }
        }
      );
      this.migrationLayer.addTo(lrmap);
      this.migrationLayer.play();

      const observable = this.commService.onMap();

      observable.subscribe((m: number) => {
        console.log("Got message from server", m);
        this.kill(m);
      });

      const mapSpeedObserver = this.commService.onAdjustMapSpeed();

      mapSpeedObserver.subscribe((m: number) => {
        console.log("Got adjusted map speed from server", m);
        this.migrationLayer.setSpeedAdjustment(m);
      });

      const mapSelfDestructObserver = this.commService.onSelfDestruct();

      mapSelfDestructObserver.subscribe((m: number) => {
        console.log("Got selfdestruct from server", m);
        this.migrationLayer.killAllNotDone();
      });
    }
  }

  ngOnInit() {

    const startObservable = this.commService.onStart();

    startObservable.subscribe((m: number) => {
      console.log("Got start from server", m);
      this.start();
    });
  }
}
