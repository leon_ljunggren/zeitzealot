import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';

import { CommService } from '../../comm.service';
import { cities } from '../../model/city';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  @Input() videoUrl = "";
  @ViewChild('myVideo') myVideo: ElementRef;

  started = false;
  selfDestructed = false;
  ended = false;

  playId(id: number) {
    const city = cities.get(id);

    if (city) {
      this.play(city.video);
    }
  }

  play(name: string) {

    this.videoUrl = "/assets/videos/" + name + ".mp4";

    this.myVideo.nativeElement.load();
    // this.myVideo.nativeElement.play();
  }

  constructor(private commService: CommService) { }

  ngOnInit() {

    this.myVideo.nativeElement.addEventListener('ended', () => {
      console.log("Video is over");
      this.videoUrl = "";
      this.myVideo.nativeElement.load();
    }, false);

    const observable = this.commService.onLevel();

    observable.subscribe((m: number) => {
      console.log("Got message from server", m);
      this.playId(m);

      /* Hack to compare with id of last city to find if it's over */
      if (m === 2) {
        this.ended = true;
      }
    });

    const messagePlayVideoObserver = this.commService.onPlayVideo();

    messagePlayVideoObserver.subscribe((name: string) => {
      console.log("Got playVideo from server", name);
        this.play(name);
    });

    const messageSelfDestructObserver = this.commService.onSelfDestruct();

    messageSelfDestructObserver.subscribe((m: number) => {
      console.log("Got selfdestruct from server", m, this.selfDestructed);
      if (!this.selfDestructed && !this.ended) {
        this.selfDestructed = true;
        this.play("selfdestruct");
      }
    });

    const startObservable = this.commService.onStart();

    startObservable.subscribe((m: number) => {
      console.log("Got start from server", m, this.started);
      if (!this.started) {
        this.started = true;
        this.play("start");
      }
    });
  }
}
