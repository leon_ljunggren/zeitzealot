import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapComponent } from './component/map/map.component';
import { MessageComponent } from './component/message/message.component';
import { CommandComponent } from './component/command/command.component';
import { ControlComponent } from './component/control/control.component';
import { CommandTestComponent } from './component/command-test/command-test.component';

const routes: Routes = [{ path: '', component: MapComponent }, 
{ path: 'message', component: MessageComponent }, 
{ path: 'command', component: CommandComponent }, 
{ path: 'command-test', component: CommandTestComponent }, 
{ path: 'control-video', component: ControlComponent }, 
{ path: 'control-final', component: ControlComponent }, 
{ path: 'control-end', component: ControlComponent }, 
{ path: 'control-intro', component: ControlComponent }];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
