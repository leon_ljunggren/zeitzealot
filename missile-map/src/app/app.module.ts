import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeNb from '@angular/common/locales/nb';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { environment } from '../environments/environment';

const config: SocketIoConfig = { url: environment.api, options: {} };

import { AppComponent } from './app.component';
import { MapComponent } from './component/map/map.component';
import { MessageComponent } from './component/message/message.component';
import { AppRoutingModule } from './app-routing.module';
import { CommandComponent } from './component/command/command.component';
import { ControlComponent } from './component/control/control.component';
import { CommandTestComponent } from './component/command-test/command-test.component';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    MessageComponent,
    CommandComponent,
    ControlComponent,
    CommandTestComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    SocketIoModule.forRoot(config)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor() {
    registerLocaleData(localeNb, 'nb');
  }
}
