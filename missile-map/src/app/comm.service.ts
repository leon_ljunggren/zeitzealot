import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs';

import { MessageChannel } from './shared-map/message-channels';

@Injectable({
  providedIn: 'root'
})
export class CommService {

  constructor(private socket: Socket) { }

  public sendPlayVideo(name: string) {
    this.send(MessageChannel.PLAY_VIDEO, name);
  }

  public onPlayVideo(): Observable<string|number> {
    return this.onMessage(MessageChannel.PLAY_VIDEO);
  }

  public sendLevel(id: number) {
    this.send(MessageChannel.LEVEL, id);
  }

  public onLevel(): Observable<string|number> {
    return this.onMessage(MessageChannel.LEVEL);
  }

  public sendGame(id: number) {
    this.send(MessageChannel.GAME, id);
  }

  public onGame(): Observable<string|number> {
    return this.onMessage(MessageChannel.GAME);
  }

  public sendMap(id: number) {
    this.send(MessageChannel.MAP, id);
  }

  public onMap(): Observable<string|number> {
    return this.onMessage(MessageChannel.MAP);
  }

  public sendAdjustMissileSpeed(speed: number) {
    this.send(MessageChannel.ADJUST_MISSILE_SPEED, speed);
  }

  public onAdjustMissleSpeed(): Observable<string|number> {
    return this.onMessage(MessageChannel.ADJUST_MISSILE_SPEED);
  }

  public sendAdjustMapSpeed(speed: number) {
    this.send(MessageChannel.ADJUST_MAP_SPEED, speed);
  }

  public onAdjustMapSpeed(): Observable<string|number> {
    return this.onMessage(MessageChannel.ADJUST_MAP_SPEED);
  }

  public sendStart() {
    this.send(MessageChannel.START, "");
  }

  public onStart(): Observable<string|number> {
    return this.onMessage(MessageChannel.START);
  }

  public onSelfDestruct(): Observable<string|number> {
    return this.onMessage(MessageChannel.SELF_DESTRUCT);
  }

  public send(channel: MessageChannel, message: string|number) {
    this.socket.emit(channel, message);
  }

  public onMessage(channel: MessageChannel): Observable<string|number> {
    return this.socket.fromEvent(channel);
  }
}
