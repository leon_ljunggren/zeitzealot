export enum CityStatus {
    AwaitingAttack,
    UnderAttack,
    Destroyed,
    Saved
}

export interface City {
    id: number;
    name: string;
    video: string;
    from: [number, number];
    to: [number, number];
    labels: [string, string];
    color: string;
    speedInMinutes: number;
    missiles: number;
    missileSpeed: number;
    status: CityStatus;
}

export const cities = new Map<number, City>();


cities.set(39, {
    id: 39, name: "Vienna", video: "generic", "from": [32.810948, 104.975137], "to": [48.190854, 16.373187],
    "labels": [null, "Vienna"], "color": "#ff3a31", speedInMinutes: 40, missiles: 12, missileSpeed: 0.65, 
    status: CityStatus.AwaitingAttack
});
cities.set(38, {
    id: 38, name: "Kyiv", video: "generic", "from": [32.810948, 104.975137], "to": [50.469721, 30.513389],
    "labels": [null, "Kyiv"], "color": "#ff3187", speedInMinutes: 39, missiles: 12, missileSpeed: 0.65, 
    status: CityStatus.AwaitingAttack
});
cities.set(37, {
    id: 37, name: "Warsaw", video: "generic", "from": [32.810948, 104.975137], "to": [52.237653, 21.012962],
    "labels": [null, "Warsaw"], "color": "#ff3a31", speedInMinutes: 41, missiles: 12, missileSpeed: 0.65, 
    status: CityStatus.AwaitingAttack
});
cities.set(36, {
    id: 36, name: "Helsinki", video: "generic", "from": [32.810948, 104.975137], "to": [60.164570, 24.929397],
    "labels": [null, "Helsinki"], "color": "#ff3a31", speedInMinutes: 47, missiles: 12, missileSpeed: 0.65, 
    status: CityStatus.AwaitingAttack
});
cities.set(35, {
    id: 35, name: "Stockholm", video: "oslo", "from": [32.810948, 104.975137], "to": [59.337104, 18.054232],
    "labels": [null, "Stockholm"], "color": "#ff7e2b", speedInMinutes: 48, missiles: 12, missileSpeed: 0.65, 
    status: CityStatus.AwaitingAttack
});
cities.set(1, {
    id: 1, name: "Oslo", video: "generic", "from": [32.810948, 104.975137], "to": [59.922340, 10.730255],
    "labels": [null, "Oslo"], "color": "#ff3a31", speedInMinutes: 49, missiles: 13, missileSpeed: 0.7, 
    status: CityStatus.AwaitingAttack
});
cities.set(2, {
    id: 2, name: "Haugesund", video: "fail", "from": [32.810948, 104.975137], "to": [59.414191, 5.270633],
    "labels": [null, "Haugesund"], "color": "#ff7e2b", speedInMinutes: 50, missiles: 21, missileSpeed: 0.8, 
    status: CityStatus.AwaitingAttack
});
cities.set(3, {
    id: 3, name: "New Delhi", video: "generic", "from": [32.810948, 104.975137], "to": [28.608002, 77.207002],
    "labels": [null, "New Delhi"], "color": "#ff3a31", speedInMinutes: 9, missiles: 11, missileSpeed: 0.35, 
    status: CityStatus.AwaitingAttack
});
cities.set(4, {
    id: 4, name: "Sydney", video: "generic", "from": [32.810948, 104.975137], "to": [-33.868780, 151.102479],
    "labels": [null, "Sydney"], "color": "#ff7e2b", speedInMinutes: 27, missiles: 13, missileSpeed: 0.55, 
    status: CityStatus.AwaitingAttack
});
cities.set(5, {
    id: 5, name: "Tokyo", video: "generic", "from": [32.810948, 104.975137], "to": [35.702582, 139.803457],
    "labels": [null, "Tokyo"], "color": "#ff3a31", speedInMinutes: 11, missiles: 11, missileSpeed: 0.45, 
    status: CityStatus.AwaitingAttack
});
cities.set(6, {
    id: 6, name: "Perth", video: "generic", "from": [32.810948, 104.975137], "to": [-31.950192, 115.846596],
    "labels": [null, "Perth"], "color": "#ff7e2b", speedInMinutes: 20, missiles: 12, missileSpeed: 0.55, 
    status: CityStatus.AwaitingAttack
});
cities.set(7, {
    id: 7, name: "Novosibirsk", video: "generic", "from": [32.810948, 104.975137], "to": [55.030866, 82.929202],
    "labels": [null, "Novosibirsk"], "color": "#ff7e2b", speedInMinutes: 13, missiles: 11, missileSpeed: 0.45, 
    status: CityStatus.AwaitingAttack
});
cities.set(8, {
    id: 8, name: "Jakutsk", video: "generic", "from": [32.810948, 104.975137], "to": [62.027641, 129.687630],
    "labels": [null, "Jakutsk"], "color": "#ff3a31", speedInMinutes: 14, missiles: 12, missileSpeed: 0.45, 
    status: CityStatus.AwaitingAttack
});

cities.set(9, {
    id: 9, name: "Buenos Aires", video: "generic", "from": [37.941952, -99.593001], "to": [-34.623150, -58.509175],
    "labels": [null, "Buenos Aires"], "color": "#00ff33", speedInMinutes: 32, missiles: 13, missileSpeed: 0.55, 
    status: CityStatus.AwaitingAttack
});

cities.set(44, {
    id: 44, name: "Stanley", video: "generic", "from": [37.941952, -99.593001], "to": [-51.693281, -57.861643],
    "labels": [null, "Stanley"], "color": "#79ff20", speedInMinutes: 33, missiles: 13, missileSpeed: 0.55, 
    status: CityStatus.AwaitingAttack
});

cities.set(45, {
    id: 45, name: "São Paulo", video: "generic", "from": [37.941952, -99.593001], "to": [-23.654380, -46.737645],
    "labels": [null, "São Paulo"], "color": "#79ff20", speedInMinutes: 31, missiles: 13, missileSpeed: 0.55, 
    status: CityStatus.AwaitingAttack
});

cities.set(10, {
    id: 10, name: "Brasilia", video: "generic", "from": [37.941952, -99.593001], "to": [-15.775712, -47.886625],
    "labels": [null, "Brasilia"], "color": "#e9ff20", speedInMinutes: 25, missiles: 12, missileSpeed: 0.55, 
    status: CityStatus.AwaitingAttack
});
cities.set(11, {
    id: 11, name: "Santiago de Chile", video: "generic", "from": [37.941952, -99.593001], "to": [-33.458049, -70.639465],
    "labels": [null, "Santiago de Chile"], "color": "#ffd220", speedInMinutes: 26, missiles: 12, missileSpeed: 0.55, 
    status: CityStatus.AwaitingAttack
});
cities.set(12, {
    id: 12, name: "Cape Town", video: "generic", "from": [37.941952, -99.593001], "to": [-33.908233, 18.566756],
    "labels": [null, "Cape Town"], "color": "#79ff20", speedInMinutes: 36, missiles: 16, missileSpeed: 0.55, 
    status: CityStatus.AwaitingAttack
});
cities.set(13, {
    id: 13, name: "Sanaá", video: "generic", "from": [37.941952, -99.593001], "to": [15.365805, 44.220888],
    "labels": [null, "Sanaá"], "color": "#e9ff20", speedInMinutes: 38, missiles: 9, missileSpeed: 0.65, 
    status: CityStatus.AwaitingAttack
});

cities.set(14, {
    id: 14, name: "Kinshasa", video: "generic", "from": [37.941952, -99.593001], "to": [-4.417388, 15.289441],
    "labels": [null, "Kinshasa"], "color": "#00ff33", speedInMinutes: 34, missiles: 13, missileSpeed: 0.45, 
    status: CityStatus.AwaitingAttack
});

cities.set(46, {
    id: 46, name: "Windhoek", video: "generic", "from": [37.941952, -99.593001], "to": [-22.565326, 17.064971],
    "labels": [null, "Windhoek"], "color": "#ffd220", speedInMinutes: 35, missiles: 13, missileSpeed: 0.45, 
    status: CityStatus.AwaitingAttack
});
cities.set(15, {
    id: 15, name: "Nairobi", video: "generic", "from": [37.941952, -99.593001], "to": [-1.290049, 36.832948],
    "labels": [null, "Nairobi"], "color": "#79ff20", speedInMinutes: 37, missiles: 16, missileSpeed: 0.35, 
    status: CityStatus.AwaitingAttack
});

cities.set(40, {
    id: 40, name: "Dakar", video: "generic", "from": [37.941952, -99.593001], "to": [14.723972, -17.465997],
    "labels": [null, "Dakar"], "color": "#79ff20", speedInMinutes: 24, missiles: 16, missileSpeed: 0.55, 
    status: CityStatus.AwaitingAttack
});
cities.set(41, {
    id: 41, name: "Lagos", video: "generic", "from": [37.941952, -99.593001], "to": [6.530299, 3.338882],
    "labels": [null, "Lagos"], "color": "#e9ff20", speedInMinutes: 29, missiles: 9, missileSpeed: 0.65, 
    status: CityStatus.AwaitingAttack
});
cities.set(42, {
    id: 42, name: "Niamey", video: "generic", "from": [37.941952, -99.593001], "to": [13.517273, 2.121607],
    "labels": [null, "Niamey"], "color": "#ffd220", speedInMinutes: 19, missiles: 11, missileSpeed: 0.45, 
    status: CityStatus.AwaitingAttack
});
cities.set(43, {
    id: 43, name: "Juba", video: "generic", "from": [37.941952, -99.593001], "to": [4.858980, 31.577360],
    "labels": [null, "Juba"], "color": "#00ff33", speedInMinutes: 30, missiles: 16, missileSpeed: 0.35, 
    status: CityStatus.AwaitingAttack
});


cities.set(16, {
    id: 16, name: "Washington, D.C.", video: "dc", "from": [56.755542, -4.399045], "to": [38.915750, -77.053707],
    "labels": [null, "Washington, D.C."], "color": "#00ccff", speedInMinutes: 10, missiles: 11, missileSpeed: 0.75, 
    status: CityStatus.AwaitingAttack
});
cities.set(17, {
    id: 17, name: "Mexico City", video: "mexico", "from": [56.755542, -4.399045], "to": [19.391916, -99.137431],
    "labels": [null, "Mexico City"], "color": "#2185ff", speedInMinutes: 17, missiles: 14, missileSpeed: 0.65, 
    status: CityStatus.AwaitingAttack
});
cities.set(18, {
    id: 18, name: "Ottawa", video: "generic", "from": [56.755542, -4.399045], "to": [45.306686, -75.734358],
    "labels": [null, "Ottawa"], "color": "#00ff33", speedInMinutes: 12, missiles: 12, missileSpeed: 0.55, 
    status: CityStatus.AwaitingAttack
});
cities.set(19, {
    id: 19, name: "San Francisco", video: "sf", "from": [56.755542, -4.399045], "to": [37.752502, -122.439322],
    "labels": [null, "San Francisco"], "color": "#00ccff", speedInMinutes: 18, missiles: 12, missileSpeed: 0.65, 
    status: CityStatus.AwaitingAttack
});
cities.set(20, {
    id: 20, name: "Chicago", video: "terminator1", "from": [56.755542, -4.399045], "to": [41.858076, -87.678339],
    "labels": [null, "Chicago"], "color": "#2185ff", speedInMinutes: 15, missiles: 15, missileSpeed: 0.45, 
    status: CityStatus.AwaitingAttack
});


cities.set(21, {
    id: 21, name: "London", video: "london", "from": [62.518788, 59.855774], "to": [51.515489, -0.128748],
    "labels": [null, "London"], "color": "#fff646 ", speedInMinutes: 46, missiles: 13, missileSpeed: 0.75, 
    status: CityStatus.AwaitingAttack
});
cities.set(22, {
    id: 22, name: "Berlin", video: "terminator2", "from": [62.518788, 59.855774], "to": [52.517502, 13.409427],
    "labels": [null, "Berlin"], "color": "#b5ff46", speedInMinutes: 43, missiles: 14, missileSpeed: 0.65, 
    status: CityStatus.AwaitingAttack
});
cities.set(23, {
    id: 23, name: "Paris", video: "paris", "from": [62.518788, 59.855774], "to": [48.858636, 2.348823],
    "labels": [null, "Paris"], "color": "#ffc846", speedInMinutes: 45, missiles: 16, missileSpeed: 0.7, 
    status: CityStatus.AwaitingAttack
});
cities.set(24, {
    id: 24, name: "Rome", video: "rome", "from": [62.518788, 59.855774], "to": [41.898391, 12.491155],
    "labels": [null, "Rome"], "color": "#fff646", speedInMinutes: 44, missiles: 12, missileSpeed: 0.65, 
    status: CityStatus.AwaitingAttack
});
cities.set(25, {
    id: 25, name: "Ankara", video: "generic", "from": [62.518788, 59.855774], "to": [39.937529, 32.835492],
    "labels": [null, "Ankara"], "color": "#b5ff46", speedInMinutes: 42, missiles: 12, missileSpeed: 0.65, 
    status: CityStatus.AwaitingAttack
});
cities.set(26, {
    id: 26, name: "Astana", video: "generic", "from": [62.518788, 59.855774], "to": [51.146971, 71.421018],
    "labels": [null, "Astana"], "color": "#ffc846", speedInMinutes: 8, missiles: 11, missileSpeed: 0.35, 
    status: CityStatus.AwaitingAttack
});
cities.set(27, {
    id: 27, name: "Hong Kong", video: "generic", "from": [62.518788, 59.855774], "to": [22.348808, 114.193577],
    "labels": [null, "Hong Kong"], "color": "#fff646", speedInMinutes: 22, missiles: 13, missileSpeed: 0.65, 
    status: CityStatus.AwaitingAttack
});
cities.set(28, {
    id: 28, name: "Ulaanbaatar", video: "generic", "from": [62.518788, 59.855774], "to": [47.911407, 106.900868],
    "labels": [null, "Ulaanbaatar"], "color": "#b5ff46", speedInMinutes: 16, missiles: 14, missileSpeed: 0.45, 
    status: CityStatus.AwaitingAttack
});
cities.set(29, {
    id: 29, name: "Beijing", video: "generic", "from": [62.518788, 59.855774], "to": [39.893009, 116.360208],
    "labels": [null, "Beijing"], "color": "#fff646", speedInMinutes: 23, missiles: 13, missileSpeed: 0.45, 
    status: CityStatus.AwaitingAttack
});

cities.set(30, {
    id: 30, name: "Moscow", video: "generic", "from": [46.363607, 2.292419], "to": [55.702557, 37.596852],
    "labels": [null, "Moscow"], "color": "#0aff84", speedInMinutes: 7, missiles: 16, missileSpeed: 0.45, 
    status: CityStatus.AwaitingAttack
});
cities.set(31, {
    id: 31, name: "Teheran", video: "generic", "from": [46.363607, 2.292419], "to": [35.709987, 51.386935],
    "labels": [null, "Teheran"], "color": "#0a85ff", speedInMinutes: 6, missiles: 17, missileSpeed: 0.35, 
    status: CityStatus.AwaitingAttack
});
cities.set(32, {
    id: 32, name: "Riyadh", video: "generic", "from": [46.363607, 2.292419], "to": [24.722308, 46.737355],
    "labels": [null, "Riyadh"], "color": "#47ff0a", speedInMinutes: 5, missiles: 12, missileSpeed: 0.45, 
    status: CityStatus.AwaitingAttack
});
cities.set(33, {
    id: 33, name: "Kairo", video: "generic", "from": [46.363607, 2.292419], "to": [30.047745, 31.243184],
    "labels": [null, "Kairo"], "color": "#0aff84", speedInMinutes: 4, missiles: 11, missileSpeed: 0.35, 
    status: CityStatus.AwaitingAttack
});
cities.set(34, {
    id: 34, name: "Rabat", video: "generic", "from": [46.363607, 2.292419], "to": [33.974006, -6.836313],
    "labels": [null, "Rabat"], "color": "#0a85ff", speedInMinutes: 3, missiles: 9, missileSpeed: 0.35, 
    status: CityStatus.AwaitingAttack
});
