import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';


import { environment } from '../environments/environment';
import { RiddleState, RiddlePlayer, RiddleAtempt, RiddleResult, RecruitsStatus } from './shared/riddle-models';
import { FinalAtempt, FinalResult, FinalState} from './shared/final-models';
import { apiConf } from './shared/api-config';
import { GpsUserTaskResponse, GpsUserTaskRequest } from './shared/gps-user-task-models';

const BROWSER_ID = "browserIdKey";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const apis = {
  player: environment.api + apiConf.player,
  save: environment.api + apiConf.atempt,
  final: environment.api + apiConf.final,
  getFinalState: environment.api + apiConf.getFinalState,
  access: environment.api + apiConf.access,
  gpsTask: environment.api + apiConf.gpsTask,
  recruitStatus: environment.api + apiConf.recruitStatus,
  teamStatus: environment.api + apiConf.teamStatus,
};

class Guid {
  static newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
}

function setCookie(name: string, domain: string, val: string) {
  const date = new Date();
  const value = val;

  date.setTime(date.getTime() + (365 * 24 * 60 * 60 * 1000));

  document.cookie = name + "=" + value + "; expires=" + date.toUTCString() + "; path=/; Domain=" + domain;
}

function getCookie(name: string) {
  const value = "; " + document.cookie;
  const parts = value.split("; " + name + "=");
  
  if (parts.length === 2) {
      return parts.pop().split(";").shift();
  }
}

function deleteCookie(name: string, domain: string) {
  const date = new Date();

  // Set it expire in -1 days
  date.setTime(date.getTime() + (-1 * 24 * 60 * 60 * 1000));

  document.cookie = name + "=; expires=" + date.toUTCString() + "; path=/; Domain=" + domain;
}

function getDomainName() {
  const hostName = window.location.hostname;

  return hostName.substring(hostName.lastIndexOf(".", hostName.lastIndexOf(".") - 1) + 1);
}

@Injectable({
  providedIn: 'root'
})
export class RiddleApiService {

  private browserId: string;

  access(): Observable<RiddleResult> {
    
    const atempt = {
      state: RiddleState.Loading,
      subState: 0,
      browserId: this.browserId,
      href: location.href
    };

    return this.http.post<RiddleAtempt>(apis.access, atempt, httpOptions).pipe(
      tap((a: RiddleAtempt) => this.log(`Trying to save access ${a}`)),
      catchError(this.handleError<RiddleResult>('access', { state: atempt.state, subState: atempt.subState }))
    );
  }

  getPlayer(): Observable<RiddlePlayer> {
    console.log("Getting player from api=", apis.player);
    /* When using get we had problem with the request sometimes beeing cached, quick fix is to use post instead,
     * "better" would probably be to find and turn off caching.
     */
    return this.http.post<RiddlePlayer>(apis.player, {}, httpOptions ).pipe(
      catchError(this.handleError<RiddlePlayer>('getPlayer', this.getNullState()))
    );
  }

  getRecruitStatus(): Observable<RecruitsStatus> {
    console.log("Getting recruit status from api=", apis.recruitStatus);
    /* When using get we had problem with the request sometimes beeing cached, quick fix is to use post instead,
     * "better" would probably be to find and turn off caching.
     */
    return this.http.post<RecruitsStatus>(apis.recruitStatus, {}, httpOptions ).pipe(
      catchError(this.handleError<RecruitsStatus>('getRecruitsStatus', { players: [this.getNullState(), this.getNullState()] }))
    );
  }

  getTeamStatus(): Observable<RecruitsStatus> {
    console.log("Getting team status from api=", apis.teamStatus);
    /* When using get we had problem with the request sometimes beeing cached, quick fix is to use post instead,
     * "better" would probably be to find and turn off caching.
     */
    return this.http.post<RecruitsStatus>(apis.teamStatus, {}, httpOptions ).pipe(
      catchError(this.handleError<RecruitsStatus>('getTeamStatus', { players: [this.getNullState(), this.getNullState()] }))
    );
  }

  getGpsTask(gpsTaskRequest: GpsUserTaskRequest): Observable<GpsUserTaskResponse> {
    console.log("Getting gps task from api=", apis.gpsTask);
    return this.http.post<GpsUserTaskResponse>(apis.gpsTask, gpsTaskRequest, httpOptions).pipe(
      tap((a: GpsUserTaskResponse) => this.log(`Trying to get gps task ${a}`)),
      catchError(this.handleError<GpsUserTaskResponse>('atempt', { description: "" }))
    );
  }

  atempt(atempt: RiddleAtempt): Observable<RiddleResult> {
    atempt.browserId = this.browserId;
    return this.http.post<RiddleAtempt>(apis.save, atempt, httpOptions).pipe(
      tap((a: RiddleAtempt) => this.log(`Trying to save riddle ${a}`)),
      catchError(this.handleError<RiddleResult>('atempt', { state: atempt.state, subState: atempt.subState }))
    );
  }

  finalAtempt(atempt: FinalAtempt): Observable<FinalResult> {
    atempt.browserId = this.browserId;
    return this.http.post<FinalResult>(apis.final, atempt, httpOptions).pipe();
  }

  getFinalAtemptState(): Observable<FinalState> {
    console.log("Getting final state from api=", apis.getFinalState);
    return this.http.post<FinalState>(apis.getFinalState, httpOptions).pipe(
      tap((a: FinalState) => this.log(`Trying to get gps task ${a}`)),
      catchError(this.handleError<FinalState>('atempt', 0))
    );
  }

  getNullState(): RiddlePlayer {
    return { state: RiddleState.Error, subState: 0, domain: '', user: 'NA', nickname: 'NA', partner: 'NA' };
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }

  constructor(private http: HttpClient) {

    this.browserId = getCookie(BROWSER_ID);

    if (!this.browserId) {
      this.browserId = localStorage.getItem(BROWSER_ID);

      if (!this.browserId) {
        this.browserId = Guid.newGuid();
  
        // localStorage.setItem(BROWSER_ID, this.browserId);
      }

      setCookie(BROWSER_ID, getDomainName(), this.browserId);
    }
  }
}
