import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-xor',
  templateUrl: './xor.component.html',
  styleUrls: ['./xor.component.scss']
})
export class XorComponent implements OnInit {

  text = "";
  speed = 80;
  showDoc = false;

  textComplete(): void {
    this.showDoc = true;
    console.log("Text complete");
  }

  constructor() { }

  ngOnInit() {
    this.text = "Hacking into strategic.mil.ru. ........ hack complete, downloading document......";
  }
}
