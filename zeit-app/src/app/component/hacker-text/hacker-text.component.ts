import { AfterViewInit, Component, OnInit, Input, Output, OnChanges, EventEmitter, ElementRef } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { Typed } from './typed';
import { MatOptionSelectionChange } from '@angular/material';

@Component({
  selector: 'app-hacker-text',
  templateUrl: './hacker-text.component.html',
  styleUrls: ['./hacker-text.component.scss']
})
export class HackerTextComponent implements OnChanges, OnInit, AfterViewInit {

  typed: Typed;
  @Input() text: string;
  @Input() speed = 40;
  @Output() textComplete: EventEmitter<any> = new EventEmitter();

  contentObservable: Observable<string>;
  contentSubscription: Subscription;

  private onTextComplete = () => {
    console.log("Done typing text");
    this.textComplete.emit(null);
  }

  private options = {
    speed: this.speed,
    backSpeed: 0,
    showCursor: true,
    cursorChar: '█',
    timeout: 0,
    hideCursorOnComplete: false,
    onComplete: this.onTextComplete
  };

  constructor(private elRef: ElementRef) { }

  private checkContent() {
    return this.text;
  }

  ngAfterViewInit() {
    if (this.typed) {
      return;
    }

    if (!this.checkContent()) {
      this.contentObservable = new Observable((ob) => {
        if (this.checkContent()) {
          ob.next(this.text);
          ob.complete();
        }
      });

      this.contentSubscription = this.contentObservable.subscribe((content) => {
        this.createTyped();
        this.contentSubscription.unsubscribe();
      });

      return;
    }

    this.createTyped();
  }

  ngOnInit() {
    this.options.speed = this.speed;

    if (!this.text || this.text === '') {
      this.text = this.elRef.nativeElement.innerHTML;
    }
    if (!this.checkContent()) {
      return;
    }
    this.createTyped();
  }

  ngOnChanges() {
    if (this.typed) {
      this.typed.textContent = this.text;
      this.typed.begin();
    }
  }

  createTyped() {
    this.typed = new Typed(this.elRef.nativeElement, this.options, this.text);

    this.typed.begin();
  }
}
