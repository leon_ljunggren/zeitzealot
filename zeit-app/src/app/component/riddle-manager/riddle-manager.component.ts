import { Component, OnInit, AfterViewChecked, AfterViewInit, OnChanges } from '@angular/core';

import { RiddleState, RiddlePlayer } from '../../shared/riddle-models';
import { RiddleApiService } from '../../riddle-api.service';


@Component({
  selector: 'app-riddle-manager',
  styleUrls: ['./riddle-manager.component.scss'],
  template: `
  <div class="main" [ngSwitch]="player.state">
    <div *ngSwitchCase="'${RiddleState.Intro}'">
      <app-riddle-intro (solved)="setState()"></app-riddle-intro>
    </div>
    <div *ngSwitchCase="'${RiddleState.Nickname}'">
      <app-riddle-nickname (solved)="setState()"></app-riddle-nickname>
    </div>
    <div *ngSwitchCase="'${RiddleState.NickAccept}'">
        <app-riddle-nick-accept (solved)="setState()" [player]="player"></app-riddle-nick-accept>
    </div>
    <div *ngSwitchCase="'${RiddleState.BeInTouch}'">
      <app-riddle-be-in-touch (solved)="setState()"></app-riddle-be-in-touch>
    </div>
    <div *ngSwitchCase="'${RiddleState.BeInTouch2}'">
      <app-riddle-be-in-touch (solved)="setState()"></app-riddle-be-in-touch>
    </div>
    <div *ngSwitchCase="'${RiddleState.BeInTouch3}'">
      <app-riddle-be-in-touch (solved)="setState()"></app-riddle-be-in-touch>
    </div>
    <div *ngSwitchCase="'${RiddleState.PersonalityTest}'">
      <app-riddle-personality-test (solved)="setState()" [player]="player"></app-riddle-personality-test>
    </div>
    <div *ngSwitchCase="'${RiddleState.PreTools}'">
      <app-riddle-pre-tools (solved)="setState()" [player]="player"></app-riddle-pre-tools>
    </div>
    <div *ngSwitchCase="'${RiddleState.Tools}'">
      <app-riddle-tools (solved)="setState()" [player]="player"></app-riddle-tools>
    </div>
    <div *ngSwitchCase="'${RiddleState.GpsIntro}'">
      <app-riddle-gps-intro (solved)="setState()" [player]="player"></app-riddle-gps-intro>
    </div>
    <div *ngSwitchCase="'${RiddleState.HighScore}'">
      <app-riddle-high-score (solved)="setState()" [player]="player"></app-riddle-high-score>
    </div>
    <div *ngSwitchCase="'${RiddleState.TeamSetup}'">
      <app-riddle-team-setup (solved)="setState()" [player]="player"></app-riddle-team-setup>
    </div>
    <div *ngSwitchCase="'${RiddleState.BranchInfo}'">
      <app-riddle-branch-info (solved)="setState()" [player]="player"></app-riddle-branch-info>
    </div>
    <div *ngSwitchCase="'${RiddleState.SatellitePre}'">
      <app-riddle-satellite-pre (solved)="setState()" [player]="player"></app-riddle-satellite-pre>
    </div>
    <div *ngSwitchCase="'${RiddleState.Satellite}'">
      <app-riddle-satellite (solved)="setState()" [player]="player"></app-riddle-satellite>
    </div>
    <div *ngSwitchCase="'${RiddleState.EncryptionCode}'">
      <app-riddle-encryption-code (solved)="setState()" [player]="player"></app-riddle-encryption-code>
    </div>
    <div *ngSwitchCase="'${RiddleState.Explanations}'">
      <app-riddle-explanations (solved)="setState()" [player]="player"></app-riddle-explanations>
    </div>
    <div *ngSwitchCase="'${RiddleState.PreStations}'">
      <app-riddle-pre-stations (solved)="setState()" [player]="player"></app-riddle-pre-stations>
    </div>
    <div *ngSwitchCase="'${RiddleState.Stations}'">
      <app-riddle-stations (solved)="setState()" [player]="player"></app-riddle-stations>
    </div>
    <div *ngSwitchCase="'${RiddleState.PreFinalStation}'">
      <app-riddle-pre-final-station (solved)="setState()" [player]="player"></app-riddle-pre-final-station>
    </div>
    <div *ngSwitchCase="'${RiddleState.FinalStation}'">
      <app-riddle-final-station (solved)="setState()" [player]="player"></app-riddle-final-station>
    </div>
    <div *ngSwitchCase="'${RiddleState.Loading}'">
        <app-riddle-loading></app-riddle-loading>
    </div>
    <div *ngSwitchDefault>
      <app-riddle-error [player]="player"></app-riddle-error>
    </div>
  </div>`
})
export class RiddleManagerComponent implements OnInit, AfterViewInit {
  
  player: RiddlePlayer = this.api.getNullState();

  setState(): void {
    console.log("Setting state", this.player);

    this.api.getPlayer().subscribe((player: RiddlePlayer) => this.player = player);

    console.log("Setting state", this.player);
  }

  constructor(private api: RiddleApiService) {
    console.log("In constructor " + Math.random());
  }

  ngOnInit() {

    this.player = {
      domain: null,
      user: null,
      nickname: null,
      state: RiddleState.Loading,
      subState: 0,
      partner: null
    };

    this.api.getPlayer().subscribe((player: RiddlePlayer) => { console.log("Got player=", player); this.player = player; });

    console.log("On Init " + Math.random());
  }

  ngAfterViewInit() {
    console.log("After view init " + Math.random());
  }

}
