import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-xor-pad',
  templateUrl: './xor-pad.component.html',
  styleUrls: ['./xor-pad.component.scss']
})
export class XorPadComponent implements OnInit {

  text = "";
  speed = 80;
  showDoc = false;

  textComplete(): void {
    this.showDoc = true;
    console.log("Text complete");
  }

  constructor() { }

  ngOnInit() {
    this.text = "Hacking into strategic.mil.ru. ........ hack complete, downloading document......";
  }
}
