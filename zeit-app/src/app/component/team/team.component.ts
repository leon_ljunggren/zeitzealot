import { Component, OnInit } from '@angular/core';
import { RiddleApiService } from '../../riddle-api.service';
import { RiddlePlayer, RiddleState, RecruitsStatus } from '../../shared/riddle-models';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  text = "";
  players: RiddlePlayer[];
 
  setRecruits(recruits: RecruitsStatus): void {
    this.players = recruits.players;
  }

  constructor(private api: RiddleApiService) { }

  ngOnInit() {
    this.text = "No one has all the pieces, you will have to work together to proceed.";
    this.api.getTeamStatus().subscribe(
      x => this.setRecruits(x),
      err => console.log('getTeamStatus got error: ', err),
      () => console.log('getTeamStatus got a complete notification')
    );
  }
}
