import { Component, OnInit, Input } from '@angular/core';

import { RiddleComponent } from '../riddle.component';
import { RiddlePlayer, RiddleState, RiddleAtempt } from '../../../shared/riddle-models';
import { RiddleApiService } from '../../../riddle-api.service';

const WAIT_TO_PROCEED = 10000;

@Component({
  selector: 'app-riddle-explanations',
  templateUrl: './explanations.component.html',
  styleUrls: ['./explanations.component.scss']
})
export class ExplanationsComponent extends RiddleComponent implements OnInit {

  @Input() player: RiddlePlayer;
  text = "";
  text2 = " ";
  text3 = " ";
  showJoin = false;
  showAnon = false;

  goNext() {
    this.sleep(WAIT_TO_PROCEED).then(() => {
      this.signalSolved();
    });
  }

  textComplete(): void {
    this.sleep(WAIT_TO_PROCEED).then(() => {
      this.showAnon = true;
      this.text2 = "We are legion, we are Anonymous.";
    });
  }

  textComplete2(): void {
    this.sleep(WAIT_TO_PROCEED).then(() => {
      this.showJoin = true;
      this.text3 = "You have done well, from a certain point of view at least. But you still have a lot to prove if you \
                   expect to join us.";
    });
  }

  textComplete3(): void {

    const atempt = new RiddleAtempt(RiddleState.Explanations);

    this.api.atempt(atempt).subscribe(
      x => this.goNext(),
      err => console.log('Explanations got error: ', err),
      () => console.log('Explanations got a complete notification')
    );
  }

  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
    if (this.player) {
      this.text =
        "Capitalists, communists, democrats, libertarians, oligarchs, plutocrats, socialist, they are all the same. \
      Squabbling over petty differences while the world burns around them. Our so called leaders have shown time and time \
      again to care only for personal power and wealth, being prepared to do anything to stay in power just a little \
      while longer. Anything but fixing any kind of real issue that is. But, in their bumbling idiocracy, these leaders have \
      sown the seeds of their destruction. We will take control of the very thing that for decades has had this world on the edge of \
      destruction and force the change that is needed. We will show the way forward, to a future where no one will have to suffer \
      under the yoke of oppression. It won't be easy, but if one bites the dust another one will take their place, and \
      another one, and another one...";
    }
  }
}
