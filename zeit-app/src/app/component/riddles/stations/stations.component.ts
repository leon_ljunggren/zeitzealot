import { Component, OnInit, Input } from '@angular/core';

import { RiddleComponent } from '../riddle.component';
import { RiddlePlayer, RiddleState, RiddleAtempt } from '../../../shared/riddle-models';
import { StationsResult, StationsState } from '../../../shared/stations-models';
import { RiddleApiService } from '../../../riddle-api.service';

const WAIT_TO_PROCEED = 10000;



@Component({
  selector: 'app-riddle-stations',
  templateUrl: './stations.component.html',
  styleUrls: ['./stations.component.scss']
})
export class StationsComponent extends RiddleComponent implements OnInit {

  @Input() player: RiddlePlayer;
  stations = [];
  text: string;

  goNext() {
    this.sleep(WAIT_TO_PROCEED).then(() => {
      this.signalSolved();
    });
  }

  textComplete(): void {}

  handleResponse(res: StationsResult) {
    this.stations = res.stations;

    if (res.subState === StationsState.Done) {
      this.text = "That was our last hope... No...there is another...";
      this.goNext();
    }
  }

  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
    if (this.player) {
      this.text = " ";
      const atempt = new RiddleAtempt(RiddleState.Stations);

      this.api.atempt(atempt).subscribe(
        x => this.handleResponse(<StationsResult>x),
        err => console.log('Stations got error: ', err),
        () => console.log('Stations got a complete notification')
      );
    }
  }
}
