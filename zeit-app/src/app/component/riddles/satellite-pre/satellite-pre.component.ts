import { Component, OnInit, Input } from '@angular/core';

import { RiddleComponent } from '../riddle.component';
import { RiddlePlayer, RiddleState, RiddleAtempt } from '../../../shared/riddle-models';
import { RiddleApiService } from '../../../riddle-api.service';

const WAIT_TO_PROCEED = 10000;

@Component({
  selector: 'app-riddle-satellite-pre',
  templateUrl: './satellite-pre.component.html',
  styleUrls: ['./satellite-pre.component.scss']
})
export class SatellitePreComponent extends RiddleComponent implements OnInit {

  @Input() player: RiddlePlayer;
  text = "";

  goNext() {
    this.sleep(WAIT_TO_PROCEED).then(() => {
      this.signalSolved();
    });
  }

  textComplete(): void {

    const atempt = new RiddleAtempt(RiddleState.SatellitePre);

    this.api.atempt(atempt).subscribe(
      x => this.goNext(),
      err => console.log('satellite-pre got error: ', err),
      () => console.log('satellite-pre got a complete notification')
    );
  }
  
  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
    if (this.player) {
      this.text = "There's a satellite in medium earth orbit that we need data from. It passes overhead once \
      every 24 hours or so and uses strong encryption that changes on each pass. That means it has to be cracked \
      in one pass or you will have to start over. However, the encryption is too strong for one agent alone, \
      you will need at least four working together to have a chance of breaching it. \
      The hack requires quite a lot of power, and can only be attempted for a limited amount of time.";
    }
  }
}
