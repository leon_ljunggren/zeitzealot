import { Component, OnInit, Input } from '@angular/core';

import { RiddleComponent } from '../riddle.component';
import { RiddlePlayer, RiddleState, RiddleAtempt } from '../../../shared/riddle-models';
import { RiddleApiService } from '../../../riddle-api.service';

const WAIT_TO_PROCEED = 10000;


@Component({
  selector: 'app-riddle-pre-stations',
  templateUrl: './pre-stations.component.html',
  styleUrls: ['./pre-stations.component.scss']
})
export class PreStationsComponent extends RiddleComponent implements OnInit {

  @Input() player: RiddlePlayer;
  text = "";
  text2 = " ";
  showJoin = false;

  goNext() {
    this.sleep(WAIT_TO_PROCEED).then(() => {
      this.signalSolved();
    });
  }

  textComplete(): void {
    this.sleep(WAIT_TO_PROCEED).then(() => {
      this.showJoin = true;
      this.text2 = "But first you will have to find them. The plans did not disclose their location, we have \
      only a weak signal to go after. Be sure to not walk in circles and remember that 1 = ∞, 2 = 2 and 3 = 1.";
    });
  }

  textComplete2(): void {

    const atempt = new RiddleAtempt(RiddleState.PreStations);

    this.api.atempt(atempt).subscribe(
      x => this.goNext(),
      err => console.log('PreStations got error: ', err),
      () => console.log('PreStations got a complete notification')
    );
  }

  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
    if (this.player) {
      this.text = "The data from the satellite has alerted us to the existence of four ground stations \
      that contains the final pieces of information that we need. They are protected by a jamming field and \
      are heavily encrypted, so heavily encrypted that they are thought to be impregnable to a large scale \
      attack. An analysis of \
      the plans from the satellite has demonstrated a weakness in the stations. But the approach will not \
      be easy. You are required to maneuver at least one agent to a close proximity, that agent can then \
      relay to the others that can work from a distance. The weakness is due to a small termix output \
      port that lags slightly behind the others when rotating the encryption keys. This will leave us with \
      a small window of opportunity each day to perform the hack. A precise strike in this window will \
      start a chain hack which will give us complete access. Only a precise strike will enable the chain \
      reaction. The port is jamming-shielded, so you will have to use proto skills.";
    }
  }
}
