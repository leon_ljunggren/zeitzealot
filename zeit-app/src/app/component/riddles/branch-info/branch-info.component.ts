import { Component, OnInit, Input } from '@angular/core';

import { RiddleComponent } from '../riddle.component';
import { RiddlePlayer, RiddleState, RiddleAtempt } from '../../../shared/riddle-models';
import { RiddleApiService } from '../../../riddle-api.service';

const WAIT_TO_PROCEED = 10000;

@Component({
  selector: 'app-riddle-branch-info',
  templateUrl: './branch-info.component.html',
  styleUrls: ['./branch-info.component.scss']
})
export class BranchInfoComponent extends RiddleComponent implements OnInit {

  @Input() player: RiddlePlayer;
  text = "";

  goNext() {
    this.sleep(WAIT_TO_PROCEED).then(() => {
      this.signalSolved();
    });
  }

  textComplete(): void {

    const atempt = new RiddleAtempt(RiddleState.BranchInfo);

    this.api.atempt(atempt).subscribe(
      x => this.goNext(),
      err => console.log('branch-info got error: ', err),
      () => console.log('branch-info got a complete notification')
    );
  }
  
  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
    if (this.player) {
      this.text = "Up until now the path has been linear and safe, one thing leading to the next with little consequences for failure. \
      That ends today, from now on we branch out and the path forward becomes murky and unsure. Information gained might not lead directly \
      to the next challenge, it might be needed later, or perhaps not relevant at all. \
      Some of you might have received such information already, or maybe not, nothing is clear. Except that the danger is real, \
      step lightly or you might not make it to the end.";
    }
  }
}
