import { Component, OnInit, Input } from '@angular/core';
import browser from 'browser-detect';

import { RiddleComponent } from '../riddle.component';
import { RiddleState, RiddlePlayer } from '../../../shared/riddle-models';
import { RiddleApiService } from '../../../riddle-api.service';
import { ToolsResult, ToolsAtempt, ToolsState } from '../../../shared/tools-models';

const RIDDLE_NAME = RiddleState.Tools;
const WAIT_AFTER_DONE = 10000;
const WAIT_TO_PROCEED = 3000;
const WAIT_TO_SHOW_TOOL_LIST = 1000;

class Tool {
  name: string;
  description: string;
  todo(toolState: ToolsState): boolean {
    return (toolState & ToolsState.getStateFromBrowser(this.name)) !== ToolsState.getStateFromBrowser(this.name);
  }

  constructor(name: string, description: string) {
    this.name = name;
    this.description = description;
  }
}

@Component({
  selector: 'app-riddle-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss']
})
export class ToolsComponent extends RiddleComponent implements OnInit {

  @Input() player: RiddlePlayer;
  text = "";

  showToolList = false;
  currentState = 0;

  tools = [ new Tool("chrome", "Element number 24, or it's plated variant"),
  new Tool("firefox", "A fox made of fire"),
  new Tool("safari", "A trip to the jungle")];

  textComplete(): void {

    if (ToolsState.isSet(this.currentState, ToolsState.Done)) {
      this.sleep(WAIT_AFTER_DONE).then(() => {
        this.signalSolved();
      });
    }
  }

  saveTool(tool: string): void {
    console.log("Saving tool");

    const atempt: ToolsAtempt = new ToolsAtempt(RIDDLE_NAME, this.currentState, tool);

    this.api.atempt(atempt).subscribe(
      x => { console.log('saveTool got value: ', x); this.textSelector((<ToolsResult>x).subState); },
      err => console.log('saveTool got error: ', err),
      () => console.log('saveTool got a complete notification')
    );
  }

  textSelector(toolsState: ToolsState): void {

    this.currentState = toolsState;

    if (toolsState === ToolsState.Unknown) {
      this.text = "The world is ending due to errors in the matrix, sit back and enjoy the ride.";
    }
    else {
      if (ToolsState.isSet(this.currentState, ToolsState.Done)) {
        this.text = "Well done";
      }
      else {
        this.text = "Come back using:";
      }
      
      this.showToolList = true;
    }
  }

  getBrowser(): string {
    const result = browser();

    console.log("Browser: ", result);

    return result.name;
  }

  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
    if (this.player) {
      this.currentState = this.player.subState;
      this.saveTool(this.getBrowser());
    }
  }
}
