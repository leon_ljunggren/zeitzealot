import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-riddle',
  template: ''
})
export class RiddleComponent implements OnInit {

  @Output() solved: EventEmitter<any> = new EventEmitter();

  signalSolved(): void {
    console.log("Signal riddle solved");
    this.solved.emit(null);
  }

  private _sleep(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  async sleep(ms: number) {
    await this._sleep(ms);
  }

  constructor() { }

  ngOnInit() {
  }
}
