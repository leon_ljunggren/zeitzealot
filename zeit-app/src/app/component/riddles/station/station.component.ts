import { Component, OnInit } from '@angular/core';
import { Subscription, timer, interval, of } from 'rxjs';
import { takeWhile, takeUntil } from 'rxjs/operators';
import { Router } from "@angular/router";

import { RiddleComponent } from '../riddle.component';
import { RiddlePlayer, RiddleState, RiddleResult } from '../../../shared/riddle-models';
import { AgentsOnline, ALLOWED_HACK_INTERVAL, formateDuration } from '../../../shared/satellite-models';
import { RiddleApiService } from '../../../riddle-api.service';
import { StationAtempt, StationResult, Station, StationStatus } from '../../../shared/stations-models';

const WAIT_TO_PROCEED = 6000;
const HACK_POLL_INTERVAL = 1000;
const HACK_TIME_ALLOVANCE = 40;
const MAX_HACKING_TIME = 5 * 60 * 1000 + HACK_TIME_ALLOVANCE * 1000;

interface SuccessCallback {
  (success: boolean): void;
}

@Component({
  selector: 'app-station',
  templateUrl: './station.component.html',
  styleUrls: ['./station.component.scss']
})
export class StationComponent extends RiddleComponent implements OnInit {

  /* This component isn't part of the riddle manager, so we need to get the player ourself,
   * maybe this should have been a part of the manager.
   */
  player: RiddlePlayer = this.api.getNullState();

  text = "";
  satelliteTime: string;
  hackingTimeRemaing: string;
  timeRemaining = Math.round(MAX_HACKING_TIME / 1000);
  agentsNeeded: number;
  agents: AgentsOnline[];
  stationName: string;
  station: Station;

  retrievedData: string;
  downloadComplete = false;

  currentDistance: number;
  lastPosition: Position = null;
  watchId: number;

  inputLat: number;
  inputLong: number;
  locationComplete = false;

  realLat: number;
  realLong: number;

  subscribe: Subscription;


  textComplete(): void {
    if (this.isDone()) {
      this.sleep(WAIT_TO_PROCEED).then(() => {
        this.downloadComplete = true;
      });
    }
  }

  saveCoords(): void {
    const self = this;
    this.getStation(function(result: StationResult) {
      if (result.station.status === StationStatus.LOCATED) {
        self.text = "Approximate location confirmed, calculating exact location...";
        self.sleep(WAIT_TO_PROCEED).then(() => {
          console.log("Showing hacking", self.station);
          self.text = " ";
          self.locationComplete = true;
          self.startHacking();
        });
      }
    });
  }

  isDone(): boolean {
    return this.station && this.station.status === StationStatus.DONE;
  }

  showData(): boolean {
    return this.isDone() && this.downloadComplete;
  }

  showGpsError(): boolean {
    return this.station && this.station.status !== StationStatus.DONE && !this.lastPosition;
  }

  showLocate(): boolean {
    return this.station && this.station.status === StationStatus.UNKNOWN;
  }

  showHacking(): boolean {
    return this.station && this.station.status === StationStatus.LOCATED && this.locationComplete;
  }

  isOvertime(): boolean {
    return this.timeRemaining < HACK_TIME_ALLOVANCE;
  }

  stopHacking(): void {
    this.timeRemaining = 0;
    this.hackingTimeRemaing = formateDuration(this.timeRemaining - HACK_TIME_ALLOVANCE);
    if (this.showHacking()) {
      this.text = "Shutting down hack to avoid meltdown.";
    }
  }

  terminateInterval(): void {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }

  isHacking(): boolean {
    return this.showHacking() && this.timeRemaining > 0;
  }

  unsubscribeGeoLocation(watchId: number) {
    if (navigator.geolocation) {
      navigator.geolocation.clearWatch(watchId);
    }
  }

  subscribeGeoLocation(callback: SuccessCallback): void {

    const self = this;

    const options = {
      enableHighAccuracy: true,
      maximumAge: 0
    };

    function success(pos: Position) {
      self.lastPosition = pos;
      callback(true);
    }

    function error(err: PositionError) {
      console.warn(`ERROR(${err.code}): ${err.message}`);
      self.lastPosition = null;

      callback(false);
    }

    if (navigator.geolocation) {
      self.watchId = navigator.geolocation.watchPosition(success, error, options);
    }
    else {
      callback(false);
    }
  }

  getStation(callback?: (station: RiddleResult) => void): void {

    let guessLat = -1;
    let guessLong = -1;

    if (this.showLocate()) {
      guessLat = this.inputLat;
      guessLong = this.inputLong;
    }

    let gpsLat = 0;
    let gpsLong = 0;
    if (this.lastPosition) {
      gpsLat = this.lastPosition.coords.latitude;
      gpsLong = this.lastPosition.coords.longitude;
    }

    const atempt = new StationAtempt(RiddleState.Station, this.stationName, gpsLat, gpsLong, guessLat, guessLong);

    this.api.atempt(atempt).subscribe(
      x => {
        const result = <StationResult>x;

        console.log("Got station result: ", result);

        this.station = result.station;
        this.currentDistance = result.distance;
        this.agents = result.agents;
        this.agentsNeeded = result.agentsNeeded;
        this.satelliteTime = formateDuration(result.timeToOverhead);
        this.retrievedData = result.data;
        this.realLat = result.latitude;
        this.realLong = result.longitude;

        if (this.station.status === StationStatus.DONE) {
          this.unsubscribeGeoLocation(this.watchId);
          this.text = "Downloading data...";
          this.terminateInterval();
        }
        else if (result.timeToOverhead < -(ALLOWED_HACK_INTERVAL / 1000)) {
          this.text = "Hacking failed due to too few agents in the right places.";
          this.terminateInterval();
        }

        if (callback) {
          callback(result);
        }
      },
      err => console.log('get station error: ', err)
    );
  }

  performHack(): void {
    if (this.isHacking()) {
      this.timeRemaining--;
      this.hackingTimeRemaing = formateDuration(this.timeRemaining - HACK_TIME_ALLOVANCE);

      this.getStation();
    }
  }

  constructor(private api: RiddleApiService, private router: Router) {
    super();
    this.stationName = this.router.url.replace("/", "");
    console.log(this.stationName);
  }

  startHacking(): void {
    if (!this.subscribe) {
      /* Start hack polling */

      /* Only poll for a maximum time, so they can't just leave the window open and so we don't fill the database */
      const timer$ = timer(MAX_HACKING_TIME);
      timer$.subscribe(() => this.stopHacking());

      this.subscribe = interval(HACK_POLL_INTERVAL).pipe(takeWhile(() => this.isHacking()))
        .pipe(takeUntil(timer$))
        .subscribe(index => this.performHack());
    }
  }

  ngOnInit() {
    this.text = " ";

    this.player = {
      domain: null,
      user: null,
      nickname: null,
      state: RiddleState.Loading,
      subState: 0,
      partner: null
    };

    const self = this;

    this.api.getPlayer().subscribe((player: RiddlePlayer) => {
      console.log("Got player=", player); 
      this.player = player;

      self.getStation(function (result: StationResult) {
        if (result.station.status !== StationStatus.DONE) {
          self.subscribeGeoLocation(succes => {
            /* Only let the gps determin when to submit data in the locate phase, after that the gps simply saves the last
             * location and the hacking system is responsible for submitting it.
             */
            if (result.station.status === StationStatus.UNKNOWN) {
              self.getStation();
            }
            
          });

          if (result.station.status === StationStatus.LOCATED) {
            self.locationComplete = true;
            self.startHacking();
          }
        }
        else {
          /* Show the stations data */
        }
      });
    });
  }
}
