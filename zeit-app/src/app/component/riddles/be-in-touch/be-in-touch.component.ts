import { Component, OnInit } from '@angular/core';

import { RiddleComponent } from '../riddle.component';

@Component({
  selector: 'app-riddle-be-in-touch',
  templateUrl: './be-in-touch.component.html',
  styleUrls: ['./be-in-touch.component.scss']
})
export class BeInTouchComponent extends RiddleComponent implements OnInit {

  gfVisible = false;
  text = "";

  constructor() { 
    super();
  }

  ngOnInit() {
    this.text = "We'll be in touch.";
      this.sleep(3000).then(() => {
        this.gfVisible = true;
      });
  }
}
