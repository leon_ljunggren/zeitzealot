import { Component, OnInit, Input } from '@angular/core';
import { Subscription, timer, interval, of } from 'rxjs';
import { takeWhile, takeUntil } from 'rxjs/operators';

import { RiddleComponent } from '../riddle.component';
import { RiddlePlayer, RiddleState, RiddleAtempt } from '../../../shared/riddle-models';
import { SatelliteResult, SatelliteState, AgentsOnline, ALLOWED_HACK_INTERVAL, formateDuration } from '../../../shared/satellite-models';
import { RiddleApiService } from '../../../riddle-api.service';


const WAIT_TO_PROCEED = 10000;
const HACK_POLL_INTERVAL = 1000;
const HACK_TIME_ALLOVANCE = 40;
const MAX_HACKING_TIME = 5 * 60 * 1000 + HACK_TIME_ALLOVANCE * 1000;

@Component({
  selector: 'app-riddle-satellite',
  templateUrl: './satellite.component.html',
  styleUrls: ['./satellite.component.scss']
})
export class SatelliteComponent extends RiddleComponent implements OnInit {

  @Input() player: RiddlePlayer;
  text = "";
  showHacking = true;
  satelliteTime: string;
  hackingTimeRemaing: string;
  timeRemaining = Math.round(MAX_HACKING_TIME / 1000);
  agents: AgentsOnline[];

  subscribe: Subscription;

  goNext() {
    this.sleep(WAIT_TO_PROCEED).then(() => {
      this.signalSolved();
    });
  }

  textComplete(): void {

    if (this.player.subState === SatelliteState.Done) {
      const atempt = new RiddleAtempt(RiddleState.Satellite);

      this.api.atempt(atempt).subscribe(
        x => this.goNext(),
        err => console.log('satellite got error: ', err),
        () => console.log('satellite got a complete notification')
      );
    }
    else if (this.player.subState === SatelliteState.PreFinished) {
      this.goNext();
    }
  }

  isOvertime(): boolean {
    return this.timeRemaining < HACK_TIME_ALLOVANCE;
  }

  stopHacking(): void {
    this.timeRemaining = 0;
    this.hackingTimeRemaing = formateDuration(this.timeRemaining - HACK_TIME_ALLOVANCE);
    this.text = "Shutting down hack to avoid meltdown.";
  }

  terminateInterval(): void {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }

  isHacking(): boolean {
    return this.player.subState === SatelliteState.Hacking && this.timeRemaining > 0;
  }

  performAtempt(firstPass: boolean, callback?): void {
    if (this.isHacking()) {
      this.timeRemaining--;
      this.hackingTimeRemaing = formateDuration(this.timeRemaining - HACK_TIME_ALLOVANCE);

      console.log("Performing satellite atempt");

      const atempt = new RiddleAtempt(RiddleState.Satellite);

      this.api.atempt(atempt).subscribe(
        x => {
          console.log('atempt got value: ', x);

          const satelliteResult = <SatelliteResult>x;

          this.player.state = satelliteResult.state;
          this.player.subState = satelliteResult.subState;
          this.agents = satelliteResult.agents;
          this.satelliteTime = formateDuration(satelliteResult.timeToOverhead);

          if (satelliteResult.subState === SatelliteState.Done || satelliteResult.subState === SatelliteState.PreFinished) {
            if (!firstPass) {
              this.text = "Hacking complete, data retrieved successfully.";
              this.terminateInterval();
            }
          }
          else if (satelliteResult.subState === SatelliteState.Hacking && 
            satelliteResult.timeToOverhead < -(ALLOWED_HACK_INTERVAL / 1000)) {
            this.text = "Hacking failed due to too few agents.";
            this.terminateInterval();
          }

          if (callback) {
            callback();
          }
        },
        err => console.log('satellite atempt got error: ', err)
      );
    }
  }

  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
    this.text = " ";
    if (this.player) {

      const self = this;
      this.performAtempt(true, function() {
        
        if (self.player.subState === SatelliteState.PreFinished) {
          self.showHacking = false;
          self.text = "Hacking completed already, data was retrieved successfully.";
        }
        else {
          if (!self.subscribe) {
            /* Start hack polling */
    
            /* Only poll for a maximum time, so they can't just leave the window open and so we don't fill the database */
            const timer$ = timer(MAX_HACKING_TIME);
            timer$.subscribe(() => self.stopHacking());
    
            self.subscribe = interval(HACK_POLL_INTERVAL).pipe(takeWhile(() => self.isHacking()))
              .pipe(takeUntil(timer$))
              .subscribe(index => self.performAtempt(false));
          }
        }
      });
    }
  }
}
