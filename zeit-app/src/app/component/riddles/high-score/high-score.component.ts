import { Component, OnInit, Input } from '@angular/core';

import { RiddleComponent } from '../riddle.component';
import { RiddlePlayer, RiddleState, RiddleAtempt } from '../../../shared/riddle-models';
import { RiddleApiService } from '../../../riddle-api.service';

const WAIT_FOR_PROCEED = 3000;

@Component({
  selector: 'app-riddle-high-score',
  templateUrl: './high-score.component.html',
  styleUrls: ['./high-score.component.scss']
})
export class HighScoreComponent extends RiddleComponent implements OnInit {

  @Input() player: RiddlePlayer;
  highScoreVisible = false;
  proceedVisible = false;
  textIntro = "";
  textLine = "";
  malStatus = "PENDING";

  textCompleteIntro(): void {
    this.sleep(WAIT_FOR_PROCEED).then(() => {
      this.highScoreVisible = true;
      this.sleep(WAIT_FOR_PROCEED).then(() => {
        this.textLine = "See that line? That's the cutoff, you have to do better than that in order to be selected to join us. \
                        The next phase will be critical.";
      });
    });
  }

  textCompleteLine(): void {
    this.sleep(WAIT_FOR_PROCEED).then(() => {
      this.proceedVisible = true;
    });
  }

  proceed(): void {
    const atempt = new RiddleAtempt(RiddleState.HighScore);

    this.api.atempt(atempt).subscribe(
      x => this.signalSolved(),
      err => console.log('high-score got error: ', err),
      () => console.log('high-score got a complete notification')
    );
  }

  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
    if (this.player && (this.player.domain === "littlemy.zeitzealot.org" || this.player.domain === "192.168.1.10")) {
      this.malStatus = "COMPLETE";
    }

    this.textIntro = "You've done well to reach this point, but this is only the beginning. \
    We want only the best, of the best, of the best, of the best, of the best... \
    lets see how you compare to the others.";
  }
}
