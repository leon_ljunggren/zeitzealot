import { Component, OnInit, Input } from '@angular/core';
import { interval, Subscription, timer } from 'rxjs';
import { takeWhile, takeUntil } from 'rxjs/operators';

import { RiddleComponent } from '../riddle.component';
import { RiddleState, RiddlePlayer } from '../../../shared/riddle-models';
import { RiddleApiService } from '../../../riddle-api.service';
import { GpsIntroResult, GpsIntroAtempt, GpsIntroState } from '../../../shared/gps-intro-models';
import { GpsUserTaskResponse } from '../../../shared/gps-user-task-models';

const RIDDLE_NAME = RiddleState.GpsIntro;
const WAIT_AFTER_DONE = 10000;
const WAIT_FOR_PROCEED = 3000;
const GPS_POLL_INTERVAL = 1000;
const GPS_MAX_POLL_TIME = 60 * 60 * 1000;

interface SuccessCallback {
  (success: boolean): void;
}

@Component({
  selector: 'app-riddle-gps-intro',
  templateUrl: './gps-intro.component.html',
  styleUrls: ['./gps-intro.component.scss']
})
export class GpsIntroComponent extends RiddleComponent implements OnInit {

  @Input() player: RiddlePlayer;
  text = "";
  currentDistance = -1;
  lastPosition: Position = null;
  currentState: GpsIntroState = GpsIntroState.Unknown;
  gpsTask: GpsUserTaskResponse;

  subscribe: Subscription;
  watchId: number;
  
  showJoke = false;
  textJoke = " ";

  pastIntro = false;

  getGeoLocation(callback: SuccessCallback): void {

    const self = this;

    const options = {
      enableHighAccuracy: true,
      timeout: 60000,
      maximumAge: 0
    };

    function success(pos: Position) {
      self.lastPosition = pos;
      callback(true);
    }

    function error(err: PositionError) {
      console.warn(`ERROR(${err.code}): ${err.message}`);
      self.lastPosition = null;

      if (self.subscribe) {
        self.subscribe.unsubscribe();
      }
      
      callback(false);
    }

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(success, error, options);
    }
    else {
      callback(false);
    }
  }

  subscribeGeoLocation(callback: SuccessCallback): void {

    const self = this;

    const options = {
      enableHighAccuracy: true,
      maximumAge: 0
    };

    function success(pos: Position) {
      self.lastPosition = pos;
      callback(true);
    }

    function error(err: PositionError) {
      console.warn(`ERROR(${err.code}): ${err.message}`);
      self.lastPosition = null;

      callback(false);
    }

    if (navigator.geolocation) {
      self.watchId = navigator.geolocation.watchPosition(success, error, options);
    }
    else {
      callback(false);
    }
  }

  unsubscribeGeoLocation(watchId: number) {
    if (navigator.geolocation) {
      navigator.geolocation.clearWatch(watchId);
    }
  }

  hasGpsLock(): boolean {
    if (this.lastPosition && this.currentState === GpsIntroState.Intro) {
      return true;
    }

    return false;
  }

  showDistance(): boolean {
    if (this.currentDistance > 0 && this.currentState !== GpsIntroState.Done) {
      return true;
    }

    return false;
  }

  showLostGpsLock(): boolean {
    return !this.hasGpsLock() && this.pastIntro && this.currentState !== GpsIntroState.Done;
  }

  loseGpsLock() {
    this.lastPosition = null;
  }

  goNext() {
    this.sleep(WAIT_AFTER_DONE).then(() => {
      this.signalSolved();
    });
  }

  textJokeComplete() {
    if (this.currentState === GpsIntroState.Done) {
      this.goNext();
    }
  }

  textComplete(): void {
    if (this.hasGpsLock()) {
      this.pastIntro = true;
      this.saveGps();

      if (!this.watchId) {
        /* Start gps polling */
  
        this.subscribeGeoLocation(succes => this.saveGps());
      }
    }
    else if (this.currentState === GpsIntroState.Done) {
      this.sleep(WAIT_FOR_PROCEED).then(() => {
        this.textJoke = "We're mostly kidding about the last part, mostly...";
        this.showJoke = true;
      });
    }
  }

  saveGps(): void {
    if (this.hasGpsLock()) {
      console.log("Saving gps-intro");

      if (this.currentState !== GpsIntroState.Unknown) {
        const atempt = new GpsIntroAtempt(RIDDLE_NAME, this.currentState, 
          this.lastPosition.coords.latitude, this.lastPosition.coords.longitude);
  
        this.api.atempt(atempt).subscribe(
          x => {
            console.log('saveGps got value: ', x);
            
            const gpsIntroResult = <GpsIntroResult>x;

            this.currentDistance = gpsIntroResult.distance;

            if (gpsIntroResult.subState === GpsIntroState.Done) {
              this.unsubscribeGeoLocation(this.watchId);
            }

            this.textSelector(gpsIntroResult.subState);
          },
          err => console.log('saveGps got error: ', err)
        );
      }
    }
  }

  textSelector(subState: GpsIntroState): void {
    if (this.currentState !== subState) {
      this.currentState = subState;

      if (this.hasGpsLock()) {
        this.text = "Got GPS lock. Time to go for a walk, " + this.gpsTask.description;
      }
      else if (subState === GpsIntroState.Done) {
        this.text = "Signal acquired, Hacking.................. Target breached, loading data. \
      RXZlcnl0aGluZyBpcyBu \
      b3QgYXMgaXQgc2VlbXMs \
      IGJlIGF3YXJlIG9mIEdG \
      Lg== \
      Data download complete. It might be prudent to start running now...";
      }
      else {
        this.text = "In order to establish a secure lock to our satellites you have to enable GPS location tracking.";
      }
    }
  }

  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
    if (this.player) {
      this.text = " ";

      this.api.getGpsTask({ state: this.player.state}).subscribe(
        result => {
            this.gpsTask = result;
            this.getGeoLocation(success => {
            this.textSelector(this.player.subState);
          });
        },
        err => console.log("gps-intro.componenet: Failed to get gpsTask", err)
      );
    }
  }
}
