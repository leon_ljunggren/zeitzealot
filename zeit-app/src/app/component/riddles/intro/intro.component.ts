import { Component, OnInit } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { RiddleComponent } from '../riddle.component';
import { RiddleState, RiddleAtempt, RiddleResult } from '../../../shared/riddle-models';
import { IntroAtempt, IntroResult } from '../../../shared/intro-models';
import { RiddleApiService } from '../../../riddle-api.service';

const WAIT_FOR_INPUT_FINISHED = 500;
const RIDDLE_NAME = RiddleState.Intro;

@Component({
  selector: 'app-riddle-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent extends RiddleComponent implements OnInit {

  results = {
    text: null,
    dateCorrect: false
  };

  data: IntroAtempt = new IntroAtempt(RIDDLE_NAME, '', '');

  private saveTexts = new Subject<IntroAtempt>();

  saveDate(term: string): void {
    this.data = new IntroAtempt(RIDDLE_NAME, term.trim().toLocaleLowerCase(), this.data.author);

    console.log("Trying to update date to ", this.data);
    this.saveTexts.next(this.data);
    this.results.dateCorrect = false;
  }

  saveAuthor(author: string): void {
    this.data = new IntroAtempt(RIDDLE_NAME, this.data.date, author.trim().toLocaleLowerCase());

    console.log("Trying to update author to ", this.data);
    this.saveTexts.next(this.data);
  }

  resultsFromServer(res: IntroResult): void {
    if (res.churchill) {
      this.results.text = "My, my, perhaps someone should have payed more atention in class.\
       He said many things, but not those exact words.";
    } 
    else if (res.correctDate && res.correctAuthor) {
      this.results.text = "Excellent, stand by...";

      this.sleep(5000).then(any => {
        this.signalSolved();
      });

    } 
    else if (res.correctDate && !this.results.dateCorrect) {
      this.results.dateCorrect = true;
      this.results.text = "Well, you got the date right, there's still hope we guess";
    } 
    else if (this.data.date !== "" && !this.results.dateCorrect) {
      this.results.text = "Come now, if you can't even get a simple date correct we might have to reconsider our intrest in you";
    } 
    else {
      this.results.text = "";
    }
  }

  constructor(private api: RiddleApiService) {
    super();
   }

  ngOnInit() {
    console.log("Showing intro");
    this.saveTexts.pipe(
      debounceTime(WAIT_FOR_INPUT_FINISHED),
      distinctUntilChanged((a, b) => JSON.stringify(a) === JSON.stringify(b)),
      switchMap((data: IntroAtempt) => this.api.atempt(data))
    )
      .subscribe(
        x => { console.log('intro.saveData got value: ', x); this.resultsFromServer(<IntroResult>x); },
        err => console.log('intro.saveDate got error: ', err),
        () => console.log('intro.saveDate got a complete notification')
      );
  }
}
