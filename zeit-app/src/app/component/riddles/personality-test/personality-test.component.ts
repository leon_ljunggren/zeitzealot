import { Component, OnInit, Input } from '@angular/core';

import { RiddleComponent } from '../riddle.component';
import { RiddleState, RiddlePlayer } from '../../../shared/riddle-models';
import { RiddleApiService } from '../../../riddle-api.service';
import { PersonalityTestResult, PersonalityTestAtempt, PersonalityTestState } from '../../../shared/personality-test-models';

const RIDDLE_NAME = RiddleState.PersonalityTest;
const WAIT_AFTER_DONE = 10000;
const WAIT_TO_PROCEED = 3000;
const WAIT_TO_SHOW_OPTIONS = 1000;

class PersonalityTestOptions {
  text: string;
  answer: string;
  hidden: boolean;
  textInput: boolean;
}

const MEN_IN_BLACK_OPTIONS = [{ text: "The girl", answer: "girl", hidden: false, textInput: false }, 
{ text: "The alien", answer: "alien", hidden: false, textInput: false }];

const CELL_OPTIONS = [{ text: "Push the button", answer: "push", hidden: false, textInput: false }, 
{ text: "Do nothing", answer: "nothing", hidden: false, textInput: false }];

const NUKE_OPTIONS = [{ text: "Fire ze missiles!", answer: "launch", hidden: false, textInput: false }, 
{ text: "Ignore orders", answer: "nothing", hidden: true, textInput: false }];

const CANDLE_OPTIONS = [{ text: "", answer: "launch", hidden: false, textInput: true }];

@Component({
  selector: 'app-riddle-personality-test',
  templateUrl: './personality-test.component.html',
  styleUrls: ['./personality-test.component.scss']
})
export class PersonalityTestComponent extends RiddleComponent implements OnInit {

  

  @Input() player: RiddlePlayer;
  text = "";
  currentTestId = PersonalityTestState.Unknown;

  options: PersonalityTestOptions[];
  showOptions = false;

  textComplete(): void {

    if (this.currentTestId === PersonalityTestState.Done) {
      this.sleep(WAIT_AFTER_DONE).then(() => {
        this.signalSolved();
      });
    }
    else if (this.hasOptions()) {
      this.sleep(WAIT_TO_SHOW_OPTIONS).then(() => {
        this.showOptions = true;
      });
    }
    else if (this.currentTestId === PersonalityTestState.MenInBlackBadReaction) {
      /* If they want to refresh fast, we don't wait to proceed on this one, since it doesn't
       * procced on its own anyway. */
      this.saveAnswer("proceed");
    }
    else if (this.currentTestId >= 0) {
      this.sleep(WAIT_TO_PROCEED).then(() => {
        this.saveAnswer("proceed");
      });
    }
  }

  hasOptions(): boolean {
    return this.currentTestId === PersonalityTestState.MenInBlackQuestion || 
    this.currentTestId === PersonalityTestState.CellQuestion || 
    this.currentTestId === PersonalityTestState.NukeQuestion ||
    this.currentTestId === PersonalityTestState.CandleQuestion;
  }

  saveAnswer(answer: string): void {
    console.log("Saving personality answer");

    const atempt = new PersonalityTestAtempt(RIDDLE_NAME, this.currentTestId, answer);

    this.api.atempt(atempt).subscribe(
      x => { console.log('saveAnswer got value: ', x); this.testSelector((<PersonalityTestResult>x).subState); },
      err => console.log('saveAnswer got error: ', err),
      () => console.log('saveAnswer got a complete notification')
    );
  }

  testSelector(testState: PersonalityTestState): void {

    this.currentTestId = testState;
    this.showOptions = false;

    switch (testState) {
      case PersonalityTestState.Intro: this.intro(); break;
      case PersonalityTestState.MenInBlackQuestion: this.menInBlackQuestion(); break;
      case PersonalityTestState.MenInBlackBadReaction: this.menInBlackBadReaction(); break;
      case PersonalityTestState.MenInBlackResurection: this.menInBlackResurection(); break;
      case PersonalityTestState.MenInBlackGoodReaction: this.menInBlackGoodReaction(); break;
      case PersonalityTestState.CellQuestion: this.cellQuestion(); break;
      case PersonalityTestState.CellNoble: this.cellNobel(); break;
      case PersonalityTestState.CellNothing: this.cellNothing(); break;
      case PersonalityTestState.NukeQuestion: this.nukeQuestion(); break;
      case PersonalityTestState.NukeBadReaction: this.nukeBadReaction(); break;
      case PersonalityTestState.NukeGoodReaction: this.nukeGoodReaction(); break;
      case PersonalityTestState.CandleQuestion: this.candleQuestion(); break;
      case PersonalityTestState.CandleGoodReaction: this.candleGoodReaction(); break;
      case PersonalityTestState.CandleBadReaction: this.candleBadReaction(); break;
      case PersonalityTestState.Done: this.done(); break;
      default: this.error();
    }
  }

  error(): void {
    this.text = "In the begging there was light... then it went out, because something went horrible wrong.";
  }

  intro(): void {
    this.text = this.player.nickname + ", we are gathering a team to change the world. \
      But we only want the best of the best of the best of the best. \
      So lets get to know you shall we.";
  }

  menInBlackQuestion(): void {
    this.options = MEN_IN_BLACK_OPTIONS;
    this.text = "You're an agent in black, tasked with protecting earthlings from aliens and aliens from earthlings. \
    You're in a back ally, on a mission hunting a vicious creature, as you hear an earth shattering scream. You \
    quickly assert the situation. In the middle of the ally there's a young girl, carrying two school books and \
    skipping along with her music without a care in the world. A few meters behind her there's a lizard looking alien, \
    hanging from a street sign and looking at you with big scared eyes. You quickly draw your gun and shoot:";
  }

  menInBlackBadReaction(): void {
    this.text = "You monster! How could you! We want people with a wide variate of skills, but child killing isn't one of them. \
    We will never work with one such as you!. Good bye!";
  }

  menInBlackResurection(): void {
    this.text = "Oh, you again? Weeeell, we guess we tricked you with that obvious Men in Black reference. \
    We guess we can give you a second chance, but make no mistake. There are some things we simply WILL NOT \
    tolerate. If you show any such tendencies in the future, you will not just be \
    eliminated from consideration, you will simply be eliminated...";
  }

  menInBlackGoodReaction(): void {
    this.text = "Why would you kill Alfred? He was just hanging out.";
  }

  partnerType(): string {
    return this.player.partner.toLowerCase();
  }

  partnerSex(pronoun: boolean): string {
    if (this.player.partner.toLowerCase() === "wife" || this.player.partner.toLowerCase() === "girlfriend") {
      return pronoun ? "her" : "she";
    }

    return pronoun ? "him" : "he";
  }

  cellQuestion(): void {
    this.options = CELL_OPTIONS;
    this.text = "You wake up in a bare cell, the only distinguishing features are a big red button and a timer slowly counting down to \
    zero. A voice plays from a hidden speaker above you. It tells you that your " + this.partnerType() + " is in \
    another identical cell and once the timer expires at least one of you will die. \
    If you push the button and " + this.partnerSex(false) + " does not then you will die and " + this.partnerSex(false) + " will \
    be freed. If " + this.partnerSex(false) + " push the button and you do not, " + this.partnerSex(false) + " will die \
    and you will be freed. If both of you push the button you will both die. If neither of you push the button \
    you will both die. What do you do?";
  }

  cellNobel(): void {
    this.text = "How noble of you. Lets just hope it wouldn't be in vain.";
  }

  cellNothing(): void {
    this.text = "Interesting... what do you think " + this.partnerSex(false) + " would think about that?";
  }

  nukeQuestion(): void {
    this.options = NUKE_OPTIONS;
    this.text = "Your name is John Bordne, the year is 1962, you are in command of a US nuclear \
    missile complex and you have just been given the order to launch everything at the Soviet Union. \
    The order came in your daily update as an encrypted attachment, all authorization and \
    verification codes checks out. You request that the order be repeated and they come back \
    loud and clear, launch NOW. Your second in command urges you to launch. What do you do?";
  }

  nukeBadReaction(): void {
    this.text = "Congratulations, you just started World War 3 and doomed the world. \
    We guess it's a good thing that you are not John Bordne. The real John Bordne \
    disobeyed his orders and broke protocol by requesting clarification over \
    an open channel. The panic in the voices of missile command as they realized \
    they had sent the orders by mistake and had been just seconds from annihilation \
    must have been a thing of beauty.";
  }

  nukeGoodReaction(): void {
    this.text = "Excellent you can think outside of the box... or you just hit the correct \
    button by mistake and just lucked your way out of World War 3. Either way, we can use that.";
  }

  candleQuestion(): void {
    this.options = CANDLE_OPTIONS;
    this.text = "In a world where a standard candle isn't so standard. What ultimate your mother joke \
    would no longer be valid.";
  }

  candleGoodReaction(): void {
    this.text = "Good, we knew you would get it.";
  }

  candleBadReaction(): void {
    this.text = "Umm, no. It doesn't look like you quite got that one. Not that that's a surprise.";
  }

  done(): void {
    this.text = "Well, this was fun, fun but pointless. We do of course already know everything there \
    is to know about you. We know what you like and what you don't. We know all your secrets \
    (don't worry, we won't tell anyone about your second account, or that one time \
    with " + this.partnerSex(true) + "). We know what you did last summer. We also know that you \
    have already broken the rules and as we promised, there will be consequences.";
  }

  constructor(private api: RiddleApiService) {
    super();
    this.options = [];
  }

  ngOnInit() {
    if (this.player) {
      this.testSelector(this.player.subState);
    }
  }
}
