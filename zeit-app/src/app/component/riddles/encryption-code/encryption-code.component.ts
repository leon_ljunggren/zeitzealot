import { Component, OnInit, Input } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { RiddleComponent } from '../riddle.component';
import { RiddlePlayer, RiddleState } from '../../../shared/riddle-models';
import { RiddleApiService } from '../../../riddle-api.service';
import { EncryptionCodeAtempt, EncryptionCodeResult } from '../../../shared/encryption-code-models';

const WAIT_TO_PROCEED = 3000;
const WAIT_FOR_INPUT_FINISHED = 500;

@Component({
  selector: 'app-riddle-encryption-code',
  templateUrl: './encryption-code.component.html',
  styleUrls: ['./encryption-code.component.scss']
})
export class EncryptionCodeComponent extends RiddleComponent implements OnInit {

  @Input() player: RiddlePlayer;
  text = "";
  intro = true;
  showEncryptionCodeInput = false;
  enc1 = "";
  enc2 = "";
  enc3 = "";
  enc4 = "";
  enc5 = "";

  private saveTexts = new Subject<EncryptionCodeAtempt>();

  goNext() {
    this.sleep(WAIT_TO_PROCEED).then(() => {
      this.signalSolved();
    });
  }

  save() {
    console.log(this.enc1, this.enc2, this.enc3, this.enc4, this.enc5);

    const code = this.enc1 + "-" + this.enc2 + "-" + this.enc3 + "-" + this.enc4 + "-" + this.enc5;
    const data = new EncryptionCodeAtempt(RiddleState.EncryptionCode, code);
    
    this.saveTexts.next(data);
  }

  textComplete(): void {

    if (this.intro) {
      this.sleep(WAIT_TO_PROCEED).then(() => {
        this.text = "Enter encryption key";
        this.intro = false;
      });
    }
    else {
      this.showEncryptionCodeInput = true;
    }
  }

  resultsFromServer(res: EncryptionCodeResult): void {
    if (res.correct) {
      this.text = "Code accepted";
      this.goNext();
    }
  }
  
  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
    if (this.player) {
      this.text = "It is time to tell you more, but this line is not secure enough. We will have to go oldschool...";

      this.saveTexts.pipe(
        debounceTime(WAIT_FOR_INPUT_FINISHED),
        distinctUntilChanged((a, b) => JSON.stringify(a) === JSON.stringify(b)),
        switchMap((data: EncryptionCodeAtempt) => this.api.atempt(data))
      )
        .subscribe(
          x => { console.log('encryption.saveData got value: ', x); this.resultsFromServer(<EncryptionCodeResult>x); },
          err => console.log('encryption.saveDate got error: ', err),
          () => console.log('encryption.saveDate got a complete notification')
        );
    }
  }
}
