import { Component, OnInit } from '@angular/core';

import { RiddleComponent } from '../riddle.component';
import { RiddleState } from '../../../shared/riddle-models';
import { RiddleApiService } from '../../../riddle-api.service';
import { NicknameResult, NicknameAtempt } from '../../../shared/nickname-models';


const RIDDLE_NAME = RiddleState.Nickname;

@Component({
  selector: 'app-riddle-nickname',
  templateUrl: './nickname.component.html',
  styleUrls: ['./nickname.component.scss']
})
export class NicknameComponent extends RiddleComponent implements OnInit {

  text = "What should we call you?";
  inputVisible = false;
  inputDisabled = false;
  reset = false;
  finished = false;
  nickname: string;

  showInput(): void {
    
    if (!this.finished) {
      if (this.reset) {
        this.sleep(2000).then(any => {
          this.text = "What should we call you?";
          this.inputVisible = true;
        });
      }
      else {
        this.inputVisible = true;
      }
    }
    else {
      this.sleep(1000).then(() => {
        this.signalSolved();
      });
    }
    
  }

  resultsFromServer(res: NicknameResult): void {

    console.log("Nickname got result: ", res);

    if (res.neo) {
      this.text = "Mr. Anderson. You disappoint us.";
      this.reset = true;
    }
    else if (res.alreadyExists) {
      this.text = "My, my, you sure are popular " + this.nickname + ". Choose another one";
      this.reset = true;
    }
    else if (res.success) {
      if (res.realName) {
        this.finished = true;
        this.text = "Using your real name eh? That is bold... we like it!";
      }
      else {
        this.signalSolved();
      }
    }
    else {
      this.text = "Try again";
      this.reset = true;
    }
  }

  saveNick(nick: string): void {
    console.log("Saving nickname");
    this.inputVisible = false;

    this.nickname = nick;
    const atempt = new NicknameAtempt(RIDDLE_NAME, nick);

    this.api.atempt(atempt).subscribe(
      x => { console.log('atempt got value: ', x); this.resultsFromServer(<NicknameResult>x); },
      err => console.log('atempt got error: ', err),
      () => console.log('atempt got a complete notification')
    );
  }

  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
  }
}
