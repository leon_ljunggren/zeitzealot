import { Component, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { RiddleComponent } from '../riddle.component';
import { RiddlePlayer, RiddleState, RiddleAtempt } from '../../../shared/riddle-models';
import { StationsResult, StationsState, FinalStationsResult, FinalStationsAtempt } from '../../../shared/stations-models';
import { RiddleApiService } from '../../../riddle-api.service';


const WAIT_TO_PROCEED = 2000;
const WAIT_FOR_INPUT_FINISHED = 1500;
const WAIT_AFTER_DONE = 15000;

@Component({
  selector: 'app-riddle-final-station',
  templateUrl: './final-station.component.html',
  styleUrls: ['./final-station.component.scss']
})
export class FinalStationComponent extends RiddleComponent implements OnInit {

  @Input() player: RiddlePlayer;
  stations = [];
  oldStations = [];
  text: string;
  done = false;
  subState: StationsState = StationsState.InProgress;

  frenchCode: string;
  usCode: string;
  chineseCode: string;
  britishCode: string;
  russianCode: string;

  frenchDone = false;
  usDone = false;
  chineseDone = false;
  britishDone = false;
  russianDone = false;

  private saveTexts = new Subject<FinalStationsAtempt>();

  goNext() {
    this.sleep(WAIT_AFTER_DONE).then(() => {
      this.signalSolved();
    });
  }

  save() {

    const data = new FinalStationsAtempt(RiddleState.FinalStation, this.frenchCode, 
      this.usCode, this.chineseCode, this.britishCode, this.russianCode);
    
    this.saveTexts.next(data);
  }

  textComplete(): void {
    if (this.subState === StationsState.Done) {
      this.sleep(WAIT_TO_PROCEED).then(() => {
        this.done = true;
        this.saveTexts.pipe(
          debounceTime(WAIT_FOR_INPUT_FINISHED),
          distinctUntilChanged((a, b) => JSON.stringify(a) === JSON.stringify(b)),
          switchMap((data: FinalStationsAtempt) => this.api.atempt(data))
        )
          .subscribe(
            x => { console.log('FinalStation.saveData got value: ', x); this.handleResponse(<FinalStationsResult>x); },
            err => console.log('FinalStation.saveDate got error: ', err),
            () => console.log('FinalStation.saveDate got a complete notification')
          );
      });
    }
  }

  showCodes(): boolean {
    return this.done;
  }

  handleResponse(res: FinalStationsResult) {
    this.stations = res.stations;
    this.oldStations = res.oldStations;

    this.subState = res.subState;

    if (res.subState === StationsState.Done) {
      this.text = "Excellent, time to analyze what we have gathered.";

      this.frenchDone = res.frenchDone;
      this.usDone = res.usDone;
      this.chineseDone = res.chineseDone;
      this.britishDone = res.britishDone;
      this.russianDone = res.russianDone;

      if (res.allDone) {
        this.goNext();
      }
    }
  }

  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
    if (this.player) {
      this.text = " ";
      const atempt = new RiddleAtempt(RiddleState.FinalStation);

      this.api.atempt(atempt).subscribe(
        x => this.handleResponse(<FinalStationsResult>x),
        err => console.log('FinalStation got error: ', err),
        () => console.log('FinalStations got a complete notification')
      );
    }
  }
}
