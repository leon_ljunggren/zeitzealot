import { Component, OnInit, Input } from '@angular/core';

import { RiddleComponent } from '../riddle.component';
import { RiddlePlayer, RiddleState, RiddleAtempt } from '../../../shared/riddle-models';
import { RiddleApiService } from '../../../riddle-api.service';

const WAIT_TO_PROCEED = 3000;

@Component({
  selector: 'app-riddle-pre-tools',
  templateUrl: './pre-tools.component.html',
  styleUrls: ['./pre-tools.component.scss']
})
export class PreToolsComponent extends RiddleComponent implements OnInit {

  @Input() player: RiddlePlayer;
  text = "";

  goNext() {
    this.sleep(WAIT_TO_PROCEED).then(() => {
      this.signalSolved();
    });
  }

  textComplete(): void {

    const atempt = new RiddleAtempt(RiddleState.PreTools);

    this.api.atempt(atempt).subscribe(
      x => this.goNext(),
      err => console.log('pre-tools got error: ', err),
      () => console.log('pre-tools got a complete notification')
    );
  }
  
  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
    if (this.player) {
      this.text = "In order to prove yourself worthy of joining us you will have to pass our tests. \
      To do that you will need to gather your tools.";
    }
  }
}
