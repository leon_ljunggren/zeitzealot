import { Component, OnInit, Input } from '@angular/core';

import { RiddleComponent } from '../riddle.component';
import { RiddlePlayer, RiddleState, RiddleAtempt } from '../../../shared/riddle-models';
import { RiddleApiService } from '../../../riddle-api.service';

const WAIT_TO_PROCEED = 10000;


@Component({
  selector: 'app-riddle-pre-final-station',
  templateUrl: './pre-final-station.component.html',
  styleUrls: ['./pre-final-station.component.scss']
})
export class PreFinalStationComponent extends RiddleComponent implements OnInit {

  @Input() player: RiddlePlayer;
  text = "";

  goNext() {
    this.sleep(WAIT_TO_PROCEED).then(() => {
      this.signalSolved();
    });
  }

  textComplete(): void {

    const atempt = new RiddleAtempt(RiddleState.PreFinalStation);

    this.api.atempt(atempt).subscribe(
      x => this.goNext(),
      err => console.log('PreFinalStations got error: ', err),
      () => console.log('PreFinalStations got a complete notification')
    );
  }

  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
    if (this.player) {
      this.text = "We have learned of the existence of one more station, it is so remote that it had escaped our notice before. \
      The tracking signal we have acquired is very weak, but we believe this can be used to our advantage. It seems that the \
      creators of the station are relying on its remoteness to keep it secure and have used a somewhat lesser \
      encryption algorithm. This means we should be able to gain access using fewer agents than what was needed for the others.";
    }
  }
}
