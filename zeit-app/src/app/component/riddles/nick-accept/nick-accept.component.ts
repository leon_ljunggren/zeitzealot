import { Component, OnInit, Input } from '@angular/core';

import { RiddleComponent } from '../riddle.component';
import { RiddlePlayer, RiddleState, RiddleAtempt } from '../../../shared/riddle-models';
import { RiddleApiService } from '../../../riddle-api.service';

@Component({
  selector: 'app-riddle-nick-accept',
  templateUrl: './nick-accept.component.html',
  styleUrls: ['./nick-accept.component.scss']
})
export class NickAcceptComponent extends RiddleComponent implements OnInit {

  @Input() player: RiddlePlayer;
  text = "";

  goNext() {
    this.sleep(3000).then(() => {
      this.signalSolved();
    });
  }

  textComplete(): void {

    const atempt = new RiddleAtempt(RiddleState.NickAccept);

    this.api.atempt(atempt).subscribe(
      x => this.goNext(),
      err => console.log('nick-accept got error: ', err),
      () => console.log('nick-accept got a complete notification')
    );
  }
  
  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
    if (this.player) {
      this.text = "Hello " + this.player.nickname + ", we have had our eyes on you for quite some time now.\
      You've peaked our interest, but there are rules. You may not speak of this to anyone, not even a whisper.\
       If you do, we'll know and there will be consequences...";

    }
  }
}
