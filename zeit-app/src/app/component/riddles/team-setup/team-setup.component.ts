import { Component, OnInit, Input } from '@angular/core';

import { RiddleComponent } from '../riddle.component';
import { RiddlePlayer, RiddleState, RiddleAtempt } from '../../../shared/riddle-models';
import { RiddleApiService } from '../../../riddle-api.service';

const WAIT_FOR_PROCEED = 1000;


@Component({
  selector: 'app-riddle-team-setup',
  templateUrl: './team-setup.component.html',
  styleUrls: ['./team-setup.component.scss']
})
export class TeamSetupComponent extends RiddleComponent implements OnInit {

  @Input() player: RiddlePlayer;
  teamVisible = false;
  linkVisible = false;
  proceedVisible = false;

  textIntro = "";
  textTeam = " ";
  textRecruit = " ";

  textCompleteIntro(): void {
    this.sleep(WAIT_FOR_PROCEED).then(() => {
      this.teamVisible = true;
      this.textTeam = "Team Omega consists of Ishmael, Smalle, Acromion and Malacoda. \
      To make things more interesting, you must recruit two more members. The love doctor and the editor. \
      You can monitor the progress of the recruitment using the link below, be sure to save it so you can return later.";
    });
  }

  textCompleteTeam(): void {
    if (this.teamVisible) {
      this.sleep(WAIT_FOR_PROCEED).then(() => {
        this.linkVisible = true;
        this.textRecruit = "We can not wait for them to catch up before proceeding, but they, and every other member of the team, \
        must be above the line before the end of the phase.";
      });
    }
  }

  textCompleteRecruit(): void {
    if (this.linkVisible) {
      this.sleep(WAIT_FOR_PROCEED).then(() => {
        this.proceedVisible = true;
      });
    }
  }

  proceed(): void {
    const atempt = new RiddleAtempt(RiddleState.TeamSetup);

    this.api.atempt(atempt).subscribe(
      x => this.signalSolved(),
      err => console.log('team-setup got error: ', err),
      () => console.log('team-setup got a complete notification')
    );
  }

  constructor(private api: RiddleApiService) {
    super();
  }

  ngOnInit() {
    this.textIntro = "We know you have been talking to some of the others, even though that was explicitly forbidden. \
    We promised there would be consequences for that. Well, here they are: you are now working together as a team, \
    that means you can consult each other, exchange information and work on clues together. But it does also mean that \
    you pass and fail as a team, all of you have to be above the line in order to pass, or none of you will.";
  }
}
