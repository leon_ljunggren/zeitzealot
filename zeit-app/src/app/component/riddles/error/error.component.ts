import { Component, OnInit, Input } from '@angular/core';
import { RiddlePlayer } from '../../../shared/riddle-models';
import { PlayerIndex } from '@angular/core/src/render3/interfaces/player';

@Component({
  selector: 'app-riddle-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {

  @Input() player: RiddlePlayer;
  
  isAdmin(): boolean {
    if (this.player) {
      return this.player.domain === 'localhost' || this.player.domain === '192.168.1.10' || this.player.domain === 'mastermind';
    }

    return false;
  }

  toString(): string {
    return JSON.stringify(this.player);
  }

  constructor() { }

  ngOnInit() {
  }

}
