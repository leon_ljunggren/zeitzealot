import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.scss']
})
export class InviteComponent implements OnInit {

  text = "";
  speed = 80;
  showDoc = false;

  textComplete(): void {
    this.showDoc = true;
    console.log("Text complete");
  }

  constructor() { }

  ngOnInit() {
    this.text = "November 16th, 19:00, Ramsdalsvegen 55... \
    we know you have other plans, CANCEL THEM! Bring the data from the stations.\
                                  There will be cake.";
  }
}
