import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { RiddleApiService } from '../../riddle-api.service';
import { FinalResult, FinalState, FinalAtempt } from '../../shared/final-models';
import { CommService } from '../../comm.service';

const WAIT_FOR_INPUT_FINISHED = 1500;
const WAIT_AFTER_DONE = 10000;

   /* Hack to now show this component until a set time after server has ordered startup,
   * a proper way of doing it would be to send an event after the starting video is done.
   */
const WAIT_AFTER_STARTUP = 92000;


@Component({
  selector: 'app-final',
  templateUrl: './final.component.html',
  styleUrls: ['./final.component.scss']
})
export class FinalComponent implements OnInit {

  text: string;
  frenchCode: string;
  usCode: string;
  chineseCode: string;
  britishCode: string;
  russianCode: string;

  self1: string;
  self2: string;
  self3: string;
  self4: string;

  frenchDone = false;
  usDone = false;
  chineseDone = false;
  britishDone = false;
  russianDone = false;

  morseDone = false;
  keyDone = false;
  potentimeterDone = false;
  toneDone = false;

  private saveTexts = new Subject<FinalAtempt>();

  state: FinalState;
  selfDestructCodesVisile = false;

  selfDestructText = "Activate Self Destruct";
  color = "red";
  borderColor = "#8b0000";

  private _sleep(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  async sleep(ms: number) {
    await this._sleep(ms);
  }

  save() {

    const data = new FinalAtempt(this.frenchCode, 
      this.usCode, this.chineseCode, this.britishCode, this.russianCode,
      this.self1, this.self2, this.self3, this.self4, false);
    
    this.saveTexts.next(data);
  }

  sendSelfDestruct() {

    const data = new FinalAtempt(this.frenchCode, 
      this.usCode, this.chineseCode, this.britishCode, this.russianCode,
      this.self1, this.self2, this.self3, this.self4, true);

      this.selfDestructText = "Self Destruct Activated";
      this.color = "green";
      this.borderColor = "darkgreen";

    this.api.finalAtempt(data).subscribe(
      x => console.log('finalAtempt got success: ', x),
      err => console.log('finalAtempt got error: ', err)
    );
  }

  showCodes(): boolean {
    return this.state === FinalState.LaunchCodes;
  }

  showText(): boolean {
    return this.state === FinalState.SelfDestructCodes && !this.selfDestructCodesVisile;
  }

  showSelfDestruct(): boolean {
    return this.state === FinalState.SelfDestructCodes && this.selfDestructCodesVisile;
  }

  showSelfDestructButton(): boolean {
    return this.state === FinalState.SelfDestructComplete;
  }

  textComplete(): void {
    if (this.showText()) {
      console.log("Send message to all that the codes are done");
    }
  }

  launch(): void {
    console.log("All codes are done");
    /* Send the start command to the other service (map/game etc) */
    this.com.start();

    /* Hack to not show the self destruct codes until the video is done, 
     * just sleeps for as long as the video are suposed to play.
     */
    this.sleep(WAIT_AFTER_STARTUP).then(() => {
        this.selfDestructCodesVisile = true;
    });
  }

  handleResponse(res: FinalResult) {
    this.state = res.state;
  
    this.frenchDone = res.frenchDone;
    this.usDone = res.usDone;
    this.chineseDone = res.chineseDone;
    this.britishDone = res.britishDone;
    this.russianDone = res.russianDone;

    this.morseDone = res.self1Done;
    this.keyDone = res.self2Done;
    this.potentimeterDone = res.self3Done;
    this.toneDone = res.self4Done;

    if (res.allDoneLaunch) {
      console.log("All launch codes are correct");
      this.text = "Excellent, stand by...";
      this.sleep(WAIT_AFTER_DONE).then(() => {
        this.launch();
      });
    }
  }

  constructor(private api: RiddleApiService, private com: CommService) { }

  ngOnInit() {

    this.text = " ";
    this.state = FinalState.LaunchCodes;

    this.api.getFinalAtemptState().subscribe(
      x => { this.state = x; if (this.state === FinalState.SelfDestructCodes) {
          this.selfDestructCodesVisile = true;
        }
      },
      err => console.log('getFinalAtemptState got error: ', err)
    );

    

    this.saveTexts.pipe(
      debounceTime(WAIT_FOR_INPUT_FINISHED),
      distinctUntilChanged((a, b) => JSON.stringify(a) === JSON.stringify(b)),
      switchMap((data: FinalAtempt) => this.api.finalAtempt(data))
    )
      .subscribe(
        x => { console.log('FinalStation.saveData got value: ', x); this.handleResponse(<FinalResult>x); },
        err => console.log('FinalStation.saveDate got error: ', err),
        () => console.log('FinalStation.saveDate got a complete notification')
      );
  }
}
