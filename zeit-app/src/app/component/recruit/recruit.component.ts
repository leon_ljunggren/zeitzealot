import { Component, OnInit } from '@angular/core';
import { RiddleApiService } from '../../riddle-api.service';
import { RiddlePlayer, RiddleState, RecruitsStatus } from '../../shared/riddle-models';

@Component({
  selector: 'app-recruit',
  templateUrl: './recruit.component.html',
  styleUrls: ['./recruit.component.scss']
})
export class RecruitComponent implements OnInit {

  text = "";
  drLove: RiddlePlayer = { state: RiddleState.Error, subState: 0, domain: '', user: 'NA', nickname: 'NA', partner: 'NA' };
  editor: RiddlePlayer = { state: RiddleState.Error, subState: 0, domain: '', user: 'NA', nickname: 'NA', partner: 'NA' };

  isInProgress(state: RiddleState): boolean {
    return state === RiddleState.Intro || state === RiddleState.Nickname || 
      state === RiddleState.NickAccept || state === RiddleState.BeInTouch || 
      state === RiddleState.PersonalityTest || state === RiddleState.Loading || 
      state === RiddleState.PreTools || state === RiddleState.Tools || 
      state === RiddleState.GpsIntro || state === RiddleState.Error;
  }

  isStarted(state: RiddleState): boolean {
    return state !== RiddleState.Intro && state !== RiddleState.Loading && 
      state !== RiddleState.Error;
  }

  setRecruits(recruits: RecruitsStatus): void {
    this.drLove = recruits.players[0]; 
    this.editor = recruits.players[1];

    if (!this.isInProgress(this.drLove.state) && !this.isInProgress(this.editor.state)) {
      this.text = "Good, both are up to speed.";
    }
    else if (this.isStarted(this.drLove.state) || this.isStarted(this.editor.state)) {
      this.text = "There is progress with the recruits, but they have far to go yet.";
    }
  }

  constructor(private api: RiddleApiService) { }

  ngOnInit() {
    this.text = " ";
    this.api.getRecruitStatus().subscribe(
      x => this.setRecruits(x),
      err => console.log('getRecruitStatus got error: ', err),
      () => console.log('getRecruitStatus got a complete notification')
    );
  }
}
