/**
 * To generate a new riddle do:
 * ng generate component component/riddles/personality-test
 */

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularFittextModule } from 'angular-fittext';
import { FormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import localeNb from '@angular/common/locales/nb';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HackerTextComponent } from './component/hacker-text/hacker-text.component';
import { IntroComponent } from "./component/riddles/intro/intro.component";
import { NicknameComponent } from './component/riddles/nickname/nickname.component';
import { RiddleComponent } from './component/riddles/riddle.component';
import { RiddleManagerComponent } from './component/riddle-manager/riddle-manager.component';
import { NickAcceptComponent } from './component/riddles/nick-accept/nick-accept.component';
import { ErrorComponent } from './component/riddles/error/error.component';
import { LoadingComponent } from './component/riddles/loading/loading.component';
import { BeInTouchComponent } from './component/riddles/be-in-touch/be-in-touch.component';
import { PersonalityTestComponent } from './component/riddles/personality-test/personality-test.component';
import { ToolsComponent } from './component/riddles/tools/tools.component';
import { GpsIntroComponent } from './component/riddles/gps-intro/gps-intro.component';
import { PreToolsComponent } from './component/riddles/pre-tools/pre-tools.component';
import { XorComponent } from './component/xor/xor.component';
import { XorPadComponent } from './component/xor-pad/xor-pad.component';
import { HighScoreComponent } from './component/riddles/high-score/high-score.component';
import { TeamSetupComponent } from './component/riddles/team-setup/team-setup.component';
import { RecruitComponent } from './component/recruit/recruit.component';
import { BranchInfoComponent } from './component/riddles/branch-info/branch-info.component';
import { SatellitePreComponent } from './component/riddles/satellite-pre/satellite-pre.component';
import { SatelliteComponent } from './component/riddles/satellite/satellite.component';
import { EncryptionCodeComponent } from './component/riddles/encryption-code/encryption-code.component';
import { ExplanationsComponent } from './component/riddles/explanations/explanations.component';
import { PreStationsComponent } from './component/riddles/pre-stations/pre-stations.component';
import { StationsComponent } from './component/riddles/stations/stations.component';
import { StationComponent } from './component/riddles/station/station.component';
import { PreFinalStationComponent } from './component/riddles/pre-final-station/pre-final-station.component';
import { FinalStationComponent } from './component/riddles/final-station/final-station.component';
import { TeamComponent } from './component/team/team.component';
import { InviteComponent } from './component/invite/invite.component';
import { FinalComponent } from './component/final/final.component';

import { environment } from '../environments/environment';

const config: SocketIoConfig = { url: environment.api, options: {} };


@NgModule({
  declarations: [
    AppComponent,
    HackerTextComponent,
    IntroComponent,
    NicknameComponent,
    RiddleComponent,
    RiddleManagerComponent,
    NickAcceptComponent,
    ErrorComponent,
    LoadingComponent,
    BeInTouchComponent,
    PersonalityTestComponent,
    ToolsComponent,
    GpsIntroComponent,
    PreToolsComponent,
    XorComponent,
    XorPadComponent,
    HighScoreComponent,
    TeamSetupComponent,
    RecruitComponent,
    BranchInfoComponent,
    SatellitePreComponent,
    SatelliteComponent,
    EncryptionCodeComponent,
    ExplanationsComponent,
    PreStationsComponent,
    StationsComponent,
    StationComponent,
    PreFinalStationComponent,
    FinalStationComponent,
    TeamComponent,
    InviteComponent,
    FinalComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    AngularFittextModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatTableModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatSliderModule,
    MatSnackBarModule,
    MatSortModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatNativeDateModule,
    MatStepperModule,
    FormsModule,
    SocketIoModule.forRoot(config)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    registerLocaleData(localeNb, 'nb');
  }
 }
