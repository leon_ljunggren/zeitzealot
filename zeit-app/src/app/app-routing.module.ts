import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RiddleManagerComponent } from './component/riddle-manager/riddle-manager.component';
import { XorComponent } from './component/xor/xor.component';
import { XorPadComponent } from './component/xor-pad/xor-pad.component';
import { RecruitComponent } from './component/recruit/recruit.component';
import { ErrorComponent } from './component/riddles/error/error.component';
import { StationComponent } from './component/riddles/station/station.component';
import { TeamComponent } from './component/team/team.component';
import { InviteComponent } from './component/invite/invite.component';
import { FinalComponent } from './component/final/final.component';

const routes: Routes = [{ path: '', component: RiddleManagerComponent }, 
{ path: 'xor', component: XorComponent }, 
{ path: 'pad', component: XorPadComponent }, 
{ path: 'omega-recruits', component: RecruitComponent }, 
{ path: 'invite', component: InviteComponent }, 
{ path: 'final', component: FinalComponent }, 
{ path: 'alpha-recruits', component: ErrorComponent }, 
{ path: 'beta-recruits', component: ErrorComponent }, 
{ path: 'psi-recruits', component: ErrorComponent }, 
{ path: 'omega', component: TeamComponent }, 
{ path: 'alpha', component: ErrorComponent }, 
{ path: 'beta', component: ErrorComponent }, 
{ path: 'psi', component: ErrorComponent }, 
{ path: 'able', component: StationComponent }, 
{ path: 'bravo', component: StationComponent }, 
{ path: 'foxtrot', component: StationComponent }, 
{ path: 'hotel', component: StationComponent }, 
{ path: 'whiskey', component: StationComponent },
{ path: 'zulu', component: ErrorComponent }, 
{ path: 'yankee', component: ErrorComponent }, 
{ path: 'india', component: ErrorComponent }, 
{ path: 'juliett', component: ErrorComponent }, 
{ path: 'charlie', component: ErrorComponent }, 
{ path: 'delta', component: ErrorComponent }, 
{ path: 'echo', component: ErrorComponent }];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
