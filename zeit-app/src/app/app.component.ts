import { Component } from '@angular/core';

import { RiddleApiService } from './riddle-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Zeit Zealot';

  constructor(private api: RiddleApiService) {
    /* Log access */
    this.api.access().subscribe(
      x => console.log('access got value: ', x),
      err => console.log('access got error: ', err),
      () => console.log('access got a complete notification')
    );
  }
}
