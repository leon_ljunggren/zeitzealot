import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CommService {

  constructor(private socket: Socket) { }

  public start() {
    this.socket.emit('start', "");
  }

  public onStart(): Observable<string|number> {
    return this.socket.fromEvent('start');
  }
}
