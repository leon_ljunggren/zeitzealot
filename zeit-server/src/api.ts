import express from "express";
import * as bodyParser from "body-parser";
import cors from "cors";
import socketIo from 'socket.io';
import { Request, Response } from "express";
import { MysqlError } from "mysql";
import { createServer, Server } from 'http';

import { RiddleChecker } from "./modules/riddle-checker";
import { RiddleResult, RiddleState } from "./shared/riddle-models";
import { MysqlUtil } from "./modules/mysql-util";
import { apiConf } from "./shared/api-config";
import { GpsUserTaskResponse } from "./shared/gps-user-task-models";
import { MessageChannel } from "./shared-map/message-channels";
import { FinalResult } from "./shared/final-models";

/* Allow origin=* for now since we're connecting to different apps in developemnt,
 * should be turned off in production.
 */
const options: cors.CorsOptions = {
    allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
    credentials: true,
    methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
    origin: "*",
    preflightContinue: false
};

const port = 3002;


export class Api {

    public app: express.Application;
    private server: Server;
    private io: SocketIO.Server;

    constructor() {
        this.app = express();
        this.config();
        this.routes();

        this.server = createServer(this.app);
        this.initSocket();
        this.listen();
    }

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));

        this.app.use(cors(options));
    }

    private initSocket(): void {
        this.io = socketIo(this.server);
    }

    private listen(): void {

        this.server.listen(port, () => {
            console.log('Running server on port %s', port);
        });

        this.io.on('connect', (socket: any) => {
            console.log('Connected client');

            socket.on(MessageChannel.PLAY_VIDEO, (m: string) => {
                console.log('[server](%s): %s', MessageChannel.PLAY_VIDEO, JSON.stringify(m));
                this.io.emit(MessageChannel.PLAY_VIDEO, m);
            });

            socket.on(MessageChannel.LEVEL, (m: string) => {
                console.log('[server](%s): %s', MessageChannel.LEVEL, JSON.stringify(m));
                this.io.emit(MessageChannel.LEVEL, m);
            });

            socket.on(MessageChannel.GAME, (m: string) => {
                console.log('[server](%s): %s', MessageChannel.GAME, JSON.stringify(m));
                this.io.emit(MessageChannel.GAME, m);
            });

            socket.on(MessageChannel.MAP, (m: string) => {
                console.log('[server](%s): %s', MessageChannel.MAP, JSON.stringify(m));
                this.io.emit(MessageChannel.MAP, m);
            });

            socket.on(MessageChannel.START, (m: string) => {
                console.log('[server](%s): %s', MessageChannel.START, JSON.stringify(m));
                this.io.emit(MessageChannel.START, m);
            });

            socket.on(MessageChannel.ADJUST_MISSILE_SPEED, (m: string) => {
                console.log('[server](%s): %s', MessageChannel.ADJUST_MISSILE_SPEED, JSON.stringify(m));
                this.io.emit(MessageChannel.ADJUST_MISSILE_SPEED, m);
            });

            socket.on(MessageChannel.ADJUST_MAP_SPEED, (m: string) => {
                console.log('[server](%s): %s', MessageChannel.ADJUST_MAP_SPEED, JSON.stringify(m));
                this.io.emit(MessageChannel.ADJUST_MAP_SPEED, m);
            });
        });
    }

    private routes(): void {
        const router = express.Router();

        router.post(apiConf.access, (req: Request, res: Response) => {

            const domain = req.hostname;
            const atempt = req.body;

            console.log('Got access: domain=', domain, ' atempt=', atempt);

            MysqlUtil.saveHistory(domain, RiddleState.Loading, atempt, function () {
                const result = {
                    riddle: RiddleState.Loading
                };
                res.json(result);
            });
        });

        router.post(apiConf.player, (req: Request, res: Response) => {

            const domain = req.hostname;

            console.log('Got player: domain=', domain, ' query=', req.query);

            MysqlUtil.getPlayer(req.hostname, function (player: any) {
                console.log('Got player from mysql: ', player);

                res.json(player);
            });
        });

        router.post(apiConf.recruitStatus, (req: Request, res: Response) => {

            const domain = req.hostname;

            console.log('Got recruitStatus: domain=', domain, ' query=', req.query);

            MysqlUtil.getPlayer("drlove.zeitzealot.org", function (drLove: any) {
                console.log('Got dr love from mysql: ', drLove);
                MysqlUtil.getPlayer("editor.zeitzealot.org", function (editor: any) {
                    console.log('Got editor from mysql: ', editor);
                    res.json({ players: [drLove, editor] });
                });
            });
        });

        router.post(apiConf.teamStatus, (req: Request, res: Response) => {

            const domain = req.hostname;

            console.log('Got teamStatus: domain=', domain, ' query=', req.query);

            MysqlUtil.getPlayer("drlove.zeitzealot.org", function (drLove: any) {
                MysqlUtil.getPlayer("editor.zeitzealot.org", function (editor: any) {
                    MysqlUtil.getPlayer("bigcheese.zeitzealot.org", function (bigcheese: any) {
                        MysqlUtil.getPlayer("wormtongue.zeitzealot.org", function (wormtongue: any) {
                            MysqlUtil.getPlayer("littlemy.zeitzealot.org", function (littlemy: any) {
                                MysqlUtil.getPlayer("pussycat.zeitzealot.org", function (pussycat: any) {
                                    MysqlUtil.getPlayer("leatherhead.zeitzealot.org", function (leatherhead: any) {
                                        res.json({ players: [drLove, editor, bigcheese, wormtongue, littlemy, pussycat, leatherhead] });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });


        router.post(apiConf.gpsTask, (req: Request, res: Response) => {

            const domain = req.hostname;
            const request = req.body;

            console.log('Got gps task: domain=', domain, ' query=', req.query);

            MysqlUtil.getGpsTask(domain, request.state, function (gpsTask: any) {
                console.log('Got gps task from mysql: ', gpsTask);
                const task: GpsUserTaskResponse = {
                    description: ""
                };

                if (gpsTask) {
                    task.description = gpsTask.description;
                }

                res.json(task);
            });
        });

        router.post(apiConf.atempt, (req: Request, res: Response) => {

            const domain = req.hostname;
            const atempt = req.body;

            console.log('Got atempt: domain=', domain, ' atempt=', atempt);

            RiddleChecker.check(domain, atempt, function (result: RiddleResult) {
                console.log("Sending post response: ", result);
                res.json(result);
            });
        });

        router.post(apiConf.final, (req: Request, res: Response) => {

            const domain = req.hostname;
            const atempt = req.body;

            console.log('Got final: domain=', domain, ' atempt=', atempt);
            const self = this;

            RiddleChecker.final(atempt, function (result: FinalResult) {
                console.log("Sending post final response: ", result);

                if (result.activateSelfDestruct) {
                    self.io.emit(MessageChannel.SELF_DESTRUCT, "");
                }

                res.json(result);
            });
        });

        router.post(apiConf.getFinalState, (req: Request, res: Response) => {

            const domain = req.hostname;
            const request = req.body;

            console.log('Got getFinalState: domain=', domain, ' query=', req.query);

            MysqlUtil.getFinalState(function(finalState: any) {
                res.json(finalState);
            });
        });

        this.app.use('/', router);
    }
}
