INSERT INTO zeit.Player values('editor.zeitzealot.org','NA',null,'intro');
INSERT INTO zeit.Player values('drlove.zeitzealot.org','NA',null,'intro');
INSERT INTO zeit.Player values('littlemy.zeitzealot.org','NA',null,'intro');
INSERT INTO zeit.Player values('leatherhead.zeitzealot.org','NA',null,'intro');
INSERT INTO zeit.Player values('pussycat.zeitzealot.org','NA',null,'intro');
INSERT INTO zeit.Player values('wormtongue.zeitzealot.org','NA',null,'intro');
INSERT INTO zeit.Player values('mastermind.zeitzealot.org','NA',null,'intro');
INSERT INTO zeit.Player values('tortoise.zeitzealot.org','NA',null,'intro');
INSERT INTO zeit.Player values('rocketeer.zeitzealot.org','NA',null,'intro');
INSERT INTO zeit.Player values('bigcheese.zeitzealot.org','NA',null,'intro');
INSERT INTO zeit.Player values('footballer.zeitzealot.org','NA',null,'intro');
INSERT INTO zeit.Player values('localhost','NA',null,'intro');

UPDATE Player SET partner = "husband" WHERE domain = "editor.zeitzealot.org";
UPDATE Player SET partner = "husband" WHERE domain = "drlove.zeitzealot.org";
UPDATE Player SET partner = "husband" WHERE domain = "littlemy.zeitzealot.org";
UPDATE Player SET partner = "wife" WHERE domain = "leatherhead.zeitzealot.org";
UPDATE Player SET partner = "boyfriend" WHERE domain = "pussycat.zeitzealot.org";
UPDATE Player SET partner = "girlfriend" WHERE domain = "wormtongue.zeitzealot.org";
UPDATE Player SET partner = "nothing" WHERE domain = "mastermind.zeitzealot.org";
UPDATE Player SET partner = "wife" WHERE domain = "tortoise.zeitzealot.org";
UPDATE Player SET partner = "wife" WHERE domain = "rocketeer.zeitzealot.org";
UPDATE Player SET partner = "wife" WHERE domain = "bigcheese.zeitzealot.org";
UPDATE Player SET partner = "wife" WHERE domain = "footballer.zeitzealot.org";

INSERT INTO zeit.Player values('192.168.1.10','ifire',null,'intro',0,'nothing');

INSERT INTO GpsTask VALUES('localhost', 'gps-intro', 59.923082, 10.763602, 100, "head to the park.");
INSERT INTO GpsTask VALUES('192.168.1.10', 'gps-intro', 59.923082, 10.763602, 100, "head to the park.");
INSERT INTO GpsTask VALUES('littlemy.zeitzealot.org', 'gps-intro', 59.923082, 10.763602, 100, "head to the park.");
INSERT INTO GpsTask VALUES('mastermind.zeitzealot.org', 'gps-intro', 59.923082, 10.763602, 100, "head to the park.");
INSERT INTO GpsTask VALUES('tortoise.zeitzealot.org', 'gps-intro', 59.923082, 10.763602, 100, "head to the park.");
INSERT INTO GpsTask VALUES('pussycat.zeitzealot.org', 'gps-intro', 59.420125, 5.300407, 100, "head to the dam that holds back the windy water.");
INSERT INTO GpsTask VALUES('leatherhead.zeitzealot.org', 'gps-intro', 59.420125, 5.300407, 100, "head to the dam that holds back the windy water.");
INSERT INTO GpsTask VALUES('wormtongue.zeitzealot.org', 'gps-intro', 59.420125, 5.300407, 100, "head to the dam that holds back the windy water.");
INSERT INTO GpsTask VALUES('editor.zeitzealot.org', 'gps-intro', 59.969200, 10.731807, 100, "head to the water by the parish.");
INSERT INTO GpsTask VALUES('drlove.zeitzealot.org', 'gps-intro', 59.969200, 10.731807, 100, "head to the water by the parish.");
INSERT INTO GpsTask VALUES('rocketeer.zeitzealot.org', 'gps-intro', 59.819408, 17.631875, 100, "head to the crossroad");


INSERT INTO Station VALUES('Able', 'stations', 66.856010, 13.783506, 6000, 300, 3, 0, 23, '<div>红鸟导弹系统的代码。 这些代码由CryptoJs库使用AES加密，PKCS7用于填充和CBC模式。</div><div>---</div><div>6bcaadfdb63442d4475177281cce9e2d889c9a2044a9c241af390bbf8a785725Vlty8ATb14E+gYyOLU2fFXq9AHTHpkzwazHo9xsoQZs=</div>', 'location-unknown');
INSERT INTO Station VALUES('Bravo', 'stations', 59.824043, 7.192807, 6000, 200, 3, 12, 56, '<div>-----BEGIN PUBLIC KEY-----</div><div>MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKVeEC+VUvxm4TjY3tIllPQdDpL0sq5h
5NP3dqUzupKxXWZRmXMAlg1gitWnFPhYrkKO+tpwCGZYxuBAHJbEMb0CAwEAAQ==</div><div>-----END PUBLIC KEY-----</div>', 'location-unknown');
INSERT INTO Station VALUES('Foxtrot', 'stations', 59.981367, 10.796408, 6000, 200, 3, 17, 19, '<div>红鸟导弹系统代码密码。 使用MD5进行哈希处理。</div><div>---</div><div>568c31f0f2406ab70255a1d83291220f</div>', 'location-unknown');
INSERT INTO Station VALUES('Hotel', 'stations', 53.339742, -1.320333, 2000, 200, 3, 19, 5, '<div>IMPORTANT: DO NOT SHARE WITH THE FRENCH, THOSE FROG EATERS CAN NOT BE TRUSTED.</div><div>Contains the authorization codes for the Trident II.</div><div>--- RSA ---</div><div>OKiH2B8lv7N9fJit0MDqe3UfAw9M+b5/5srEoFvd1pLEs+Bt9rXA2ploebF2iPNvmQNk43AbQHzsULqpYUQZ2w==</div>', 'location-unknown');
INSERT INTO Station VALUES('Whiskey', 'final-station', 78.229301, 15.601448, 6000, 400, 3, 15, 02, '<div>-----BEGIN RSA PRIVATE KEY-----</div><div>MIIBPQIBAAJBAKVeEC+VUvxm4TjY3tIllPQdDpL0sq5h5NP3dqUzupKxXWZRmXMA<br>lg1gitWnFPhYrkKO+tpwCGZYxuBAHJbEMb0CAwEAAQJBAIwcrsoiUzXYVWu9FM4p<br>WZOcKwUIn3xxJVkTi0teEO7IR4JbM3hxSmCJzzlrkgVL323zW6rM9yRBqbLzECcl<br>fgECIQDx8gL4lyDGmuqwsJW3Y70KuvXKZ4Bwu4n13f0hJApTEQIhAK75QVxaBtGd<br>Xz6Pddqbg8EQYcja53gqAt3leLSWSpvtAiEAlMyUyzKXM84/HJ/d1EYBE8JcPDlf<br>ovWUKaR3c5RozSECIQCc0vSdBGeGKmm6VbSIycfuG3O5JVI/JlBJd6mXGfknBQIh<br>AI7G9Mc/ZxcZWEUgllmivin+Dx/Zs/8v4dIhRl6aOAjS</div><div>-----END RSA PRIVATE KEY-----</div>', 'location-unknown');