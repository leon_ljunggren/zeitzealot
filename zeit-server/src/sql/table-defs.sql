CREATE DATABASE zeit CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE USER 'zeituser'@'%' IDENTIFIED WITH mysql_native_password BY 'PDBXCQg9LwhBGZBG5cbZVR6mg3UC7a';
GRANT ALL ON zeit.* TO 'zeituser'@'%';

CREATE TABLE `Player` (
  `domain` varchar(190) ,
  `user` varchar(190) ,
  `nickname` varchar(190) ,
  `state` varchar(190) ,
  `subState` int(11) DEFAULT 0,
  `partner` varchar(190) DEFAULT "",
  PRIMARY KEY (`domain`),
  UNIQUE KEY user_index(`user`),
  UNIQUE KEY nick_index(`nickname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;

ALTER TABLE Player ADD COLUMN `subState` int(11) DEFAULT 0;
ALTER TABLE Player ADD COLUMN `partner` varchar(190) DEFAULT "";

CREATE TABLE `PlayerHistory`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(190) ,
  `riddle` varchar(190) ,
  `atempt` text ,
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY (`domain`,`riddle`),
  KEY (`riddle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE `GpsTask`(
  `domain` varchar(190),
  `state` varchar(190) ,
  `latitude` DOUBLE,
  `longitude` DOUBLE,
  `approvalRadius` int(11) DEFAULT 0,
  `description` varchar(190) DEFAULT "",
  PRIMARY KEY (`domain`, `state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE `Station`(
  `name` varchar(190) DEFAULT "",
  `state` varchar(190) ,
  `latitude` DOUBLE,
  `longitude` DOUBLE,
  `locationRadius` int(11) DEFAULT 0,
  `approvalRadius` int(11) DEFAULT 0,
  `agentsNeeded` int(11) DEFAULT 0,
  `hour` int(11) DEFAULT 0,
  `minute` int(11) DEFAULT 0,
  `data` text,
  `status` varchar(190) DEFAULT "location-unknown",
  PRIMARY KEY (`name`),
  KEY (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE `TeamTask`(
  `state` varchar(190) ,
  `complete` TINYINT,
  PRIMARY KEY (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE `FinalState`(
  `id` int(11) DEFAULT 0,
  `state` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;
