import { RiddleResult, RiddleAtempt, RiddleState } from './riddle-models';

export class NicknameResult implements RiddleResult {
    state: RiddleState;
    subState: number;
    success: boolean;
    alreadyExists: boolean;
    neo: boolean;
    realName: boolean;
}

export class NicknameAtempt extends RiddleAtempt {
    state: RiddleState;
    subState: number;
    nickname: string;

    constructor(state: RiddleState, nickname: string) {
        super(state, 0);
        this.nickname = nickname;
    }
}
