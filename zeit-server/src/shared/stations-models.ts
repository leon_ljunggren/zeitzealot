import { RiddleResult, RiddleState, RiddleAtempt } from "./riddle-models";
import { Coordinates } from "./gps-intro-models";
import { AgentsOnline } from './satellite-models';

export enum StationStatus {
    UNKNOWN = "location-unknown",
    LOCATED = "located",
    DONE = "done"
}

export class Station {
    name: string;
    done: boolean;
    status: StationStatus;
    url: string;


    constructor(name: string, status: StationStatus) {
        this.name = name;
        this.done = status === StationStatus.DONE;
        this.status = status;
        this.url = name.toLowerCase();
    }
}

export enum StationsState {
    Unknown = -1,
    InProgress = 0,
    Done = 1
}

export class StationsResult implements RiddleResult {
    state: RiddleState;
    subState: StationsState;
    stations: Station[];
}

export class FinalStationsResult implements RiddleResult {
    state: RiddleState;
    subState: StationsState;
    stations: Station[];
    oldStations: Station[];
    frenchDone: boolean;
    usDone: boolean;
    chineseDone: boolean;
    britishDone: boolean;
    russianDone: boolean;
    allDone: boolean; 
}

export class FinalStationsAtempt extends RiddleAtempt {
    station: string;
    frenchCode: string;
    usCode: string;
    chineseCode: string;
    britishCode: string;
    russianCode: string;

    constructor(state: RiddleState, frenchCode: string, usCode: string, chineseCode: string, britishCode: string, russianCode: string) {
        super(state, 0);
        this.frenchCode = frenchCode;
        this.usCode = usCode;
        this.chineseCode = chineseCode;
        this.britishCode = britishCode;
        this.russianCode = russianCode;
    }
}


export class StationResult implements RiddleResult {
    state: RiddleState;
    subState: StationsState;
    station: Station;
    distance: number;
    latitude: number;
    longitude: number;
    timeToOverhead: number;
    agents: AgentsOnline[];
    agentsNeeded: number;
    data: string;
}

export class StationAtempt extends RiddleAtempt {
    station: string;
    gpsCoords: Coordinates;
    guessCoords: Coordinates;

    constructor(state: RiddleState, station: string, gpsLat: number, gpsLong: number, guessLat: number, guessLong: number) {
        super(state, 0);
        this.station = station;
        this.gpsCoords = new Coordinates(gpsLat, gpsLong);
        this.guessCoords = new Coordinates(guessLat, guessLong);
    }
}
