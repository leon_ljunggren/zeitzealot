import { RiddleResult, RiddleAtempt, RiddleState } from './riddle-models';

export enum ToolsState {
    Unknown = -1,
    List = 1 << 1,
    Chrome = 1 << 2,
    Firefox = 1 << 3,
    Safari = 1 << 4,
    Done = 1 << 5
}

export namespace ToolsState {
    export function next(atempt: ToolsAtempt): ToolsState {

        let state = atempt.subState | getStateFromBrowser(atempt.tool);

        console.log("ToolsState: atempt=", atempt.subState, " browser=", getStateFromBrowser(atempt.tool), " state=", state);

        if (hasAll(state)) {
            state = state | ToolsState.Done;
        }

        return state;
    }

    export function isSet(state: ToolsState, flag: ToolsState) {
        return (state & flag) === flag;
    }

    export function hasAll(toolState: ToolsState): boolean {
        return isSet(toolState, ToolsState.Chrome) && isSet(toolState, ToolsState.Firefox) && isSet(toolState, ToolsState.Safari);
    }

    export function getStateFromBrowser(browser: string): ToolsState {
        switch (browser) {
            case "chrome": return ToolsState.Chrome;
            case "chromium": return ToolsState.Chrome;
            case "firefox": return ToolsState.Firefox;
            case "safari": return ToolsState.Safari;
        }

        return 0;
    }
}

export class ToolsResult implements RiddleResult {
    state: RiddleState;
    subState: ToolsState;
}

export class ToolsAtempt extends RiddleAtempt {
    tool: string;

    constructor(state: RiddleState, subState: ToolsState, tool: string) {
        super(state, subState);
        this.tool = tool;
    }
}
