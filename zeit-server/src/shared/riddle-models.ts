export enum RiddleState {
    Intro = "intro",
    Nickname = "nickname",
    NickAccept = "nick-accept",
    BeInTouch = "be-in-touch",
    PersonalityTest = "personality-test",
    Error = "error",
    Loading = "loading",
    PreTools = "pre-tools",
    Tools = "tools",
    GpsIntro = "gps-intro",
    BeInTouch2 = "be-in-touch-2",
    HighScore = "high-score",
    TeamSetup = "team-setup",
    BranchInfo = "branch-info",
    SatellitePre = "satellite-pre",
    Satellite = "satellite",
    EncryptionCode = "encryption-code",
    Explanations = "explanations",
    PreStations = "pre-stations",
    Stations = "stations",
    Station = "station",
    PreFinalStation = "pre-final-station",
    FinalStation = "final-station",
    BeInTouch3 = "be-in-touch-3"
}

export interface RiddlePlayer {
    domain: string;
    user: string;
    nickname: string;
    state: RiddleState;
    subState: number;
    partner: string;
}

export interface RecruitsStatus {
    players: RiddlePlayer[];
}


export class RiddleAtempt {
    state: RiddleState;
    subState: number;
    browserId?: string;

    constructor(state: RiddleState, subState?: number) {
        this.state = state;

        if (typeof subState !== 'undefined') {
            this.subState = subState;
        }
        else {
            this.subState = 0;
        }
    }
}

export interface RiddleResult {
    state: RiddleState;
    subState: number;
}
