export enum FinalState {
    LaunchCodes = 0,
    SelfDestructCodes = 1,
    SelfDestructComplete = 2
}

export class FinalResult {
    state: FinalState;
    frenchDone: boolean;
    usDone: boolean;
    chineseDone: boolean;
    britishDone: boolean;
    russianDone: boolean;
    allDoneLaunch: boolean; 
    self1Done: boolean;
    self2Done: boolean;
    self3Done: boolean;
    self4Done: boolean;
    allDoneSelf: boolean; 
    activateSelfDestruct: boolean;
}

export class FinalAtempt {
    browserId: string;
    frenchCode: string;
    usCode: string;
    chineseCode: string;
    britishCode: string;
    russianCode: string;

    self1: string;
    self2: string;
    self3: string;
    self4: string;

    activateSelfDestruct: boolean;

    constructor(frenchCode: string, usCode: string, chineseCode: string, britishCode: string, russianCode: string,
        self1: string, self2: string, self3: string, self4: string, activateSelfDestruct: boolean) {
        this.frenchCode = frenchCode;
        this.usCode = usCode;
        this.chineseCode = chineseCode;
        this.britishCode = britishCode;
        this.russianCode = russianCode;
        this.self1 = self1;
        this.self2 = self2;
        this.self3 = self3;
        this.self4 = self4;
        this.activateSelfDestruct = activateSelfDestruct;
    }
}
