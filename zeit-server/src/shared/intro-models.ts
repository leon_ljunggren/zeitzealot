import { RiddleResult, RiddleAtempt, RiddleState } from './riddle-models';

export class IntroResult implements RiddleResult {
    state: RiddleState;
    subState: number;
    correctDate: boolean;
    churchill: boolean;
    correctAuthor: boolean;
}

export class IntroAtempt extends RiddleAtempt {
    state: RiddleState;
    subState: number;
    date: string;
    author: string;

    constructor(state: RiddleState, date: string, author: string) {
        super(state, 0);
        this.date = date;
        this.author = author;
    }
}
