import { RiddleResult, RiddleState } from "./riddle-models";

export const ALLOWED_HACK_INTERVAL = 30 * 1000;

export class AgentsOnline {
    nickname: string;
    location?: string;
}

export enum SatelliteState {
    Unknown = -1,
    Hacking = 0,
    Done = 1,
    PreFinished = 2
}

export class SatelliteResult implements RiddleResult {
    state: RiddleState;
    subState: SatelliteState;
    agents: AgentsOnline[];
    timeToOverhead: number;
}


export function formateDuration(inputSeconds: any): string {
    let totalHours: number;
    let totalMinutes: number;
    let totalSeconds: number;
    let hours: string | number;
    let minutes: string | number;
    let seconds: string | number;
    let result = '';

    let negative = false;

    if (inputSeconds < 0) {
      negative = true;
    }

    totalSeconds = Math.abs(inputSeconds);
    totalMinutes = totalSeconds / 60;
    totalHours = totalMinutes / 60;

    seconds = Math.floor(totalSeconds) % 60;
    minutes = Math.floor(totalMinutes) % 60;
    hours = Math.floor(totalHours) % 60;

    if (negative) {
      seconds = -seconds;
      minutes = -minutes;
      hours = -hours;
    }

    if (hours !== 0) {
      result += hours + ':';

      if (minutes.toString().length === 1) {
        minutes = '0' + minutes;
      }
    }

    result += minutes + ':';

    if (seconds.toString().length === 1) {
      seconds = '0' + seconds;
    }

    result += seconds;

    return result;
  }
