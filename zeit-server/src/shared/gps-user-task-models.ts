import { RiddleState } from "./riddle-models";

export class GpsUserTaskRequest {
    state: RiddleState;

    constructor(state: RiddleState) {
        this.state = state;
    }
}

export class GpsUserTaskResponse {
    description: string;
}
