export const apiConf = {
    player: '/api/player',
    atempt: '/api/atempt',
    final: '/api/final',
    getFinalState: '/api/getFinalState',
    access: '/api/access',
    gpsTask: '/api/gpsTask',
    recruitStatus: '/api/recruitStatus',
    teamStatus: '/api/teamStatus'
};
