import { RiddleResult, RiddleAtempt, RiddleState } from './riddle-models';

export class Coordinates {
    latitude: number; 
    longitude: number;

    constructor(latitude: number, longitude: number) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}

export enum GpsIntroState {
    Unknown = -1,
    Intro = 0,
    Done = 1
}

export class GpsIntroResult implements RiddleResult {
    state: RiddleState;
    subState: GpsIntroState;
    distance: number;
}

export class GpsIntroAtempt extends RiddleAtempt {
    coordinates: Coordinates;

    constructor(state: RiddleState, subState: GpsIntroState,  latitude: number, longitude: number) {
        super(state, subState);
        this.coordinates = new Coordinates(latitude, longitude);
    }
}
