import { RiddleResult, RiddleAtempt, RiddleState } from './riddle-models';

export class EncryptionCodeResult implements RiddleResult {
    state: RiddleState;
    subState: number;
    correct: boolean;
}

export class EncryptionCodeAtempt extends RiddleAtempt {
    state: RiddleState;
    subState: number;
    code: string;

    constructor(state: RiddleState, code: string) {
        super(state, 0);
        this.code = code;
    }
}
