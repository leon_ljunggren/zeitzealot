import { RiddleResult, RiddleAtempt, RiddleState } from './riddle-models';

export enum PersonalityTestState {
    Unknown = -1,
    Intro = 0,
    MenInBlackQuestion = 1,
    MenInBlackBadReaction = 2,
    MenInBlackResurection = 3,
    MenInBlackGoodReaction = 4,
    CellQuestion = 5,
    CellNoble = 6,
    CellNothing = 7,
    NukeQuestion = 8,
    NukeBadReaction = 9,
    NukeGoodReaction = 10,
    CandleQuestion = 11,
    CandleBadReaction = 12,
    CandleGoodReaction = 13,
    Done = 14
}

export class PersonalityTestResult implements RiddleResult {
    state: RiddleState;
    subState: PersonalityTestState;
}

export class PersonalityTestAtempt extends RiddleAtempt {
    answer: string;

    constructor(state: RiddleState, subState: PersonalityTestState, answer: string) {
        super(state, subState);
        this.answer = answer;
    }
}
