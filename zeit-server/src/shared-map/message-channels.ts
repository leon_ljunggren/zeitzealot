export enum MessageChannel  {
    LEVEL = 'level',
    GAME = 'game',
    MAP = 'map',
    START = 'start',
    SELF_DESTRUCT = "selfDestruct",
    ADJUST_MISSILE_SPEED = "adjustMissileSpeed",
    ADJUST_MAP_SPEED = "adjustMapSpeed",
    PLAY_VIDEO = "playVideo"
}
