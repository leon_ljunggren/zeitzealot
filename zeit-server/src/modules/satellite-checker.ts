import { MysqlUtil } from './mysql-util';
import { RiddlePlayer, RiddleState, RiddleAtempt } from '../shared/riddle-models';
import { RiddleCallback, RiddleChecker } from './riddle-checker';
import { SatelliteResult, SatelliteState, ALLOWED_HACK_INTERVAL } from '../shared/satellite-models';
import { AgentsOnline } from '../shared/satellite-models';

const CLEAN_AGENT_INTERVAL = 1000;
const AGENT_TIME_TO_LIVE = 10 * 1000;
const AGENTS_NEEDED = 4;

const HOUR = 20;
const MINUTE = 39;

export class Satellite {

    private static agentsOnline = new Set();
    private static agentToLastSeen = new Map();

    private static cleanAgentInterval: NodeJS.Timeout = setInterval( () => {
        const now = new Date().getTime();
        Satellite.agentToLastSeen.forEach(function(lastSeen: number, nickname: string) {
            const msSinceLastSeen = now - lastSeen;

            if (msSinceLastSeen > AGENT_TIME_TO_LIVE) {
                Satellite.agentsOnline.delete(nickname);
                Satellite.agentToLastSeen.delete(nickname);

                console.log("Satellite removing: ", nickname);
            }
        });
    }, CLEAN_AGENT_INTERVAL);

    public static addAgentOnline(player: RiddlePlayer): void {
        this.agentsOnline.add(player.nickname);
        this.agentToLastSeen.set(player.nickname, new Date().getTime());
    }

    public static addDays(oldDate: Date, days:  number): Date {
        const date = new Date(oldDate.valueOf());
        date.setDate(date.getDate() + days);
        return date;
    }

    public static getSatelliteOverhead(): Date {
        let satelliteOverhead = new Date();
        satelliteOverhead.setHours(HOUR, MINUTE, 0, 0);

        const now = new Date(new Date().getTime() - 2 * ALLOWED_HACK_INTERVAL);

        if (satelliteOverhead.getTime() < now.getTime()) {
            satelliteOverhead = this.addDays(satelliteOverhead, 1);
        }

        return satelliteOverhead;
    }

    public static getSecondsToSatelliteOverhead(): number {
        return Math.round((this.getSatelliteOverhead().getTime() - new Date().getTime()) / 1000);
    }

    public static handle(player: RiddlePlayer, atempt: RiddleAtempt, callback: RiddleCallback) {

        const result: SatelliteResult = {
            state: RiddleState.Satellite,
            subState: SatelliteState.Hacking,
            agents: [],
            timeToOverhead: Number.MAX_SAFE_INTEGER
        };

        const self = this;

        MysqlUtil.getTeamTask(RiddleState.Satellite, function(task) {
            if ((task && task.complete) || player.subState === SatelliteState.Done) {

                const state = RiddleChecker.next(player.state);
                const subState = 0;

                result.state = RiddleState.Satellite;
                result.subState = SatelliteState.PreFinished;
    
                MysqlUtil.saveState(player.domain, state, subState, function () {
                    callback(result);
                });
            }
            else {
                let complete = false;
                self.addAgentOnline(player);
                
                result.timeToOverhead = self.getSecondsToSatelliteOverhead();
    
                /* Time is negative in 2 * ALLOWED_HACK_INTERVAL, but only ALLOWED_HACK_INTERVAL are allowed */
                if (result.timeToOverhead < 0 && result.timeToOverhead > -ALLOWED_HACK_INTERVAL) {
                    if (self.agentsOnline.size >= AGENTS_NEEDED) {
                        result.subState = SatelliteState.Done;
                        complete = true;
                    }
                }
    
                self.agentsOnline.forEach(function (nick: string) {
                    result.agents.push({nickname: nick});
                });
    
                MysqlUtil.saveTeamTask(RiddleState.Satellite, complete, function() {
                    MysqlUtil.saveState(player.domain, result.state, result.subState, function () {
                        callback(result);
                    });
                });
            }
        });
    }
}
