import { MysqlUtil } from './mysql-util';
import { RiddlePlayer, RiddleState, RiddleAtempt } from '../shared/riddle-models';
import { RiddleCallback, RiddleChecker } from './riddle-checker';
import { StationsResult, StationsState, Station, StationStatus, 
    StationAtempt, StationResult, FinalStationsResult, FinalStationsAtempt } from '../shared/stations-models';
import { GpsUtil } from './gps-util';
import { Coordinates } from '../shared/gps-intro-models';
import { ALLOWED_HACK_INTERVAL } from '../shared/satellite-models';

const CLEAN_AGENT_INTERVAL = 1000;
const AGENT_TIME_TO_LIVE = 10 * 1000;

export class StationServer {
    private name: string;
    private agentsOnline = new Set();
    private agentToLastSeen = new Map();
    private agentToLocation = new Map();

    private cleanAgentInterval: NodeJS.Timeout;

    constructor(name: string) {
        this.name = name;

        const self = this;
        this.cleanAgentInterval = setInterval(() => {
            const now = new Date().getTime();
            self.agentToLastSeen.forEach(function (lastSeen: number, nickname: string) {
                const msSinceLastSeen = now - lastSeen;
    
                if (msSinceLastSeen > AGENT_TIME_TO_LIVE) {
                    self.agentsOnline.delete(nickname);
                    self.agentToLastSeen.delete(nickname);
                    self.agentToLocation.delete(nickname);
    
                    console.log("StationServer(" + self.name + ") removing: ", nickname);
                }
            });
        }, CLEAN_AGENT_INTERVAL);
    } 

    public hasAgentInRange(): boolean {

        for (const [nickname, location] of this.agentToLocation.entries()) {
            if (location.inRange) {
                return true;
            }
        }

        return false;
    }

    public getAgentLocation(nickname: string): string {
        const location = this.agentToLocation.get(nickname);

        if (location && location.loc) {
            return location.loc;
        }

        return "";
    }

    public addAgentOnline(player: RiddlePlayer, location: string, inRange: boolean): void {
        this.agentsOnline.add(player.nickname);
        this.agentToLastSeen.set(player.nickname, new Date().getTime());
        this.agentToLocation.set(player.nickname, { loc: location, inRange: inRange });
    }

    public addDays(oldDate: Date, days: number): Date {
        const date = new Date(oldDate.valueOf());
        date.setDate(date.getDate() + days);
        return date;
    }

    public getWindowOfOpportunity(hour: number, minute: number): Date {
        let window = new Date();
        window.setHours(hour, minute, 0, 0);

        const now = new Date(new Date().getTime() - 2 * ALLOWED_HACK_INTERVAL);

        if (window.getTime() < now.getTime()) {
            window = this.addDays(window, 1);
        }

        return window;
    }

    public getSecondsToWindowOfOpportunity(hour: number, minute: number): number {
        return Math.round((this.getWindowOfOpportunity(hour, minute).getTime() - new Date().getTime()) / 1000);
    }
}


export class Stations {

    private static stationServers = new Map();


    public static handle(player: RiddlePlayer, atempt: RiddleAtempt, callback: RiddleCallback) {

        const result: StationsResult = {
            state: RiddleState.Stations,
            subState: StationsState.InProgress,
            stations: []
        };

        MysqlUtil.getStations(RiddleState.Stations, function (stations) {

            let done = true;
            stations.forEach(function (station: any) {
                result.stations.push(new Station(station.name, station.status));

                if (station.status !== StationStatus.DONE) {
                    done = false;
                }
            });

            if (done) {
                result.subState = StationsState.Done;

                const state = RiddleChecker.next(player.state);

                MysqlUtil.saveState(player.domain, state, 0, function () {
                    callback(result);
                });
            }
            else {
                callback(result);
            }
        });
    }

    public static handleFinal(player: RiddlePlayer, atempt: FinalStationsAtempt, callback: RiddleCallback) {

        const result: FinalStationsResult = {
            state: RiddleState.Stations,
            subState: StationsState.InProgress,
            stations: [],
            oldStations: [],
            frenchDone: false,
            usDone: false,
            chineseDone: false,
            britishDone: false,
            russianDone: false,
            allDone: false
        };

        /* Get the old stations so we still have a reference to them */
        MysqlUtil.getStations(RiddleState.Stations, function (oldStations: any) {

            let done = true;
            oldStations.forEach(function (station: any) {
                result.oldStations.push(new Station(station.name, station.status));

                if (station.status !== StationStatus.DONE) {
                    done = false;
                }
            });

            /* Get the new station */
            MysqlUtil.getStations(RiddleState.FinalStation, function (stations) {
                stations.forEach(function (station: any) {
                    result.stations.push(new Station(station.name, station.status));

                    if (station.status !== StationStatus.DONE) {
                        done = false;
                    }
                });

                if (done) {
                    result.subState = StationsState.Done;
                }
                

                if (atempt.frenchCode === "57886-95653-57346-32534") {
                    result.frenchDone = true;
                }

                if (atempt.usCode === "00000000" || atempt.usCode === "00000-00000-00000-00000") {
                    result.usDone = true;
                }

                if (atempt.chineseCode === "98036-13962-29179-30595") {
                    result.chineseDone = true;
                }

                if (atempt.britishCode === "36142-82497-23742-73535") {
                    result.britishDone = true;
                }

                if (atempt.russianCode === "15699-23542-78734-32359") {
                    result.russianDone = true;
                }

                result.allDone = result.frenchDone && result.usDone && result.chineseDone && result.britishDone && result.russianDone;

                if (result.allDone) {
                    const state = RiddleChecker.next(player.state);

                    MysqlUtil.saveState(player.domain, state, 0, function () {
                        callback(result);
                    });
                }
                else {
                    callback(result);
                }
            });
        });
    }

    public static handleStation(player: RiddlePlayer, atempt: StationAtempt, callback: RiddleCallback) {

        MysqlUtil.getStation(atempt.station, function (station) {

            let server = Stations.stationServers.get(station.name);

            if (!server) {
                server = new StationServer(station.name);
                Stations.stationServers.set(station.name, server);
            }

            const result: StationResult = {
                state: RiddleState.Station,
                subState: StationsState.InProgress,
                station: new Station(station.name, station.status),
                distance: GpsUtil.calculateDistance(new Coordinates(station.latitude, station.longitude), atempt.gpsCoords),
                latitude: -1,
                longitude: -1,
                timeToOverhead: 0,
                agents: [],
                agentsNeeded: station.agentsNeeded,
                data: ""
            }; 

            if (station.status === StationStatus.UNKNOWN) {
                const distance = GpsUtil.calculateDistance(new Coordinates(station.latitude, station.longitude), atempt.guessCoords);

                if (distance <= station.locationRadius) {
                    result.station.status = StationStatus.LOCATED;

                    MysqlUtil.saveStation(atempt.station, StationStatus.LOCATED, function () {
                        callback(result);
                    });
                }
                else {
                    callback(result);
                }
            }
            else if (station.status === StationStatus.LOCATED) {
                /* Hacking part */

                let location = "remote";
                let inRange = false;

                if (result.distance <= station.approvalRadius) {
                    location = "in range";
                    inRange = true;
                }

                server.addAgentOnline(player, location, inRange);

                result.latitude = station.latitude;
                result.longitude = station.longitude;

                result.timeToOverhead = server.getSecondsToWindowOfOpportunity(station.hour, station.minute);

                /* Time is negative in 2 * ALLOWED_HACK_INTERVAL, but only ALLOWED_HACK_INTERVAL are allowed */
                if (result.timeToOverhead < 0 &&
                    result.timeToOverhead > -ALLOWED_HACK_INTERVAL &&
                    server.agentsOnline.size >= station.agentsNeeded &&
                    server.hasAgentInRange()) {

                    result.subState = StationsState.Done;
                    result.station.status = StationStatus.DONE;
                    result.data = station.data;

                    MysqlUtil.saveStation(atempt.station, StationStatus.DONE, function () {
                        callback(result);
                    });
                }
                else {
                    server.agentsOnline.forEach(function (nick: string) {
                        result.agents.push({ nickname: nick, location: server.getAgentLocation(nick) });
                    });

                    callback(result);
                }
            }
            else if (station.status === StationStatus.DONE) {

                result.subState = StationsState.Done;
                result.data = station.data;

                callback(result);
            }
            else {
                console.log("Wrong status on station=" + station);
                callback(result);
            }
        });
    }
}
