import { PersonalityTestAtempt, PersonalityTestState } from "../shared/personality-test-models";

const nextPersonalityTest = new Map<PersonalityTestState, PersonalityTestState>([
    [PersonalityTestState.Intro, PersonalityTestState.MenInBlackQuestion],
    [PersonalityTestState.MenInBlackBadReaction, PersonalityTestState.MenInBlackResurection],
    [PersonalityTestState.MenInBlackResurection, PersonalityTestState.CellQuestion],
    [PersonalityTestState.MenInBlackGoodReaction, PersonalityTestState.CellQuestion],
    [PersonalityTestState.CellNoble, PersonalityTestState.NukeQuestion],
    [PersonalityTestState.CellNothing, PersonalityTestState.NukeQuestion],
    [PersonalityTestState.NukeBadReaction, PersonalityTestState.CandleQuestion],
    [PersonalityTestState.NukeGoodReaction, PersonalityTestState.CandleQuestion],
    [PersonalityTestState.CandleBadReaction, PersonalityTestState.Done],
    [PersonalityTestState.CandleGoodReaction, PersonalityTestState.Done],
]);

export class PersonalityTest {
    public static next(atempt: PersonalityTestAtempt): PersonalityTestState {
        const nextState = nextPersonalityTest.get(atempt.subState);

        if (nextState) {
            return nextState;
        }
        else {
            if (atempt.subState === PersonalityTestState.MenInBlackQuestion) {
                if (atempt.answer.toLowerCase() === "girl") {
                    return PersonalityTestState.MenInBlackBadReaction;
                }
                else {
                    return PersonalityTestState.MenInBlackGoodReaction;
                }
            }
            else if (atempt.subState === PersonalityTestState.CellQuestion) {
                if (atempt.answer.toLowerCase() === "nothing") {
                    return PersonalityTestState.CellNothing;
                }
                else {
                    return PersonalityTestState.CellNoble;
                }
            }
            else if (atempt.subState === PersonalityTestState.NukeQuestion) {
                if (atempt.answer.toLowerCase() === "launch") {
                    return PersonalityTestState.NukeBadReaction;
                }
                else {
                    return PersonalityTestState.NukeGoodReaction;
                }
            }
            else if (atempt.subState === PersonalityTestState.CandleQuestion) {
                if (atempt.answer.toLowerCase().includes("dark") && 
                (atempt.answer.toLowerCase().includes("energy") || atempt.answer.toLowerCase().includes("matter"))) {
                    return PersonalityTestState.CandleGoodReaction;
                }
                else {
                    return PersonalityTestState.CandleBadReaction;
                }
            }

            return PersonalityTestState.Unknown;
        }
    }

    public static fixResult(state: PersonalityTestState): PersonalityTestState {

        /* Resurection doesn't happen until you refresh, so return the bad reaction state */
        if (state === PersonalityTestState.MenInBlackResurection) {
            return PersonalityTestState.MenInBlackBadReaction;
        }

        return state;
    }
}
