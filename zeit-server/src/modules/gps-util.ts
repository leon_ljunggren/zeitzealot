import { Coordinates } from "../shared/gps-intro-models";

export class GpsTask {
    domain: string;
    state: string;
    latitude: number;
    longitude: number;
    approvalRadius: number;
    description: string;
}

export class GpsUtil {
    public static calculateDistance(cord1: Coordinates, cord2: Coordinates) {
        const R = 6371000; // meters
        const dLat = this.toRadians(cord2.latitude - cord1.latitude);
        const dLon = this.toRadians(cord2.longitude - cord1.longitude);
        const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.toRadians(cord1.latitude)) * Math.cos(this.toRadians(cord2.latitude)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        const distance = R * c;
        return distance;
    }

    private static toRadians(degrees: number): number {
        return degrees * Math.PI / 180;
    }
}
