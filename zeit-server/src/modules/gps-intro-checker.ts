import { MysqlUtil } from './mysql-util';
import { GpsIntroAtempt, GpsIntroState, GpsIntroResult, Coordinates } from '../shared/gps-intro-models';
import { RiddlePlayer, RiddleState } from '../shared/riddle-models';
import { RiddleCallback, RiddleChecker } from './riddle-checker';
import { GpsUtil, GpsTask } from './gps-util';

const nextPersonalityTest = new Map<GpsIntroState, GpsIntroState>([
]);

export class GpsIntro {

    public static handle(player: RiddlePlayer, atempt: GpsIntroAtempt, callback: RiddleCallback) {

        const result: GpsIntroResult = {
            state: RiddleState.GpsIntro,
            subState: GpsIntroState.Unknown,
            distance: Number.MAX_SAFE_INTEGER
        };

        MysqlUtil.getGpsTask(player.domain, RiddleState.GpsIntro, function(gpsTask: GpsTask) {

            result.distance = GpsUtil.calculateDistance(new Coordinates(gpsTask.latitude, gpsTask.longitude), atempt.coordinates);

            if (result.distance < gpsTask.approvalRadius) {
                result.subState = GpsIntroState.Done;
            }
            else {
                result.subState = GpsIntroState.Intro;
            }
    
            let state = RiddleState.GpsIntro;
            let subState = result.subState;
    
            /* If we're done, we should proceed to the next riddle */
            if (result.subState === GpsIntroState.Done) {
                state = RiddleChecker.next(player.state);

                /* The late commers should not see the team setup riddles */
                if (player.domain === "drlove.zeitzealot.org" || player.domain === "editor.zeitzealot.org") {
                    state = RiddleState.BranchInfo;
                }

                subState = 0;
            }
    
            console.log("Saving gps-intro state=", subState, " result=", result);
    
            MysqlUtil.saveState(player.domain, state, subState, function () {
                callback(result);
            });
        });
    }

    private static next(atempt: GpsIntroAtempt): GpsIntroState {
        const nextState = nextPersonalityTest.get(atempt.subState);

        if (nextState) {
            return nextState;
        }
        else {
            return GpsIntroState.Unknown;
        }
    }

    private static fixResult(state: GpsIntroState): GpsIntroState {

        return state;
    }
}
