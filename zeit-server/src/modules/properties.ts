import dotenv from "dotenv";
import fs from "fs";

let mysqlHost = '127.0.0.1';

if (fs.existsSync(".env")) {
    dotenv.config({ path: ".env" });
    mysqlHost = process.env["MYSQL_HOST"];
} 

console.log("Using mysql host ", mysqlHost);

export const Properties = {
    MYSQL_SETTINGS: {
        connectionLimit: 20,
        host: mysqlHost,
        port: 3306,
        user: 'zeituser',
        password: 'PDBXCQg9LwhBGZBG5cbZVR6mg3UC7a',
        database: 'zeit'
    }
};
