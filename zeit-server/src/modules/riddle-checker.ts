import { MysqlUtil } from './mysql-util';

import { RiddleResult, RiddleAtempt, RiddlePlayer, RiddleState } from '../shared/riddle-models';
import { IntroAtempt, IntroResult } from '../shared/intro-models';
import { NicknameAtempt, NicknameResult } from '../shared/nickname-models';
import { PersonalityTestAtempt, PersonalityTestResult, PersonalityTestState } from '../shared/personality-test-models';
import { ToolsAtempt, ToolsResult, ToolsState } from '../shared/tools-models';
import { GpsIntro } from './gps-intro-checker';
import { Satellite } from './satellite-checker';
import { GpsIntroAtempt } from '../shared/gps-intro-models';
import { PersonalityTest } from './personality-test-checker';
import { EncryptionCodeAtempt, EncryptionCodeResult } from '../shared/encryption-code-models';
import { Stations } from './stations-checker';
import { StationAtempt, FinalStationsAtempt, StationResult } from '../shared/stations-models';
import { FinalState, FinalResult, FinalAtempt } from '../shared/final-models';

export interface RiddleCallback {
    (res: RiddleResult): void;
}

export interface FinalCallback {
    (res: FinalResult): void;
}

const RATE_LIMIT_TIME = 60 * 1000;

export class RiddleChecker {

    static readonly nextRiddle = new Map<RiddleState, RiddleState>([
        [RiddleState.Intro, RiddleState.Nickname],
        [RiddleState.Nickname, RiddleState.NickAccept],
        [RiddleState.NickAccept, RiddleState.PersonalityTest],
        [RiddleState.PersonalityTest, RiddleState.PreTools],
        [RiddleState.PreTools, RiddleState.Tools],
        [RiddleState.Tools, RiddleState.GpsIntro],
        [RiddleState.GpsIntro, RiddleState.BranchInfo],
        [RiddleState.HighScore, RiddleState.TeamSetup],
        [RiddleState.TeamSetup, RiddleState.BranchInfo],
        [RiddleState.BranchInfo, RiddleState.SatellitePre],
        [RiddleState.SatellitePre, RiddleState.Satellite],
        [RiddleState.Satellite, RiddleState.EncryptionCode],
        [RiddleState.EncryptionCode, RiddleState.Explanations],
        [RiddleState.Explanations, RiddleState.PreStations],
        [RiddleState.PreStations, RiddleState.Stations],
        [RiddleState.Stations, RiddleState.PreFinalStation],
        [RiddleState.PreFinalStation, RiddleState.FinalStation],
        [RiddleState.FinalStation, RiddleState.BeInTouch3]
    ]);

    public static next(state: RiddleState): RiddleState {
        const nextState = RiddleChecker.nextRiddle.get(state);

        if (nextState) {
            return nextState;
        }
        else {
            return RiddleState.Error;
        }
    }

    private static lastSaveMap = new Map();
    private static lastStationResultMap = new Map();

    private static errorResult = {
        state: RiddleState.Error,
        subState: 0
    };

    private static olderThan(date: number, lenghtInMs: number): boolean {
        if (!date) {
            return true;
        }

        const diff = Date.now() - date;

        return diff > lenghtInMs;
    }

    /**
     * Checks if a riddle should be rate limited. Using this static way
     * rather than as a method on the atempt object as originaly planned.
     * The reason is that these objects are not properly deserialized,
     * so they wouldn't have the functions on them.
     */
    private static isRateLimited(atempt: RiddleAtempt): boolean {
        return atempt.state === RiddleState.GpsIntro || atempt.state === RiddleState.Satellite || atempt.state === RiddleState.Station;
    }

    private static shouldSaveHistory(player: RiddlePlayer, atempt: RiddleAtempt, result: RiddleResult): boolean {

        /* Always save history if this changed the player's state */
        if (player.state !== result.state || player.subState !== result.subState) {
            if (atempt.state !== RiddleState.Station) {
                return true;
            }
        }

        /* Hack for station riddles, which does not use the player state/subState system */
        if (atempt.state === RiddleState.Station) {
            const lastResult = this.lastStationResultMap.get(player.domain);

            this.lastStationResultMap.set(player.domain, result);

            if (lastResult) {
                const res = <StationResult>result;

                if (lastResult.station && res.station && lastResult.station.status !== res.station.status) {
                    return true;
                }
            }
            else {
                return true;
            }
        }

        let lastSaved = RiddleChecker.lastSaveMap.get(player.domain);

        if (this.olderThan(lastSaved, RATE_LIMIT_TIME)) {
            lastSaved = Date.now();
            this.lastSaveMap.set(player.domain, lastSaved);

            return true;
        }

        return false;
    }

    public static isValidState(player: RiddlePlayer, atempt: RiddleAtempt): boolean {
        if (player.state === atempt.state) {
            return true;
        }
        else if (player.state === RiddleState.Stations && atempt.state === RiddleState.Station) {
            return true;
        }
        else if (player.state === RiddleState.FinalStation && atempt.state === RiddleState.Station) {
            return true;
        }

        return false;
    }

    public static final(atempt: FinalAtempt, callback: FinalCallback): void {
        const self = this;


        const result: FinalResult = {
            state: FinalState.LaunchCodes,
            frenchDone: false,
            usDone: false,
            chineseDone: false,
            britishDone: false,
            russianDone: false,
            allDoneLaunch: false,
            self1Done: false,
            self2Done: false,
            self3Done: false,
            self4Done: false,
            allDoneSelf: false,
            activateSelfDestruct: false
        };

        MysqlUtil.getFinalState(function (finalState) {

            result.state = finalState;

            if (result.state === FinalState.SelfDestructComplete && atempt.activateSelfDestruct) {
                result.activateSelfDestruct = true;
                callback(result);
            }
            else if (result.state === FinalState.LaunchCodes) {
                if (atempt.frenchCode === "57886-95653-57346-32534") {
                    result.frenchDone = true;
                }
    
                if (atempt.usCode === "00000000" || atempt.usCode === "00000-00000-00000-00000") {
                    result.usDone = true;
                }
    
                if (atempt.chineseCode === "98036-13962-29179-30595") {
                    result.chineseDone = true;
                }
    
                if (atempt.britishCode === "36142-82497-23742-73535") {
                    result.britishDone = true;
                }
    
                if (atempt.russianCode === "15699-23542-78734-32359") {
                    result.russianDone = true;
                }
    
                result.allDoneLaunch = result.frenchDone && result.usDone && result.chineseDone && result.britishDone && result.russianDone;
    
                if (result.allDoneLaunch) {
                    result.state = FinalState.SelfDestructCodes;
    
                    MysqlUtil.saveFinalState(result.state, function() {
                        callback(result);
                    });
                }
                else {
                    callback(result);
                }
            }
            else if (result.state === FinalState.SelfDestructCodes) {
                if (atempt.self1 === "6495") {
                    result.self1Done = true;
                }
    
                if (atempt.self2 === "4216") {
                    result.self2Done = true;
                }
    
                if (atempt.self3 === "GAFFC") {
                    result.self3Done = true;
                }
    
                if (atempt.self4 === "1524") {
                    result.self4Done = true;
                }

                result.allDoneSelf = result.self1Done && result.self2Done && result.self3Done && result.self4Done;
    
                if (result.allDoneSelf) {
                    result.state = FinalState.SelfDestructComplete;
    
                    MysqlUtil.saveFinalState(result.state, function() {
                        callback(result);
                    });
                }
                else {
                    callback(result);
                }
            }
            else {
                callback(result);
            }
           
        });
    }

    public static check(domain: string, atempt: RiddleAtempt, callback: RiddleCallback): void {
        const self = this;

        if (atempt && atempt.state) {

            MysqlUtil.getPlayer(domain, function (player) {
                if (player) {
                    if (self.isValidState(player, atempt)) {
                        RiddleChecker.riddleSelector(player, atempt, function (res: RiddleResult) {

                            /* Only save history if the riddle isn't ratelimited, or enough time has passed since
                             * last save (or the state has changed)
                             */
                            if (!RiddleChecker.isRateLimited(atempt) || RiddleChecker.shouldSaveHistory(player, atempt, res)) {
                                MysqlUtil.saveHistory(domain, player.state, atempt, function () {
                                    callback(res);
                                });
                            }
                            else {
                                callback(res);
                            }
                        });
                    }
                    else {
                        console.log("RiddleChecker.check: Player state don't match atempt domain=",
                            domain, " player=", player, " atempt=", atempt);
                        callback(RiddleChecker.errorResult);
                    }

                }
                else {
                    console.log("RiddleChecker.check: Got null player for domain=", domain, " atempt=", atempt);
                    callback(RiddleChecker.errorResult);
                }
            });
        }
        else {
            console.log("RiddleChecker.check: Invalid atempt for domain=", domain, " atempt=", atempt);
            callback(RiddleChecker.errorResult);
        }
    }

    private static intro(player: RiddlePlayer, atempt: IntroAtempt, callback: RiddleCallback) {

        const result: IntroResult = {
            state: RiddleState.Intro,
            subState: 0,
            correctDate: false,
            churchill: false,
            correctAuthor: false
        };

        if (atempt.date === '5' || atempt.date === '5th' || atempt.date === '5t' || atempt.date === '05') {
            result.correctDate = true;
        }

        if (atempt.author.includes("churchill")) {
            result.churchill = true;
        } else if (atempt.author.includes("santayana")) {
            result.correctAuthor = true;
        }

        if (result.correctDate && result.correctAuthor) {
            MysqlUtil.saveState(player.domain, RiddleChecker.next(player.state), 0, function () {
                callback(result);
            });
        }
        else {
            callback(result);
        }
    }

    private static nickname(player: RiddlePlayer, atempt: NicknameAtempt, callback: RiddleCallback) {

        const result: NicknameResult = {
            state: RiddleState.Nickname,
            subState: 0,
            success: false,
            alreadyExists: false,
            neo: false,
            realName: false
        };

        MysqlUtil.getPlayerByNickname(atempt.nickname, function (existingPlayer) {
            console.log("RiddleChacker.nickname: existingPlayer=", existingPlayer);
            if (existingPlayer && existingPlayer.domain !== player.domain) {
                result.alreadyExists = true;
                callback(result);
            }
            else if (atempt.nickname.toLowerCase().startsWith("anderson") ||
                atempt.nickname.toLowerCase().startsWith("mr anderson") ||
                atempt.nickname.toLowerCase().startsWith("mranderson") ||
                atempt.nickname.toLowerCase().startsWith("mr.anderson") ||
                atempt.nickname.toLowerCase().startsWith("mr. anderson") ||
                atempt.nickname.toLowerCase().startsWith("neo")) {
                result.neo = true;
                callback(result);
            }
            else {
                MysqlUtil.saveNickname(player.domain, atempt.nickname, function () {
                    MysqlUtil.saveState(player.domain, RiddleChecker.next(player.state), 0, function () {

                        if (atempt.nickname.toLowerCase().startsWith(player.user.toLowerCase())) {
                            result.realName = true;
                        }

                        result.success = true;
                        callback(result);
                    });
                });
            }
        });
    }

    private static personalityTest(player: RiddlePlayer, atempt: PersonalityTestAtempt, callback: RiddleCallback) {

        const result: PersonalityTestResult = {
            state: RiddleState.PersonalityTest,
            subState: PersonalityTestState.Unknown
        };

        if (player.subState === atempt.subState) {
            result.subState = PersonalityTest.next(atempt);

            let state = RiddleState.PersonalityTest;
            let subState = result.subState;

            /* If we're done, we should proceed to the next riddle */
            if (result.subState === PersonalityTestState.Done) {
                state = RiddleChecker.next(player.state);
                subState = 0;
            }

            MysqlUtil.saveState(player.domain, state, subState, function () {

                result.subState = PersonalityTest.fixResult(result.subState);

                callback(result);
            });
        }
        else {
            console.log("Atempted to bypass personality test: player=", player, " atempt=", atempt);
            callback(result);
        }
    }

    private static tools(player: RiddlePlayer, atempt: ToolsAtempt, callback: RiddleCallback) {

        const result: ToolsResult = {
            state: RiddleState.Tools,
            subState: ToolsState.Unknown
        };

        result.subState = ToolsState.next(atempt);

        let state = RiddleState.Tools;
        let subState = result.subState;

        /* If we're done, we should proceed to the next riddle */
        if (ToolsState.isSet(result.subState, ToolsState.Done)) {
            state = RiddleChecker.next(player.state);
            subState = 0;
        }

        console.log("Saving tool state=", subState);

        MysqlUtil.saveState(player.domain, state, subState, function () {
            callback(result);
        });
    }

    private static encryptionCode(player: RiddlePlayer, atempt: EncryptionCodeAtempt, callback: RiddleCallback) {

        const result: EncryptionCodeResult = {
            state: RiddleState.EncryptionCode,
            subState: 0,
            correct: false
        };

        const code = atempt.code.toLowerCase();

        if (code === "7081-5395-3836-6949-4464") {
            result.correct = true;
        }

        if (result.correct) {
            MysqlUtil.saveState(player.domain, RiddleChecker.next(player.state), 0, function () {
                callback(result);
            });
        }
        else {
            callback(result);
        }
    }

    private static proceedToNextState(player: RiddlePlayer, callback: RiddleCallback) {
        MysqlUtil.saveState(player.domain, RiddleChecker.next(player.state), 0, function () {
            callback({ state: player.state, subState: 0 });
        });
    }

    private static riddleSelector(player: RiddlePlayer, atempt: RiddleAtempt, callback: RiddleCallback) {
        switch (atempt.state) {
            case RiddleState.Intro: RiddleChecker.intro(player, <IntroAtempt>atempt, callback); break;
            case RiddleState.Nickname: RiddleChecker.nickname(player, <NicknameAtempt>atempt, callback); break;
            case RiddleState.NickAccept: RiddleChecker.proceedToNextState(player, callback); break;
            case RiddleState.PersonalityTest: RiddleChecker.personalityTest(player, <PersonalityTestAtempt>atempt, callback); break;
            case RiddleState.PreTools: RiddleChecker.proceedToNextState(player, callback); break;
            case RiddleState.Tools: RiddleChecker.tools(player, <ToolsAtempt>atempt, callback); break;
            case RiddleState.GpsIntro: GpsIntro.handle(player, <GpsIntroAtempt>atempt, callback); break;
            case RiddleState.HighScore: RiddleChecker.proceedToNextState(player, callback); break;
            case RiddleState.TeamSetup: RiddleChecker.proceedToNextState(player, callback); break;
            case RiddleState.BranchInfo: RiddleChecker.proceedToNextState(player, callback); break;
            case RiddleState.SatellitePre: RiddleChecker.proceedToNextState(player, callback); break;
            case RiddleState.Satellite: Satellite.handle(player, atempt, callback); break;
            case RiddleState.EncryptionCode: RiddleChecker.encryptionCode(player, <EncryptionCodeAtempt>atempt, callback); break;
            case RiddleState.Explanations: RiddleChecker.proceedToNextState(player, callback); break;
            case RiddleState.PreStations: RiddleChecker.proceedToNextState(player, callback); break;
            case RiddleState.Stations: Stations.handle(player, atempt, callback); break;
            case RiddleState.Station: Stations.handleStation(player, <StationAtempt>atempt, callback); break;
            case RiddleState.PreFinalStation: RiddleChecker.proceedToNextState(player, callback); break;
            case RiddleState.FinalStation: Stations.handleFinal(player, <FinalStationsAtempt>atempt, callback); break;
            default: callback(RiddleChecker.errorResult); break;
        }
    }
}
