import * as mysql from 'mysql';
import { Properties } from './properties';
import { RiddleState, RiddleAtempt } from '../shared/riddle-models';
import { StationStatus } from '../shared/stations-models';
import { FinalState } from '../shared/final-models';

interface MysqlCallback {
    (rows: any, fields: any): void;
}

class MysqlReaderWriter {
    private pool = mysql.createPool(Properties.MYSQL_SETTINGS);

    public read(sql: string, callback: MysqlCallback) {
        this.pool.query(sql, function (err, rows, fields) {

            if (err) {
                throw err;
            }

            callback(rows, fields);
        });
    }

    public write(sql: string, callback: MysqlCallback) {
        this.pool.query(sql, function (err, rows, fields) {

            if (err) {
                throw err;
            }

            callback(rows, fields);
        });
    }
}

export class MysqlUtil {
    private static mysql = new MysqlReaderWriter();

    public static getPlayer(domain: string, callback: MysqlCallback): void {
        let sql = 'SELECT * FROM Player WHERE domain = ?';
        sql = mysql.format(sql, [domain]);

        MysqlUtil.mysql.read(sql, function (rows: any) {

            let player = {};
            // console.log('mysql.getPlayer: rows=', rows);
            rows.forEach(function (value: any, index: number) {

                player = value;
            });

            callback(player, undefined);
        });
    }

    public static getPlayerByNickname(nickname: string, callback: MysqlCallback): void {
        let sql = 'SELECT * FROM Player WHERE nickname = ?';
        sql = mysql.format(sql, [nickname]);

        this.mysql.read(sql, function (rows) {

            let player;
            // console.log('mysql.getPlayerByNickname: rows=', rows);
            rows.forEach(function (value: any, index: number) {

                player = value;
            });

            callback(player, undefined);
        });
    }

    public static getGpsTask(domain: string, state: string, callback: MysqlCallback): void {
        let sql = 'SELECT * FROM GpsTask WHERE domain = ? AND state = ?';
        sql = mysql.format(sql, [domain, state]);

        MysqlUtil.mysql.read(sql, function (rows: any) {

            let task = {};
            // console.log('mysql.getGpsTask: rows=', rows);
            rows.forEach(function (value: any, index: number) {

                task = value;
            });

            callback(task, undefined);
        });
    }

    public static getStations(state: RiddleState, callback: MysqlCallback): void {
        let sql = 'SELECT * FROM Station WHERE state = ?';
        sql = mysql.format(sql, [state]);

        MysqlUtil.mysql.read(sql, function (rows: any) {

            const stations: any[] = [];
            rows.forEach(function (value: any, index: number) {

                stations.push(value);
            });

            callback(stations, undefined);
        });
    }

    public static getStation(name: string, callback: MysqlCallback): void {
        let sql = 'SELECT * FROM Station WHERE name = ?';
        sql = mysql.format(sql, [name]);

        MysqlUtil.mysql.read(sql, function (rows: any) {

            let station: {};
            rows.forEach(function (value: any, index: number) {

                station = value;
            });

            callback(station, undefined);
        });
    }

    public static saveStation(name: string, status: StationStatus, callback: MysqlCallback): void {
        let sql = 'UPDATE Station SET status = ? WHERE name = ?';
        sql = mysql.format(sql, [status, name]);

        this.mysql.write(sql, function () {
    
            callback(undefined, undefined);
        });
    }

    public static getFinalState(callback: MysqlCallback): void {
        let sql = 'SELECT * FROM FinalState WHERE id = ?';
        sql = mysql.format(sql, [0]);

        MysqlUtil.mysql.read(sql, function (rows: any) {

            let state = 0;
            rows.forEach(function (value: any, index: number) {

                state = value.state;
            });

            callback(state, undefined);
        });
    }

    public static saveFinalState(state: FinalState, callback: MysqlCallback): void {
        let sql = 'INSERT INTO FinalState(id, state) VALUES(?,?) ON DUPLICATE KEY UPDATE state = VALUES(state)';
        sql = mysql.format(sql, [0, state]);

        this.mysql.write(sql, function () {
    
            callback(undefined, undefined);
        });
    }

    public static getTeamTask(state: string, callback: MysqlCallback): void {
        let sql = 'SELECT * FROM TeamTask WHERE state = ?';
        sql = mysql.format(sql, [state]);

        MysqlUtil.mysql.read(sql, function (rows: any) {

            let task = {};
            rows.forEach(function (value: any, index: number) {

                task = value;
            });

            callback(task, undefined);
        });
    }

    public static saveTeamTask(state: RiddleState, complete: boolean, callback: MysqlCallback) {

        /* No point in saving unless the task is complete */
        if (complete) {
            let sql = 'INSERT INTO TeamTask(state, complete) VALUES(?,?) ON DUPLICATE KEY UPDATE complete = VALUES(complete)';
            sql = mysql.format(sql, [state, complete]);
    
            this.mysql.write(sql, function () {
    
                callback(undefined, undefined);
            });
        }
        else {
            callback(undefined, undefined);
        }
    }

    public static saveState(domain: string, state: RiddleState, subState: number, callback: MysqlCallback) {
        if (state !== RiddleState.Error) {
            let sql = 'UPDATE Player SET state = ?, subState = ? WHERE domain = ?';
            sql = mysql.format(sql, [state, subState, domain]);

            this.mysql.write(sql, function () {

                callback(undefined, undefined);
            });
        }
        else {
            /* We never save the error state, just log it */
            console.log("mysql-util: Trying to save error state for domain=", domain, " state=", state, " subState=", subState);
            callback(undefined, undefined);
        }
    }

    public static saveNickname(domain: string, nickname: string, callback: MysqlCallback) {
        let sql = 'UPDATE Player SET nickname = ? WHERE domain = ?';
        sql = mysql.format(sql, [nickname, domain]);

        this.mysql.write(sql, function () {

            callback(undefined, undefined);
        });
    }

    public static saveHistory(domain: string, state: RiddleState, atempt: RiddleAtempt, callback: MysqlCallback) {

        let sql = 'INSERT INTO PlayerHistory(domain, riddle, atempt) VALUES(?,?,?)';
        sql = mysql.format(sql, [domain, state, JSON.stringify(atempt)]);

        this.mysql.write(sql, function () {

            callback(undefined, undefined);
        });
    }
}
